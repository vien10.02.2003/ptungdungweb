package com.example.backend.api.repository;

import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.entity.LopHoc;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "lop-hoc")
public interface LopHocRepository extends JpaRepository<LopHoc, Long> {

    Boolean existsBymonHoc (String monHoc);
    @Query("SELECT DISTINCT new com.example.backend.api.dto.LopHocDTO(l,true,false) FROM LopHoc l WHERE LOWER(l.monHoc) LIKE LOWER(CONCAT('%', :monHoc, '%'))")
    List<LopHocDTO> findLopHocByTen(String monHoc);

    @Query("SELECT l FROM LopHoc l WHERE l.giaoVien.id = :giaoVienId")
    List<LopHoc> getLopHocByGiaoVien(Long giaoVienId);
    LopHoc getLopHocById(Long id);

    @Modifying
    @Transactional
    @Query("SELECT DISTINCT i FROM LopHoc i WHERE i.giaoVien IS NULL")
    List<LopHoc> findLopHocNotHaveTeacher();

}
