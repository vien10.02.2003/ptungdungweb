package com.example.backend.api.controller;

import com.example.backend.api.dto.PhuHuynhDTO;
import com.example.backend.api.service.PhuHuynhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/phuhuynh")
public class PhuHuynhController {
    @Autowired
    PhuHuynhService phuHuynhService;

    @PostMapping("/them")
    public PhuHuynhDTO addPhuHuynh(@RequestBody PhuHuynhDTO phuHuynh){
        return phuHuynhService.addPhuHuynh(phuHuynh);
    }

    @GetMapping("/all")
    public List<PhuHuynhDTO> getAllPhuHuynh(){
        List<PhuHuynhDTO> l =  phuHuynhService.getAllPhuHuynh();
        return l;
    }
    @GetMapping("/tienchuadong")
    public Integer tienChuaDong(@RequestParam("id") Long id){
        return phuHuynhService.getTienChuaDong(id);
    }

    @PutMapping("/cap-nhat")
    public PhuHuynhDTO updatePhuHuynh(@RequestParam("id") Long id,@RequestBody PhuHuynhDTO phuHuynh){
        return phuHuynhService.updatePhuHuynh(id, phuHuynh);
    }

    @DeleteMapping("")
    public Boolean deletePhuHuynh(@RequestParam("id") Long id){
        return phuHuynhService.deletePhuHuynh(id);
    }
}
