package com.example.backend.api.entity;

import jakarta.persistence.*;

import java.util.Set;

@Entity
@Table(name = "hocsinh")
public class HocSinh extends Person {
    private Integer tienDaDong;
    private Integer heSoDongTien =1;

    @ManyToMany
    @JoinTable(
            name = "hocsinh_lophoc",
            joinColumns = @JoinColumn(name = "hocsinh_id"),
            inverseJoinColumns = @JoinColumn(name = "lophoc_id")
    )
    private Set<LopHoc> lopHoc;

    @OneToMany(cascade=CascadeType.ALL,mappedBy = "hocSinh")
    private Set<DiemDanh> diemDanh;

    @OneToOne(mappedBy = "hocSinh")
    private PhuHuynh phuHuynh;

    public Integer getTienDaDong() {
        return tienDaDong;
    }

    public void setTienDaDong(Integer tienDaDong) {
        this.tienDaDong = tienDaDong;
    }

    public Integer getHeSoDongTien() {
        return heSoDongTien;
    }

    public void setHeSoDongTien(Integer heSoDongTien) {
        this.heSoDongTien = heSoDongTien;
    }

    public Set<LopHoc> getLopHoc() {
        return lopHoc;
    }

    public void setLopHoc(Set<LopHoc> lopHoc) {
        this.lopHoc = lopHoc;
    }

    public Set<DiemDanh> getDiemDanh() {
        return diemDanh;
    }

    public void setDiemDanh(Set<DiemDanh> diemDanh) {
        this.diemDanh = diemDanh;
    }

    public PhuHuynh getPhuHuynh() {
        return phuHuynh;
    }

    public void setPhuHuynh(PhuHuynh phuHuynh) {
        this.phuHuynh = phuHuynh;
    }
}
