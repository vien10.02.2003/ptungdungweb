package com.example.backend.api.repository;

import com.example.backend.api.dto.GiaoVienDTO;
import com.example.backend.api.entity.GiaoVien;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "giao-vien")
public interface GiaoVienRepository extends JpaRepository<GiaoVien, Long> {

    @Query("SELECT DISTINCT new com.example.backend.api.dto.GiaoVienDTO(g,true) FROM GiaoVien g ORDER BY SUBSTRING_INDEX(g.ten, ' ', -1) ASC")
    List<GiaoVienDTO> findAllGiaoVienSapXepTheoTen();

    @Query("SELECT DISTINCT g FROM GiaoVien g WHERE LOWER(g.ten) LIKE LOWER(CONCAT('%', :ten, '%'))")
    List<GiaoVien> findGiaoVienByTen(String ten);

    @Query("SELECT DISTINCT ph FROM GiaoVien ph WHERE ph.user.id = :id")
    GiaoVien getByUserId (@Param("id") Long id);
    GiaoVien getGiaoVienById (Long id);
    @Query("SELECT DISTINCT new com.example.backend.api.dto.GiaoVienDTO(gv,true) FROM GiaoVien gv")
    List<GiaoVienDTO> getAll();
}
