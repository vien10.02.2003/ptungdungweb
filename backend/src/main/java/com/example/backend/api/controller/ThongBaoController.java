package com.example.backend.api.controller;

import com.example.backend.api.entity.ThongBao;
import com.example.backend.api.service.ThongBaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/thongbao")
public class ThongBaoController {

    @Autowired
    private ThongBaoService thongBaoService;

    @GetMapping("/all")
    public List<ThongBao> layTatCaThongBao() {
        return thongBaoService.layTatCaThongBao();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ThongBao> layThongBaoTheoId(@PathVariable Long id) {
        Optional<ThongBao> thongBao = thongBaoService.layThongBaoTheoId(id);
        return thongBao.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ThongBao taoThongBao(@RequestBody ThongBao thongBao) {
        return thongBaoService.luuThongBao(thongBao);
    }

}