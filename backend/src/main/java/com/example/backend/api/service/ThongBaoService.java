package com.example.backend.api.service;

import com.example.backend.api.entity.ThongBao;
import com.example.backend.api.repository.ThongBaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ThongBaoService {

    @Autowired
    private ThongBaoRepository thongBaoRepository;

    public List<ThongBao> layTatCaThongBao() {
        return thongBaoRepository.getAll();
    }

    public Optional<ThongBao> layThongBaoTheoId(Long id) {
        return thongBaoRepository.findById(id);
    }

    public ThongBao luuThongBao(ThongBao thongBao) {
        return thongBaoRepository.save(thongBao);
    }
}