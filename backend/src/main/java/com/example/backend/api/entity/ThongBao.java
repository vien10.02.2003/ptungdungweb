package com.example.backend.api.entity;

import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class ThongBao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String tieuDe;
    @Column(length = 10000)
    private String noiDung;
    private LocalDate ngayThongBao;

    // Getters và Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public LocalDate getNgayThongBao() {
        return ngayThongBao;
    }

    public void setNgayThongBao(LocalDate ngayThongBao) {
        this.ngayThongBao = ngayThongBao;
    }

    public void thongBaoMoLop(String tenlop, String noidung) {
        this.ngayThongBao = LocalDate.now();
        this.tieuDe = "Mở lớp "+tenlop;
        this.noiDung = noidung;
    }
}

