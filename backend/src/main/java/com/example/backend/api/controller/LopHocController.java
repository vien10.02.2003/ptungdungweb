package com.example.backend.api.controller;

import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.service.LopHocService;
import com.example.backend.security.service.SecurityService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lophoc")
public class LopHocController {
    @Autowired
    private LopHocService lopHocService;

    @Autowired
    private SecurityService securityService;

    @PostMapping("them")
    public LopHocDTO createLopHoc(@RequestBody LopHocDTO newlopHoc){
        if (securityService.isAdmin())
            return lopHocService.addLopHoc(newlopHoc);
        else
            return null;
    }

    @GetMapping("/all")
    public List<LopHocDTO> getLopHoc(){
        if (securityService.isAdmin())
            return lopHocService.getAllLopHoc();
        else
            return null;
    }

    @GetMapping("")
    public LopHocDTO getOneLopHoc(@RequestParam("id") Long id){
        if (securityService.isAdmin()|| securityService.isTeacher())
            return lopHocService.getOneLopHoc(id);
        else
            return null;
    }

    @GetMapping("/byten")
    public List<LopHocDTO> getLopHocTheoTen(@RequestParam("monHoc") String monHoc){
        if (securityService.isAdmin())
            return lopHocService.getLopHocByTen(monHoc);
        else
            return null;
    }
//    @GetMapping("/NotHaveTeacher")
//    public List<LopHocDTO> getLopHocNotHaveTeacher() {
//        if (securityService.isAdmin())
//            return lopHocService.getLopHocNotHaveTeacher();
//        else return null;
//    }


    @GetMapping("/giaovien/{id}")
    public List<LopHocDTO> getLopHocTheoGiaoVien(@PathVariable("id") Long giaoVienId){
        if (securityService.isAdmin()|| securityService.isTeacher())
            return lopHocService.getLopHocTheoGiaoVien(giaoVienId);
        else
            return null;
    }

    @PutMapping("/update")
    public LopHocDTO updateLopHoc(@RequestParam("id") Long id, @RequestBody LopHocDTO lopHoc){
        if (securityService.isAdmin())
            return lopHocService.updateLopHoc(id,lopHoc);
        else
            return null;
    }

    @DeleteMapping("")
    public Boolean deleteLopHoc(@RequestParam("id") Long id){
        if (securityService.isAdmin())
            return lopHocService.deleteLopHoc(id);
        else
            return false;
    }

    @PostMapping("/them-giao-vien")
    public LopHocDTO themGiaoVien(@RequestParam Long maLop, @RequestParam Long maGiaoVien,@RequestBody LopHocDTO lopHocDTO){
        if (securityService.isAdmin())
            return lopHocService.themGiaoVien(maLop, maGiaoVien, lopHocDTO);
        else
            return null;
    }

}
