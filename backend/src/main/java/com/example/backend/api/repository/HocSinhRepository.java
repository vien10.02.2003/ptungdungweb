package com.example.backend.api.repository;

import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.PhuHuynh;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "hoc-vien")
public interface HocSinhRepository extends JpaRepository<HocSinh, Long> {
    @Query("SELECT DISTINCT h FROM HocSinh h ORDER BY SUBSTRING_INDEX(h.ten, ' ', -1) ASC")
    List<HocSinh> findAllHocSinhSapXepTheoTen();

    @Query("SELECT DISTINCT h FROM HocSinh h WHERE LOWER(h.ten) LIKE LOWER(CONCAT('%', :ten, '%'))")
    List<HocSinh> findHocSinhByTen(String ten);

    @Query("SELECT DISTINCT ph FROM HocSinh ph WHERE ph.user.id = :id")
    HocSinh getByUserId (@Param("id") Long id);
    HocSinh getHocSinhById(Long id);
}
