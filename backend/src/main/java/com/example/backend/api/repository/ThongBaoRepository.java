package com.example.backend.api.repository;

import com.example.backend.api.entity.PhuHuynh;
import com.example.backend.api.entity.ThongBao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ThongBaoRepository extends JpaRepository<ThongBao, Long> {
    @Query("SELECT tb FROM ThongBao tb ORDER BY tb.ngayThongBao DESC")
    List<ThongBao> getAll();
}