package com.example.backend.api.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.Set;

@Entity
@Table(name = "giaovien")
public class GiaoVien extends Person {
    private Integer tienDaNhan;
    private Integer tienChuaNhan;

    @OneToMany(mappedBy = "giaoVien", cascade ={CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH} )
    private Set<LopHoc> lopHoc;

    public Integer getTienDaNhan() {
        return tienDaNhan;
    }

    public void setTienDaNhan(Integer tienDaNhan) {
        this.tienDaNhan = tienDaNhan;
    }

    public Integer getTienChuaNhan() {
        return tienChuaNhan;
    }

    public void setTienChuaNhan(Integer tienChuaNhan) {
        this.tienChuaNhan = tienChuaNhan;
    }

    public Set<LopHoc> getLopHoc() {
        return lopHoc;
    }

    public void setLopHoc(Set<LopHoc> lopHoc) {
        this.lopHoc = lopHoc;
    }
}
