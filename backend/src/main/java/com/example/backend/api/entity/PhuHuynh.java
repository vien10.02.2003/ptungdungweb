package com.example.backend.api.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "phuhuynh")
public class PhuHuynh extends Person {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hocsinh_id", referencedColumnName = "id")
    private HocSinh hocSinh;

    public HocSinh getHocSinh() {
        return hocSinh;
    }

    public void setHocSinh(HocSinh hocSinh) {
        this.hocSinh = hocSinh;
    }
}
