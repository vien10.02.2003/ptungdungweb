package com.example.backend.api.service;

import com.example.backend.api.dto.HocSinhDTO;
import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.dto.PhuHuynhDTO;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.LopHoc;
import com.example.backend.api.entity.PhuHuynh;
import com.example.backend.api.repository.HocSinhRepository;
import com.example.backend.api.repository.LopHocRepository;
import com.example.backend.api.repository.PhuHuynhRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PhuHuynhService {
    @Autowired
    private PhuHuynhRepository phuHuynhRepository;
    @Autowired
    private HocSinhRepository hocSinhRepository;
    @Autowired
    private LopHocRepository lopHocRepository;

    public PhuHuynhDTO addPhuHuynh(PhuHuynhDTO phuHuynh) {
        if (phuHuynh != null) {
            HocSinh hocSinh = null;
            if (phuHuynh.getHocSinh() != null){
                hocSinh = hocSinhRepository.getHocSinhById(phuHuynh.getHocSinh().getId());
            }
            phuHuynhRepository.save(phuHuynh.convertToEntity(null, hocSinh));
            return phuHuynh;
        }
        return null;
    }

    public PhuHuynhDTO updatePhuHuynh(Long id, PhuHuynhDTO phuHuynh){
        if(id>=1){
            PhuHuynh phuHuynh1 = phuHuynhRepository.getPhuHuynhById(id);
            if(phuHuynh1 != null){
                HocSinh hocSinh = null;
                if (phuHuynh.getHocSinh() != null){
                    hocSinh = hocSinhRepository.getHocSinhById(phuHuynh.getHocSinh().getId());
                }
                phuHuynhRepository.save(phuHuynh.convertToEntity(phuHuynh1, hocSinh));
                return phuHuynh;
            }
        }
        return null;
    }

    public Boolean deletePhuHuynh(Long id){
        if(id>= 1){
            phuHuynhRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<PhuHuynhDTO> getAllPhuHuynh(){
        return phuHuynhRepository.findAll().stream()
                .map(phuHuynh -> new PhuHuynhDTO(phuHuynh, true))
                .collect(Collectors.toList());
    }

    public Integer getTienChuaDong(Long  id){
        if (id>=1){
            Integer tongTienLopHoc = 0;
            Integer tienDaDong = null;
            PhuHuynh phuHuynh = phuHuynhRepository.getPhuHuynhById(id);
            if (phuHuynh != null){
                PhuHuynhDTO phuHuynhDTO = new PhuHuynhDTO(phuHuynh ,true);
                HocSinh hocSinh = hocSinhRepository.getHocSinhById(phuHuynhDTO.getHocSinh().getId());
                HocSinhDTO hocSinhDTO = new HocSinhDTO(hocSinh,true,false, false);
                tienDaDong = hocSinhDTO.getTienDaDong();
                if (hocSinhDTO !=null){
                    if (hocSinhDTO.getLopHoc() != null){
                        for (LopHocDTO lopHoc : hocSinhDTO.getLopHoc()){
                            LopHoc lopHoc1 = lopHocRepository.getLopHocById(lopHoc.getId());
                            LopHocDTO lopHocDTO = new LopHocDTO(lopHoc1,false, false);
                            tongTienLopHoc = tongTienLopHoc + lopHocDTO.getSoTien();
                        }
                    }
                }
                tongTienLopHoc = tongTienLopHoc* hocSinhDTO.getHeSoDongTien();
            }
            return tongTienLopHoc - tienDaDong;
        }
        return null;
    }
}
