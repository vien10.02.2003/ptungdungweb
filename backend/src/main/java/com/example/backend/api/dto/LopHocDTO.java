package com.example.backend.api.dto;

import com.example.backend.api.entity.DiemDanh;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.LopHoc;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;
@Data
public class LopHocDTO {
    private Long id;
    private String monHoc;
    private Integer soTien;
    private Integer soKhoaHoc;
    private Integer soLuongHocSinh;
    private Boolean isActive;
    private Set<HocSinhDTO> hocSinh;//kiểm tra many to many
    private Long giaoVienId;
    private Set<DiemDanhDTO> diemDanh;
    private String ngayBatDau;
    private String ngayKetThuc;
    private String lichHoc;

    public LopHocDTO() {
    }

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }

    public LopHocDTO(LopHoc entity, Boolean coHocSinh, Boolean coDiemDanh) {
        if (entity == null){ return;}
        this.id = entity.getId();
        this.monHoc = entity.getMonHoc();
        this.soTien = entity.getSoTien();
        this.soKhoaHoc = entity.getSoKhoaHoc();
        this.soLuongHocSinh = entity.getSoLuongHocSinh();
        this.isActive = entity.getIsActive();
        if (coHocSinh){
            if (entity.getHocSinh() != null){
                Set<HocSinhDTO> hocSinhDtoSet = new HashSet<>();
                for (HocSinh element : entity.getHocSinh()) {
                    hocSinhDtoSet.add(new HocSinhDTO(element,false,false, false));
                }
                this.hocSinh = hocSinhDtoSet;
            }
        }
        this.giaoVienId = entity.getGiaoVien() != null ? entity.getGiaoVien().getId() : null;
        if (coDiemDanh){
            if (entity.getDiemDanh() != null){
                Set<DiemDanhDTO> diemDanhDtoSet = new HashSet<>();
                for (DiemDanh element : entity.getDiemDanh()) {
                    diemDanhDtoSet.add(new DiemDanhDTO(element));
                }
                this.diemDanh = diemDanhDtoSet;
            }
        }
        if (entity.getNgayBatDau() != null)
            this.ngayBatDau = localDateToString(entity.getNgayBatDau());
        if (entity.getNgayKetThuc() != null)
            this.ngayKetThuc = localDateToString(entity.getNgayKetThuc());
        this.lichHoc = entity.getLichHoc();
    }

    public LopHoc convertToEntity(LopHoc entity, Set<HocSinh> hocsinh, Set<DiemDanh> diemdanh) {

        if(entity == null)
            entity = new LopHoc();
        if (this.getMonHoc() != null)
            entity.setMonHoc(this.getMonHoc());
        if (this.getSoKhoaHoc() != null)
            entity.setSoKhoaHoc(this.getSoKhoaHoc());
        if (this.getSoLuongHocSinh() != null)
            entity.setSoLuongHocSinh(this.getSoLuongHocSinh());
        if (this.getSoTien() != null)
            entity.setSoTien(this.getSoTien());
        if (this.getIsActive() != null)
            entity.setIsActive(this.getIsActive());
        if (this.getNgayBatDau() != null)
            entity.setNgayBatDau(stringToLocalDate(this.getNgayBatDau()));
        if (this.getNgayKetThuc() != null)
            entity.setNgayKetThuc(stringToLocalDate(this.getNgayKetThuc()));
        if (this.getLichHoc() != null)
            entity.setLichHoc(this.getLichHoc());
        
        if (hocsinh != null){
            for (HocSinh element : hocsinh){
                element.getLopHoc().add(entity);
            }
            entity.setHocSinh(hocsinh);
        }
        if (diemdanh != null){
            for (DiemDanh element : diemdanh){
                element.setLopHoc(entity);
            }
            entity.setDiemDanh(diemdanh);
        }
        return entity;
    }
}
