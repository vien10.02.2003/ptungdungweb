package com.example.backend.api.dto;

import com.example.backend.api.entity.*;
import lombok.Data;
import org.springframework.security.core.parameters.P;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Data
public class PhuHuynhDTO {
    private Long id;
    private String ten;
    private String ngaySinh;
    private String SDT;
    private String diaChi;
    private String email;
    private String gioiTinh;
    private HocSinhDTO hocSinh;
    private UserDTO user;

    public PhuHuynhDTO() {
    }

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }

    public PhuHuynhDTO(PhuHuynh entity, Boolean coHocSinh) {
        if (entity == null){ return;}
        this.id = entity.getId();
        this.ten = entity.getTen();
        if (entity.getNgaySinh() != null)
            this.ngaySinh = localDateToString(entity.getNgaySinh());
        this.SDT = entity.getSDT();
        this.diaChi = entity.getDiaChi();
        this.email = entity.getEmail();
        this.gioiTinh = entity.getGioiTinh();
        if(coHocSinh){
            this.hocSinh = entity.getHocSinh() != null ? new HocSinhDTO(entity.getHocSinh(),false,false, false) : null;
        }
        this.user = entity.getUser() != null ? new UserDTO(entity.getUser()) : null;
    }

    public PhuHuynh convertToEntity(PhuHuynh entity, HocSinh hocsinh) {

        if(entity == null)
            entity = new PhuHuynh();
        if(this.getTen() != null)
            entity.setTen(this.getTen());
        if(this.getNgaySinh() != null)
            entity.setNgaySinh(stringToLocalDate(this.getNgaySinh()));
        if(this.getSDT() != null)
            entity.setSDT(this.getSDT());
        if(this.getDiaChi() != null)
            entity.setDiaChi(this.getDiaChi());
        if(this.getEmail() != null)
            entity.setEmail(this.getEmail());
        if(this.getGioiTinh() != null)
            entity.setGioiTinh(this.getGioiTinh());

        if (hocsinh != null){
            hocsinh.setPhuHuynh(entity);
            entity.setHocSinh(hocsinh);
        }
        return entity;
    }
}
