package com.example.backend.api.service;

import com.example.backend.api.dto.DiemDanhDTO;
import com.example.backend.api.entity.DiemDanh;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.LopHoc;
import com.example.backend.api.repository.DiemDanhRepository;
import com.example.backend.api.repository.HocSinhRepository;
import com.example.backend.api.repository.LopHocRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DiemDanhService {
    @Autowired
    private HocSinhRepository hocSinhRepository;
    @Autowired
    private LopHocRepository lopHocRepository;
    @Autowired
    private DiemDanhRepository diemDanhRepository;

    public DiemDanhDTO addDiemDanh(DiemDanhDTO diemDanhDTO){
        if (diemDanhRepository.existsByNgayAndLopHocIdAndHocSinhId
                (diemDanhDTO.stringToLocalDate(diemDanhDTO.getNgay())
                        , diemDanhDTO.getLopHocId()
                        , diemDanhDTO.getHocSinhId()))
            throw new RuntimeException("Diem danh da ton tai!");

        if(diemDanhDTO != null){
            HocSinh hocSinh = null;
            if(diemDanhDTO.getHocSinhId() != null){
                hocSinh = hocSinhRepository.getHocSinhById(diemDanhDTO.getHocSinhId());
            }
            LopHoc lopHoc = null;
            if (diemDanhDTO.getLopHocId() != null){
                lopHoc = lopHocRepository.getLopHocById(diemDanhDTO.getLopHocId());
            }
            diemDanhRepository.save(diemDanhDTO.convertToEntity(null, lopHoc, hocSinh));
            return diemDanhDTO;
        }
        return null;
    }

    public List<DiemDanhDTO> addDanhSachDiemDanh(List<DiemDanhDTO> diemDanhDTOList){
        if(diemDanhDTOList != null){
            HocSinh hocSinh = null;
            for(DiemDanhDTO diemDanhDTO : diemDanhDTOList){
                if (diemDanhRepository.existsByNgayAndLopHocIdAndHocSinhId
                        (diemDanhDTO.stringToLocalDate(diemDanhDTO.getNgay())
                                , diemDanhDTO.getLopHocId()
                                , diemDanhDTO.getHocSinhId()))
                    break;
                if(diemDanhDTO.getHocSinhId() != null){
                    hocSinh = hocSinhRepository.getHocSinhById(diemDanhDTO.getHocSinhId());
                }
                LopHoc lopHoc = null;
                if (diemDanhDTO.getLopHocId() != null){
                    lopHoc = lopHocRepository.getLopHocById(diemDanhDTO.getLopHocId());
                }
                diemDanhRepository.save(diemDanhDTO.convertToEntity(null, lopHoc, hocSinh));
            }
            return diemDanhDTOList;
        }
        return null;
    }

    public DiemDanhDTO updateDiemDanh(Long id, DiemDanhDTO diemDanhDTO){
        if(id>= 1){
            DiemDanh diemDanh = diemDanhRepository.getDiemDanhById(id);
            if(diemDanh != null){
                HocSinh hocSinh = null;
                if(diemDanhDTO.getHocSinhId() != null){
                    hocSinh = hocSinhRepository.getHocSinhById(diemDanhDTO.getHocSinhId());
                }
                LopHoc lopHoc = null;
                if (diemDanhDTO.getLopHocId() != null){
                    lopHoc = lopHocRepository.getLopHocById(diemDanhDTO.getLopHocId());
                }
                diemDanhRepository.save(diemDanhDTO.convertToEntity(diemDanh, lopHoc, hocSinh));
                return diemDanhDTO;
            }
        }
        return null;
    }
    public Boolean deleteDiemDanh(Long id){
        if (id >= 1){
            diemDanhRepository.deleteById(id);
            return true;
        }
        return false;
    }
    public List<DiemDanhDTO> getAllDiemDanh(){
        List<DiemDanh> diemDanhList = diemDanhRepository.findAll();
        return diemDanhList.stream()
                .map(diemDanh -> new DiemDanhDTO(diemDanh))
                .collect(Collectors.toList());
    }

    public List<DiemDanhDTO> getDiemDanhByLopHocIdAndThoiGian(Long lopHocId, LocalDate start, LocalDate end){
        return diemDanhRepository.findDiemDanhByLopHocIdAndThoiGianBetween(lopHocId, start, end);
    }

    public List<DiemDanhDTO> getDiemDanhByHocSinhIdAndThoiGian(Long hocSinhId, Long lopHocId, LocalDate start, LocalDate end){
        return diemDanhRepository.findDiemDanhByHocSinhIdAndThoiGianBetween(hocSinhId, lopHocId, start, end);
    }
}
