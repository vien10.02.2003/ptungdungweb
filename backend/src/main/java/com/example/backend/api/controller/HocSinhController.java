package com.example.backend.api.controller;

import com.example.backend.api.dto.HocSinhDTO;
import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.service.HocSinhService;
import com.example.backend.security.service.SecurityService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/hocsinh")
public class HocSinhController {
    @Autowired
    HocSinhService hocSinhService;

    @Autowired
    private SecurityService securityService;

    @PostMapping("")
    public HocSinhDTO addHocSinh(@RequestBody HocSinhDTO hocSinhDTO){
        if (securityService.isAdmin()){
            return hocSinhService.addHocSinh(hocSinhDTO);
        }
        else
            return null;
    }

    @GetMapping("/all")
    public List<HocSinhDTO> getAllHocSinh(){
        if (securityService.isAdmin())
            return hocSinhService.getAllHocSinh();
        else
            return null;
    }
    @GetMapping("")
    public HocSinhDTO getOneHocSinh(@RequestParam("id") long id){
        if (securityService.isAdmin())
            return hocSinhService.getOneHocSinh(id);
        else return null;
    }
    
    @PutMapping("/cap-nhat")
    public HocSinhDTO updateHocSinh(@RequestParam("id") Long id,@RequestBody HocSinhDTO hocSinh){

            return hocSinhService.updateHocSinh(id, hocSinh);
       
    }
    @PutMapping("/them-hoc-phi")
    public ResponseEntity<?> updateHocPhi(@RequestBody JsonNode jsonNode){
       return hocSinhService.updateHocPhi(jsonNode);
    }

    @PostMapping("/dangkylophoc")
    public Boolean dangKyLopHoc(@RequestParam Long maHocSinh, @RequestParam Long maLop){
        if (securityService.isAdmin())
            return hocSinhService.dangKyLopHoc(maHocSinh, maLop);
        return false;
    }

    @DeleteMapping("")
    public Boolean deleteHocSinh(@RequestParam("id") Long id){

        if (securityService.isAdmin())
            return hocSinhService.deleteHocSinh(id);
        else
            return false;
    }
    @PostMapping("/them-lop-hoc")
    public LopHocDTO themLopHoc(@RequestParam Long maLop, @RequestParam Long maHocSinh, @RequestBody LopHocDTO lopHocDTO){
        if (securityService.isAdmin())
            return hocSinhService.themLopHoc(maLop, maHocSinh, lopHocDTO);
        else
            return null;
    }

}
