package com.example.backend.api.service;

import com.example.backend.api.dto.DiemDanhDTO;
import com.example.backend.api.dto.HocSinhDTO;
import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.entity.*;
import com.example.backend.api.repository.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class LopHocService {
    @Autowired
    private LopHocRepository lopHocRepository;
    @Autowired
    private HocSinhRepository hocSinhRepository;
    @Autowired
    private GiaoVienRepository giaoVienRepository;
    @Autowired
    private DiemDanhRepository diemDanhRepository;
    @Autowired
    private ThongBaoRepository thongBaoRepository;

    private final ObjectMapper objectMapper;

    public LopHocService(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    public LopHocDTO addLopHoc(LopHocDTO newLopHoc) {
        if(newLopHoc != null){
            if(lopHocRepository.existsBymonHoc(newLopHoc.getMonHoc()))
                throw new RuntimeException("Lop hoc da ton tai");
            lopHocRepository.save(newLopHoc.convertToEntity(null, null, null));
            ThongBao thongBao = new ThongBao();
            thongBao.thongBaoMoLop(newLopHoc.getMonHoc(),
                    "Kính gửi các học viên,\n" +
                            "Chúng tôi rất vui mừng thông báo về việc mở lớp học mới cho khóa học "+newLopHoc.getMonHoc()+". " +
                            "Khóa học này sẽ giúp các học viên nắm vững các kiến thức cơ bản, nâng cao và hơn thế nữa, " +
                            "Thông tin chi tiết về lớp học như sau:\n" +
                            "- **Tên khóa học**: "+newLopHoc.getMonHoc()+"\n" +
                            "- **Giảng viên**: Nguyễn Văn A\n" +
                            "- **Thời gian**: Bắt đầu từ ngày 01/07/2024, mỗi "+newLopHoc.getLichHoc()+"\n" +
                            "- **Địa điểm**: Phòng 101, Tòa nhà B, Trường Học viện mật mã\n\n" +
                            "Để đăng ký tham gia khóa học, vui lòng truy cập vào website của chúng tôi hoặc liên hệ trực tiếp với phòng đào tạo qua email: daotao@university.edu.vn.\n\n" +
                            "Chúng tôi mong đợi sự tham gia nhiệt tình của các bạn.\n\n" +
                            "Trân trọng,\n" +
                            "Ban Quản lý Đào tạo");
            thongBaoRepository.save(thongBao);
            return newLopHoc;
        }
        return null;
    }


    public LopHocDTO updateLopHoc(Long id, LopHocDTO lopHoc){
        if(lopHoc != null){
            LopHoc lopHoc1 = lopHocRepository.getLopHocById(id);
                if(lopHoc1 != null){

                    Set<HocSinh> hocSinhSet = null;
                    if (lopHoc.getHocSinh() != null){
                        hocSinhSet = new HashSet<>();
                        for (HocSinhDTO hocSinhDTO : lopHoc.getHocSinh()){
                            hocSinhSet.add(hocSinhRepository.getHocSinhById(hocSinhDTO.getId()));
                        }
                    }
                    Set<DiemDanh> diemDanhSet = null;
                    if (lopHoc.getDiemDanh() != null){
                        diemDanhSet = new HashSet<>();
                        for (DiemDanhDTO diemDanhDTO : lopHoc.getDiemDanh()){
                            diemDanhSet.add(diemDanhRepository.getDiemDanhById(diemDanhDTO.getId()));
                        }
                    }
                    lopHocRepository.save(lopHoc.convertToEntity(lopHoc1, hocSinhSet, diemDanhSet));
                    return lopHoc;
                }
        }
        return null;
    }
    public LopHocDTO themGiaoVien(Long maLop, Long maGiaoVien, LopHocDTO lopHoc){
        if(lopHoc != null){
            LopHoc lopHoc1 = lopHocRepository.getLopHocById(maLop);
            if(lopHoc1 != null){
GiaoVien giaoVien = giaoVienRepository.getGiaoVienById(maGiaoVien);
lopHoc1.setGiaoVien(giaoVien);
                Set<HocSinh> hocSinhSet = null;
                if (lopHoc.getHocSinh() != null){
                    hocSinhSet = new HashSet<>();
                    for (HocSinhDTO hocSinhDTO : lopHoc.getHocSinh()){
                        hocSinhSet.add(hocSinhRepository.getHocSinhById(hocSinhDTO.getId()));
                    }
                }
                Set<DiemDanh> diemDanhSet = null;
                if (lopHoc.getDiemDanh() != null){
                    diemDanhSet = new HashSet<>();
                    for (DiemDanhDTO diemDanhDTO : lopHoc.getDiemDanh()){
                        diemDanhSet.add(diemDanhRepository.getDiemDanhById(diemDanhDTO.getId()));
                    }
                }
                lopHocRepository.save(lopHoc.convertToEntity(lopHoc1, hocSinhSet, diemDanhSet));
                return lopHoc;
            }
        }
        return null;
    }

    public Boolean deleteLopHoc(Long id){
        if(id>=1){
            LopHoc lopHoc = lopHocRepository.getLopHocById(id);
            if(lopHoc != null){
                lopHocRepository.delete(lopHoc);
                return true;
            }
        }
        return false;
    }

    public List<LopHocDTO> getAllLopHoc(){
        return lopHocRepository.findAll().stream()
                .map(lopHoc -> new LopHocDTO(lopHoc, true, false))
                .collect(Collectors.toList());
    }

    public List<LopHocDTO> getLopHocByTen(String monHoc){
        return lopHocRepository.findLopHocByTen(monHoc);
    }

    public List<LopHocDTO> getLopHocTheoGiaoVien(Long giaoVienId){
        List<LopHocDTO> dto = new ArrayList<>();
        List<LopHoc> entity = lopHocRepository.getLopHocByGiaoVien(giaoVienId);
        for (LopHoc lopHoc: entity) {
            dto.add(new LopHocDTO(lopHoc, true, false));
        }
        return dto;
    }

    public LopHocDTO getOneLopHoc(Long id){
        return new LopHocDTO(lopHocRepository.getLopHocById(id),true, true);
    }

    private String formatStringByJson(String json) {
        return json.replaceAll("\"", "");
    }
//    public List<LopHocDTO> getLopHocNotHaveTeacher() {
//        if (lopHocRepository.findLopHocNotHaveTeacher() != null)
//        return lopHocRepository.findLopHocNotHaveTeacher();
//        else return null;
//    }

}

