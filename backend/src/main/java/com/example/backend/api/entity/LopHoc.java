package com.example.backend.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "lophoc")
public class LopHoc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,length = 50)
    private String monHoc;

    private Integer soTien;

    private Integer soKhoaHoc;

    private Integer soLuongHocSinh;

    private Boolean isActive;

    @ManyToMany(mappedBy = "lopHoc")
    private Set<HocSinh> hocSinh;

    @ManyToOne(cascade ={CascadeType.DETACH,CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinColumn(name = "giaovien_id",nullable = true)
    private GiaoVien giaoVien;

    @OneToMany(mappedBy = "lopHoc", cascade = CascadeType.ALL)
    private Set<DiemDanh> diemDanh;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate ngayBatDau;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate ngayKetThuc;
    private String lichHoc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public Integer getSoTien() {
        return soTien;
    }

    public void setSoTien(Integer soTien) {
        this.soTien = soTien;
    }

    public Integer getSoKhoaHoc() {
        return soKhoaHoc;
    }

    public void setSoKhoaHoc(Integer soKhoaHoc) {
        this.soKhoaHoc = soKhoaHoc;
    }

    public Integer getSoLuongHocSinh() {
        return soLuongHocSinh;
    }

    public void setSoLuongHocSinh(Integer soLuongHocSinh) {
        this.soLuongHocSinh = soLuongHocSinh;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Set<HocSinh> getHocSinh() {
        return hocSinh;
    }

    public void setHocSinh(Set<HocSinh> hocSinh) {
        this.hocSinh = hocSinh;
    }

    public GiaoVien getGiaoVien() {
        return giaoVien;
    }

    public void setGiaoVien(GiaoVien giaoVien) {
        this.giaoVien = giaoVien;
    }

    public Set<DiemDanh> getDiemDanh() {
        return diemDanh;
    }

    public void setDiemDanh(Set<DiemDanh> diemDanh) {
        this.diemDanh = diemDanh;
    }

    public LocalDate getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(LocalDate ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public LocalDate getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(LocalDate ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public String getLichHoc() {
        return lichHoc;
    }

    public void setLichHoc(String lichHoc) {
        this.lichHoc = lichHoc;
    }
}

