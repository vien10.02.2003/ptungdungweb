package com.example.backend.api.repository;

import com.example.backend.api.entity.PhuHuynh;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "phu-huynh")
public interface PhuHuynhRepository extends JpaRepository<PhuHuynh, Long> {
    @Query("SELECT DISTINCT ph FROM PhuHuynh ph WHERE ph.user.id = :id")
    PhuHuynh getByUserId (@Param("id") Long id);

    PhuHuynh getPhuHuynhById (Long id);
}
