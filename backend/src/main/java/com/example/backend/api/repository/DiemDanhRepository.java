package com.example.backend.api.repository;

import com.example.backend.api.dto.DiemDanhDTO;
import com.example.backend.api.entity.DiemDanh;
import com.example.backend.api.entity.LopHoc;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@RepositoryRestResource(path="diem-danh")
public interface DiemDanhRepository  extends JpaRepository<DiemDanh, Long> {
    DiemDanh getDiemDanhById(Long id);

    boolean existsByNgayAndLopHocIdAndHocSinhId(LocalDate ngay, Long lopHocId, Long hocSinhId);

    @Query("SELECT DISTINCT new com.example.backend.api.dto.DiemDanhDTO(d) FROM DiemDanh d WHERE d.lopHoc.id = :lopHocId AND d.ngay BETWEEN :start AND :end ORDER BY d.ngay")
    List<DiemDanhDTO> findDiemDanhByLopHocIdAndThoiGianBetween(@Param("lopHocId") Long lopHocId,
                                                               @Param("start") LocalDate start,
                                                               @Param("end") LocalDate end);
    @Query("SELECT DISTINCT new com.example.backend.api.dto.DiemDanhDTO(d) FROM DiemDanh d WHERE d.hocSinh.id = :hocSinhId AND d.lopHoc.id = :lopHocId AND d.ngay BETWEEN :start AND :end ORDER BY d.ngay")
    List<DiemDanhDTO> findDiemDanhByHocSinhIdAndThoiGianBetween(@Param("hocSinhId") Long hocSinhId,
                                                                @Param("lopHocId") Long lopHocId,
                                                               @Param("start") LocalDate start,
                                                               @Param("end") LocalDate end);

    @Modifying
    @Transactional
    @Query("SELECT DISTINCT i FROM DiemDanh i WHERE i.lopHoc.id=:lopHocId AND i.hocSinh.id=:hocSinhId")
    List<DiemDanh> findDiemDanhByLopHocIdAndHocSinhId(@Param("lopHocId") Long lopHocId,@Param("hocSinhId") Long hocSinhId);
}
