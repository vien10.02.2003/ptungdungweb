package com.example.backend.api.controller;

import com.example.backend.api.dto.DiemDanhDTO;
import com.example.backend.api.service.DiemDanhService;
import com.example.backend.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping("/diemdanh")
public class DiemDanhController {
    @Autowired
    private SecurityService securityService;
    @Autowired
    private DiemDanhService diemDanhService;

    @PostMapping("")
    public DiemDanhDTO addDiemDanh(@RequestBody DiemDanhDTO diemDanhDTO){
        if (securityService.isAdmin() || securityService.isTeacher()){
            return diemDanhService.addDiemDanh(diemDanhDTO);
        }
        return null;
    }
    @PostMapping("/all")
    public List<DiemDanhDTO> addDanhSachDiemDanh(@RequestBody List<DiemDanhDTO> diemDanhDTOList){
        if (securityService.isAdmin() || securityService.isTeacher()){
            return diemDanhService.addDanhSachDiemDanh(diemDanhDTOList);
        }
        return null;
    }
    @GetMapping("/all")
    public List<DiemDanhDTO> getAllDiemDanh(){
        if (securityService.isAdmin() || securityService.isTeacher()){
            return diemDanhService.getAllDiemDanh();
        }
        return null;
    }

    @GetMapping("/diemDanh")
    public List<DiemDanhDTO> getDiemDanh(@RequestParam Long lopHocId,
                                     @RequestParam String startTime,
                                     @RequestParam String endTime) {
        LocalDate start = this.stringToLocalDate(startTime);
        LocalDate end = this.stringToLocalDate(endTime);
        return diemDanhService.getDiemDanhByLopHocIdAndThoiGian(lopHocId, start, end);
    }

    @GetMapping("/hocsinh")
    public List<DiemDanhDTO> getDiemDanhByHocSinhId(@RequestParam Long hocSinhId,
                                                    @RequestParam Long lopHocId,
                                         @RequestParam String startTime,
                                         @RequestParam String endTime) {
        LocalDate start = this.stringToLocalDate(startTime);
        LocalDate end = this.stringToLocalDate(endTime);
        return diemDanhService.getDiemDanhByHocSinhIdAndThoiGian(hocSinhId, lopHocId, start, end);
    }

    @PutMapping("")
    public DiemDanhDTO updateDiemDanh(@RequestParam("id") Long id, @RequestBody DiemDanhDTO diemDanhDTO){
        if (securityService.isAdmin() || securityService.isTeacher())
            return diemDanhService.updateDiemDanh(id, diemDanhDTO);
        return null;
    }
    @DeleteMapping("")
    public Boolean deleteDiemDanh(@RequestParam("id") Long id){
        if (securityService.isAdmin() || securityService.isTeacher()){
            return diemDanhService.deleteDiemDanh(id);
        }
        return null;
    }

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }
}
