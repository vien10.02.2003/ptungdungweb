package com.example.backend.api.dto;

import com.example.backend.api.entity.GiaoVien;
import com.example.backend.api.entity.LopHoc;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;

@Data
public class GiaoVienDTO {

    private Long id;
    private String ten;

    private String ngaySinh;
    private String SDT;
    private String diaChi;
    private String email;
    private String gioiTinh;
    private Integer tienDaNhan;
    private Integer tienChuaNhan;
    private Set<LopHocDTO> lopHoc;
    private UserDTO user;

    public GiaoVienDTO() {
    }

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }

    public GiaoVienDTO(GiaoVien entity, Boolean coLopHoc) {
        if (entity == null){ return;}
        this.id = entity.getId();
        this.ten = entity.getTen();
        if (entity.getNgaySinh() != null)
            this.ngaySinh = localDateToString(entity.getNgaySinh());
        this.SDT = entity.getSDT();
        this.diaChi = entity.getDiaChi();
        this.email = entity.getEmail();
        this.gioiTinh = entity.getGioiTinh();
        this.tienDaNhan = entity.getTienDaNhan();
        this.tienChuaNhan = entity.getTienChuaNhan();
        if(coLopHoc){
            if (entity.getLopHoc() != null){
                Set<LopHocDTO> lopHocDtoSet = new HashSet<>();
                for (LopHoc element : entity.getLopHoc()) {
                    lopHocDtoSet.add(new LopHocDTO(element,false, false));
                }
                this.lopHoc = lopHocDtoSet;
            }
        }
        this.user = entity.getUser() != null ? new UserDTO(entity.getUser()) : null;
    }

    public GiaoVien convertToEntity(GiaoVien entity, Set<LopHoc> lophoc) {
        if(entity == null){
            entity = new GiaoVien();
        }
        if (this.getEmail() != null){
            entity.setEmail(this.getEmail());
        }
        if (this.getTen() != null){
            entity.setTen(this.getTen());
        }
        if (this.getSDT() != null){
            entity.setSDT(this.getSDT());
        }
        if (this.getDiaChi() != null){
            entity.setDiaChi(this.getDiaChi());
        }
        if (this.getGioiTinh() != null){
            entity.setGioiTinh(this.getGioiTinh());
        }
        if (this.getTienChuaNhan() != null){
            entity.setTienChuaNhan(this.getTienChuaNhan());
        }
        if (this.getTienDaNhan() != null){
            entity.setTienDaNhan(this.getTienDaNhan());
        }
        if (this.getNgaySinh() != null){
            entity.setNgaySinh(stringToLocalDate(this.getNgaySinh()));
        }
        if (lophoc != null){
            for (LopHoc element : lophoc){
                element.setGiaoVien(entity);
            }
            entity.setLopHoc(lophoc);
        }
        return entity;
    }
}
