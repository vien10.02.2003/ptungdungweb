package com.example.backend.api.dto;

import com.example.backend.api.entity.DiemDanh;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.LopHoc;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Data
public class DiemDanhDTO {
    private Long id;
    private String ngay;
    private Boolean trangThai;
    private String nhanXet;
    private Long lopHocId;
    private Long hocSinhId;

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }

    public DiemDanhDTO() {
    }

    public DiemDanhDTO(DiemDanh entity) {
        if (entity == null){ return;}
        this.id = entity.getId();
        if (entity.getNgay() != null)
            this.ngay = localDateToString(entity.getNgay());
        this.trangThai = entity.getTrangThai();
        this.nhanXet = entity.getNhanXet();
        this.lopHocId = entity.getLopHoc() != null ? entity.getLopHoc().getId() : null;
        this.hocSinhId = entity.getHocSinh() != null ? entity.getHocSinh().getId() : null;
    }

    public DiemDanh convertToEntity(DiemDanh entity, LopHoc lopHoc, HocSinh hocSinh) {
        if(entity == null){
            entity = new DiemDanh();
        }
        if (this.getNgay() != null){
            entity.setNgay(stringToLocalDate(this.getNgay()));
        }
        if (this.getNhanXet() != null){
            entity.setNhanXet(this.getNhanXet());
        }
        if (this.getTrangThai() != null){
            entity.setTrangThai(this.getTrangThai());
        }
        if (lopHoc != null){
            lopHoc.getDiemDanh().add(entity);
            entity.setLopHoc(lopHoc);
        }
        if (hocSinh != null){
            hocSinh.getDiemDanh().add(entity);
            entity.setHocSinh(hocSinh);
        }
        return entity;
    }
}
