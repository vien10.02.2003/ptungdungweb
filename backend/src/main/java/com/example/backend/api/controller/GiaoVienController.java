package com.example.backend.api.controller;

import com.example.backend.api.dto.GiaoVienDTO;
import com.example.backend.api.service.GiaoVienService;
import com.example.backend.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/giaovien")
public class GiaoVienController {
    @Autowired
    GiaoVienService giaoVienService;

    @Autowired
    private SecurityService securityService;

    @PostMapping("them")
    public GiaoVienDTO addGiaoVien(@RequestBody GiaoVienDTO giaoVien){
        if (securityService.isAdmin())
            return giaoVienService.addGiaoVien(giaoVien);
        else
            return null;
    }

    @GetMapping("/all")
    public List<GiaoVienDTO> getAllGiaoVien(){

        if (securityService.isAdmin())
            return giaoVienService.getAllGiaoVien();
        else
            return null;
    }
    @GetMapping("/allname")
    public List<GiaoVienDTO> getAllGiaoVienSapXepTheoTen(){

        if (securityService.isAdmin())
            return giaoVienService.getAllGiaoVienSapXepTheoTen();
        else
            return null;
    }
    @PutMapping("cap-nhat")
    public GiaoVienDTO updateGiaoVien(@RequestParam("id") Long id, @RequestBody GiaoVienDTO giaoVien){
        if (securityService.isAdmin())
            return giaoVienService.updateGiaoVien(id, giaoVien);
        else
            return null;
    }

    @DeleteMapping("")
    public Boolean deleteGiaoVien(@RequestParam("id") Long id){
        if (securityService.isAdmin())
            return giaoVienService.deleteGiaoVien(id);
        else
            return false;
    }
}
