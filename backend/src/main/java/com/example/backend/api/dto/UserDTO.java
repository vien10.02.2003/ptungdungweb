package com.example.backend.api.dto;

import com.example.backend.security.entity.User;
import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String username;
    private String role;

    public UserDTO() {
    }

    public UserDTO(User entity) {
        if (entity == null){
            return;
        }
        this.id = entity.getId();
        this.username = entity.getUsername();
        this.role = entity.getRole();
    }
}
