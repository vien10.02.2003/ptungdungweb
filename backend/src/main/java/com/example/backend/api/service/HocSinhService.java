package com.example.backend.api.service;

import com.example.backend.api.dto.DiemDanhDTO;
import com.example.backend.api.dto.HocSinhDTO;
import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.entity.*;
import com.example.backend.api.dto.PhuHuynhDTO;
import com.example.backend.api.entity.DiemDanh;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.LopHoc;
import com.example.backend.api.entity.PhuHuynh;
import com.example.backend.api.repository.DiemDanhRepository;
import com.example.backend.api.repository.HocSinhRepository;
import com.example.backend.api.repository.LopHocRepository;
import com.example.backend.api.repository.PhuHuynhRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HocSinhService {
    @Autowired
    private HocSinhRepository hocSinhRepository;
    @Autowired
    private LopHocRepository lopHocRepository;
    @Autowired
    private DiemDanhRepository diemDanhRepository;
    @Autowired
    private PhuHuynhRepository phuHuynhRepository;
    private final ObjectMapper objectMapper;

    public HocSinhService(ObjectMapper objectMapper){
        this.objectMapper  = objectMapper;
    }
    public HocSinhDTO addHocSinh(HocSinhDTO hocSinh) {//lúc tạo mới chưa cho vào lớp và điểm danh
        if (hocSinh != null) {
            PhuHuynh phuHuynh = null;
            PhuHuynhDTO phuHuynhDTO = null;
            PhuHuynh phuHuynh1 = null;
            Boolean kiemTra = false;
            if (hocSinh.getPhuHuynh() != null){
                if (hocSinh.getPhuHuynh().getId() != null)
                    phuHuynh = phuHuynhRepository.getPhuHuynhById(hocSinh.getPhuHuynh().getId());
                else {
                    phuHuynhDTO = hocSinh.getPhuHuynh();
                    if (phuHuynhDTO != null){
                        phuHuynh1 = phuHuynhRepository.save(phuHuynhDTO.convertToEntity(null,null));
                        kiemTra = true;
                    }
                }
            }
            HocSinh hocSinh1= hocSinhRepository.save(hocSinh.convertToEntity(null, null, null, phuHuynh));
            if (kiemTra){
                hocSinh1.setPhuHuynh(phuHuynh1);
                hocSinhRepository.save(hocSinh1);
                phuHuynh1.setHocSinh(hocSinh1);
                phuHuynhRepository.save(phuHuynh1);
            }
            return hocSinh;
        }
        return null;
    }

    public HocSinhDTO updateHocSinh(Long id, HocSinhDTO hocSinh){
        if(id>=1){
            HocSinh hocSinh1 = hocSinhRepository.getHocSinhById(id);
            if(hocSinh1 != null){
                Set<LopHoc> lopHocSet = null;
                if (hocSinh.getLopHoc() != null){
                    lopHocSet = new HashSet<>();
                    for (LopHocDTO lopHoc : hocSinh.getLopHoc()){
                        lopHocSet.add(lopHocRepository.getLopHocById(lopHoc.getId()));
                    }
                }
                Set<DiemDanh> diemDanhSet = null;
                if (hocSinh.getDiemDanh() != null){
                    diemDanhSet = new HashSet<>();
                    for (DiemDanhDTO diemDanhDTO : hocSinh.getDiemDanh()){
                        diemDanhSet.add(diemDanhRepository.getDiemDanhById(diemDanhDTO.getId()));
                    }
                }
                PhuHuynh phuHuynh = null;
                if (hocSinh.getPhuHuynh() != null){
                    phuHuynh = phuHuynhRepository.getPhuHuynhById(hocSinh.getPhuHuynh().getId());
                }
                hocSinhRepository.save(hocSinh.convertToEntity(hocSinh1, lopHocSet, diemDanhSet, phuHuynh));
                return hocSinh;
            }
        }
        return null;
    }
    public ResponseEntity<?> updateHocPhi(JsonNode jsonNode) {
        try {
            long maSinhVien = Integer.parseInt(formatStringByJson(String.valueOf(jsonNode.get("id"))));
            int tienDaDong = Integer.parseInt(formatStringByJson(String.valueOf(jsonNode.get("tienDaDong"))));
            Optional<HocSinh> hocSinh= hocSinhRepository.findById(maSinhVien);
            hocSinh.get().setTienDaDong(hocSinh.get().getTienDaDong()+tienDaDong);
            hocSinhRepository.save(hocSinh.get());
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    private String formatStringByJson(String json){
        return json.replaceAll("\"","");
    }

    public Boolean dangKyLopHoc(Long idHocSinh, Long idLopHoc){
        if (idHocSinh >=1 && idLopHoc >=1){
            HocSinh hocSinh = hocSinhRepository.getHocSinhById(idHocSinh);
            LopHoc lopHoc = lopHocRepository.getLopHocById(idLopHoc);
            if (hocSinh != null && lopHoc != null){

                hocSinh.getLopHoc().add(lopHoc);
                hocSinhRepository.save(hocSinh);
                lopHoc.getHocSinh().add(hocSinh);
                lopHocRepository.save(lopHoc);
                return true;
            }
            return false;
        }
        return false;
    }
    public Boolean deleteHocSinh(Long id){
        if(id>= 1){
            hocSinhRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<HocSinhDTO> getAllHocSinh(){
        List<HocSinh> hocSinhList = hocSinhRepository.findAll();
        return hocSinhList.stream()
                .map(hocSinh -> new HocSinhDTO(hocSinh, true, true, true))
                .collect(Collectors.toList());
    }
    public  HocSinhDTO getOneHocSinh(Long id){
        HocSinh hocSinh = hocSinhRepository.getHocSinhById(id);
        return new HocSinhDTO(hocSinh, true, true, true);
    }
    public LopHocDTO themLopHoc(Long maLop, Long maHocSinh, LopHocDTO lopHoc){
        if(lopHoc != null){
            LopHoc lopHoc1 = lopHocRepository.getLopHocById(maLop);
            if(lopHoc1 != null){
                HocSinh hocSinh = hocSinhRepository.getHocSinhById(maHocSinh);
                Set<HocSinh> hocSinhList = new HashSet<>();
                hocSinhList.add(hocSinh);
                lopHoc1.setHocSinh(hocSinhList);
                System.out.println(lopHoc1.getHocSinh());
                Set<HocSinh> hocSinhSet = null;
                if (lopHoc.getHocSinh() != null){
                    hocSinhSet = new HashSet<>();
                    for (HocSinhDTO hocSinhDTO : lopHoc.getHocSinh()){
                        hocSinhSet.add(hocSinhRepository.getHocSinhById(hocSinhDTO.getId()));
                    }
                }
                Set<DiemDanh> diemDanhSet = null;
                if (lopHoc.getDiemDanh() != null){
                    diemDanhSet = new HashSet<>();
                    for (DiemDanhDTO diemDanhDTO : lopHoc.getDiemDanh()){
                        diemDanhSet.add(diemDanhRepository.getDiemDanhById(diemDanhDTO.getId()));
                    }
                }
                lopHocRepository.save(lopHoc.convertToEntity(lopHoc1, hocSinhSet, diemDanhSet));
                return lopHoc;
            }
        }
        return null;
    }
}
