package com.example.backend.api.dto;

import com.example.backend.api.entity.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashSet;
import java.util.Set;

@Data
public class HocSinhDTO{
    private Long id;
    private String ten;
    private String ngaySinh;
    private String SDT;
    private String diaChi;
    private String email;
    private String gioiTinh;
    private Integer tienDaDong;
    private Integer heSoDongTien =1;
    private Set<LopHocDTO> lopHoc;//kiểm tra
    private Set<DiemDanhDTO> diemDanh;
    private PhuHuynhDTO phuHuynh;//kiểm tra
    private UserDTO user;

    public HocSinhDTO() {
    }

    public LocalDate stringToLocalDate(String ngay) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        try {
            return  LocalDate.parse(ngay, formatter);
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("hay nhap ngay dang dd-MM-yyyy");
        }
    }
    public String localDateToString(LocalDate ngay){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return ngay.format(formatter);
    }

    public HocSinhDTO(HocSinh entity, Boolean coLopHoc, Boolean coPhuHuynh, Boolean coDiemDanh) {
        if (entity == null){ return;}
        this.id = entity.getId();
        this.ten = entity.getTen();
        if (entity.getNgaySinh() != null)
            this.ngaySinh = localDateToString(entity.getNgaySinh());
        this.SDT = entity.getSDT();
        this.diaChi = entity.getDiaChi();
        this.email = entity.getEmail();
        this.gioiTinh = entity.getGioiTinh();
        this.tienDaDong = entity.getTienDaDong();
        this.heSoDongTien = entity.getHeSoDongTien();
        if (coLopHoc){
            if (entity.getLopHoc() != null){
                Set<LopHocDTO> lopHocDtoSet = new HashSet<>();
                for (LopHoc element : entity.getLopHoc()) {
                    lopHocDtoSet.add(new LopHocDTO(element,false, false));
                }
                this.lopHoc = lopHocDtoSet;
            }
        }
        if (coDiemDanh){
            if (entity.getDiemDanh() != null){
                Set<DiemDanhDTO> diemDanhDtoSet = new HashSet<>();
                for (DiemDanh element : entity.getDiemDanh()) {
                    diemDanhDtoSet.add(new DiemDanhDTO(element));
                }
                this.diemDanh = diemDanhDtoSet;
            }
        }
        if (coPhuHuynh){
            this.phuHuynh = entity.getPhuHuynh() != null ? new PhuHuynhDTO(entity.getPhuHuynh(),false) : null;
        }
        this.user = entity.getUser() != null ? new UserDTO(entity.getUser()) : null;
    }

    public HocSinh convertToEntity(HocSinh entity, Set<LopHoc> lophoc, Set<DiemDanh> diemdanh, PhuHuynh phuhuynh) {

        if(entity == null)
            entity = new HocSinh();
        if(this.getTen() != null)
            entity.setTen(this.getTen());
        if(this.getNgaySinh() != null)
            entity.setNgaySinh(stringToLocalDate(this.getNgaySinh()));
        if(this.getSDT() != null)
            entity.setSDT(this.getSDT());
        if(this.getDiaChi() != null)
            entity.setDiaChi(this.getDiaChi());
        if(this.getEmail() != null)
            entity.setEmail(this.getEmail());
        if(this.getGioiTinh() != null)
            entity.setGioiTinh(this.getGioiTinh());
        if(this.getTienDaDong() != null)
            entity.setTienDaDong(this.getTienDaDong());
        if(this.getHeSoDongTien() != null)
            entity.setHeSoDongTien(this.getHeSoDongTien());
        if (lophoc != null){
            for (LopHoc element : lophoc){
                element.getHocSinh().add(entity);
            }
            entity.setLopHoc(lophoc);
        }
        if (diemdanh != null){
            for (DiemDanh element : diemdanh){
                element.setHocSinh(entity);
            }
            entity.setDiemDanh(diemdanh);
        }
        if (phuhuynh != null){
            phuhuynh.setHocSinh(entity);
            entity.setPhuHuynh(phuhuynh);
        }
        return entity;
    }
}
