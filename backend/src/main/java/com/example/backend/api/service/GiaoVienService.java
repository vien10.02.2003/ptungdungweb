package com.example.backend.api.service;

import com.example.backend.api.dto.GiaoVienDTO;
import com.example.backend.api.dto.LopHocDTO;
import com.example.backend.api.entity.GiaoVien;
import com.example.backend.api.entity.LopHoc;
import com.example.backend.api.repository.GiaoVienRepository;
import com.example.backend.api.repository.LopHocRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class GiaoVienService {
    @Autowired
    private GiaoVienRepository giaoVienRepository;
    @Autowired
    private LopHocRepository lopHocRepository;

    public GiaoVienDTO addGiaoVien(GiaoVienDTO giaoVien) {
        if (giaoVien != null) {
            giaoVienRepository.save(giaoVien.convertToEntity(null, null));
            return giaoVien;
        }
        return null;
    }

    public GiaoVienDTO updateGiaoVien(Long id, GiaoVienDTO giaoVien){
        if(id>=1){
            GiaoVien giaoVien1 = giaoVienRepository.getGiaoVienById(id);
            if(giaoVien1 != null){
                Set<LopHoc> lopHocSet = null;
                if(giaoVien.getLopHoc() != null){
                    lopHocSet = new HashSet<>();
                    for (LopHocDTO lophoc : giaoVien.getLopHoc()){
                        lopHocSet.add(lopHocRepository.getLopHocById(lophoc.getId()));
                    }
                }
                giaoVienRepository.save(giaoVien.convertToEntity(giaoVien1, lopHocSet));
                return giaoVien;
            }
        }
        return null;
    }

    public Boolean deleteGiaoVien(Long id){
        if(id>= 1){
            GiaoVien giaoVien=giaoVienRepository.getGiaoVienById(id);
            if (giaoVien != null) {
                for (LopHoc lopHoc : giaoVien.getLopHoc()) {
                    lopHoc.setGiaoVien(null);
                    lopHocRepository.save(lopHoc);
                }
                giaoVienRepository.deleteById(id);
            }

            return true;
        }
        return false;
    }

    public List<GiaoVienDTO> getAllGiaoVien(){
        return giaoVienRepository.getAll();
    }

    public List<GiaoVienDTO> getAllGiaoVienSapXepTheoTen() {
        return giaoVienRepository.findAllGiaoVienSapXepTheoTen();
    }
}