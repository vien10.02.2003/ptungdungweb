package com.example.backend.security;

public class Endpoints {
    public static final String front_end_host = "http://localhost:3000";
    public static final String[] PUBLIC_GET_ENDPOINTS = {
         "/giao-vien",
            "/hoc-vien",
            "/lop-hoc",
            "/phu-huynh"
    };
    public static final String[] PUBLIC_POST_ENDPOINTS = {
"/**"
    };
    public static final String[] PUBLIC_PUT_ENDPOINTS = {
"/**"

    };
    public static final String[] PUBLIC_DELETE_ENDPOINTS = {

    };

    public static final String[] ADMIN_GET_ENDPOINTS = {

    };
    public static final String[] ADMIN_POST_ENDPOINTS = {
"/**"
    };
    public static final String[] ADMIN_PUT_ENDPOINTS = {
            "/**"
    };
    public static final String[] ADMIN_DELETE_ENDPOINTS = {

    };
}
