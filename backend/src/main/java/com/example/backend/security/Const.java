package com.example.backend.security;

public class Const {
    public static enum Role {
        ADMIN("ADMIN"),
        STUDENT("STUDENT"),
        TEACHER("TEACHER"),
        PARENTS("PARENTS");

        private final String role;

        // Constructor
        Role(String role) {
            this.role = role;
        }

        // Getter cho trường role
        public String getRole() {
            return role;
        }

        // Phương thức kiểm tra vai trò
        public static Boolean checkRole(String role) {
            if (role == null) {
                return false;
            }

            for (Role r : Role.values()) {
                if (r.getRole().equals(role)) {
                    return true;
                }
            }
            return false;
        }
    }
}
