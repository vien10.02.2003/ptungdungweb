package com.example.backend.security.service;
import com.example.backend.security.Const;
import com.example.backend.security.repository.UserRepository;
import com.example.backend.security.unit.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class SecurityService {
    @Autowired
    UserRepository userRepository;
    public boolean isAdmin() {
        if (null == SecurityUtils.getCurrentUser()){
            return false;
        }
        if(Objects.equals(SecurityUtils.getCurrentUser().getUser().getRole(), Const.Role.ADMIN.name())){
            return true;
        };
        return false;
    }

    public boolean isParents() {
        if (null == SecurityUtils.getCurrentUser()){
            return false;
        }
        Object a = SecurityUtils.getCurrentUser();
        if(Objects.equals(SecurityUtils.getCurrentUser().getUser().getRole(), Const.Role.PARENTS.name())){
            return true;
        };
        return false;
    }

    public boolean isTeacher() {
        if (null == SecurityUtils.getCurrentUser()){
            return false;
        }
        if(Objects.equals(SecurityUtils.getCurrentUser().getUser().getRole(), Const.Role.TEACHER.name())){
            return true;
        };
        return false;
    }

    public boolean isStudent() {
        if (null == SecurityUtils.getCurrentUser()){
            return false;
        }
        if(Objects.equals(SecurityUtils.getCurrentUser().getUser().getRole(), Const.Role.STUDENT.name())){
            return true;
        };
        return false;
    }
}
