package com.example.backend.security.service;

import com.example.backend.security.entity.CustomUserDetails;
import com.example.backend.security.entity.User;
import com.example.backend.security.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    public void init() {
        if(loadUserByUsername("admin") == null){
            User user = new User();
            user.setUsername("admin");
            user.setPassword(passwordEncoder.encode("admin"));
            user.setRole("ADMIN");
            userRepository.save(user);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        // Kiểm tra xem user có tồn tại trong database không?
        User user = userRepository.findByUsername(username);
        if (user == null) {
            return null;
        }
        return new CustomUserDetails(user);
    }

    public UserDetails loadUserById(Long id) {
        // Kiểm tra xem user có tồn tại trong database không?
        User user = userRepository.findUserById(id);
        if (user == null) {
            return null;
        }
        return new CustomUserDetails(user);
    }


}

