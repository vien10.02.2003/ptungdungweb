package com.example.backend.security.controller;

import com.example.backend.api.dto.GiaoVienDTO;
import com.example.backend.api.dto.HocSinhDTO;
import com.example.backend.api.dto.PhuHuynhDTO;
import com.example.backend.api.entity.GiaoVien;
import com.example.backend.api.entity.HocSinh;
import com.example.backend.api.entity.PhuHuynh;
import com.example.backend.api.repository.GiaoVienRepository;
import com.example.backend.api.repository.HocSinhRepository;
import com.example.backend.api.repository.PhuHuynhRepository;
import com.example.backend.security.Const;
import com.example.backend.security.entity.User;
import com.example.backend.security.jwt.JwtTokenProvider;
import com.example.backend.security.repository.UserRepository;
import com.example.backend.security.service.SecurityService;
import com.example.backend.security.unit.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;
    @Autowired
    SecurityService securityService;
    @Autowired
    PhuHuynhRepository phuHuynhRepository;
    @Autowired
    HocSinhRepository hocSinhRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;

    @PostMapping("")
    public Map<String, String> createUser(@RequestBody User user) {
        Map<String, String> response = new HashMap<>();
        if (!securityService.isAdmin()) {
            response.put("status", "error");
            response.put("message", "don't have permission");
            return response;
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        if (!Const.Role.checkRole(user.getRole())) {
            response.put("status", "error");
            response.put("message", "invalid role");
            return response;
        }
        response.put("status", "success");
        response.put("message", "");
        return response;
    }

    @GetMapping("/info")
    public Object getCurrentUserInfomation() {
        if (securityService.isAdmin()) {
            return null;
        }
        if (securityService.isParents()) {
            PhuHuynh phuhuynh = phuHuynhRepository.getByUserId(SecurityUtils.getCurrentUser().getUser().getId());
            return new PhuHuynhDTO(phuhuynh,false);
        }
        if (securityService.isStudent()) {
            HocSinh hocSinh = hocSinhRepository.getByUserId(SecurityUtils.getCurrentUser().getUser().getId());
            return new HocSinhDTO(hocSinh,false,false,false);
        }
        if (securityService.isTeacher()) {
            GiaoVien giaovien = giaoVienRepository.getByUserId(SecurityUtils.getCurrentUser().getUser().getId());
            return new GiaoVienDTO(giaovien,false);
        }
        return null;
    }
}
