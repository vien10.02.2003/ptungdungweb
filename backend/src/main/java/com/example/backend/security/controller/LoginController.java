package com.example.backend.security.controller;

import com.example.backend.security.entity.CustomUserDetails;
import com.example.backend.security.jwt.JwtTokenProvider;
import com.example.backend.security.unit.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;


    @PostMapping("/login")
    public Map<String, String> authenticateUser(@RequestBody Map<String, String> loginRequest) {
        Map<String, String> response = new HashMap<>();
        // Xác thực từ username và password.
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.get("username"),
                            loginRequest.get("password")
                    )
            );
            // Set thông tin authentication vào Security Context
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // Trả về jwt cho người dùng.
            String jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
            response.put("role", Objects.requireNonNull(SecurityUtils.getCurrentUser()).getUser().getRole());
            response.put("token", jwt);
            response.put("type", "Bearer");
            return response;
        } catch (BadCredentialsException ex) {
            // Xử lý trường hợp thông tin đăng nhập không hợp lệ.
            response.put("status", "error");
            response.put("message", "Invalid username or password");
            return response;
        }
    }

}

