package com.example.backend.security.unit;

import com.example.backend.security.entity.CustomUserDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {
    public static boolean isAuthenticated(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return null != authentication && authentication.isAuthenticated();
    }

    public static CustomUserDetails getCurrentUser(){
        if (!isAuthenticated())
            return null;

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return  (CustomUserDetails)authentication.getPrincipal();
    }


}