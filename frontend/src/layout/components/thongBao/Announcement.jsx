// src/components/Announcement.js
import React from 'react';
import './Announcement.css'; // Tạo file này để style riêng cho Announcement

const Announcement = ({ tieuDe, noiDung, ngayThongBao }) => {
    return (
        <div className="announcement">
            <h1 className='d-flex align-items-center justify-content-center flex-column'>Thông báo</h1>
            <h2>{tieuDe}</h2>
            <p>{noiDung}</p>
            <p className="date">{ngayThongBao}</p>
        </div>
    );
};

export default Announcement;
