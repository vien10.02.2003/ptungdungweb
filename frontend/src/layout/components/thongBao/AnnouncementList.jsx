// src/components/AnnouncementList.js
import React, { useEffect, useState } from "react";
import Announcement from "./Announcement";
import axios from "../../../axiosConfig";
import moment from "moment";
import "./AnnouncementList.css"; // Tạo file này để style riêng cho AnnouncementList
import { API_ENPOINT } from "../../../appConfig";

const AnnouncementList = () => {
  const [announcements, setAnnouncements] = useState([]);

  useEffect(() => {
    axios
      .get(API_ENPOINT + "thongbao/all") // Thay đổi URL này với API thực tế của bạn
      .then((response) => {
        setAnnouncements(response.data);
      })
      .catch((error) => {
        console.error("Có lỗi xảy ra khi lấy dữ liệu:", error);
      });
  }, []);

  return (
    <div className="announcement-list">
      {announcements.length !== 0 ? (
        announcements.map((announcement, index) => (
          <Announcement
            key={index}
            tieuDe={announcement.tieuDe}
            noiDung={announcement.noiDung}
            ngayThongBao={moment(announcement.ngayThongBao).format(
              "DD/MM/YYYY"
            )}
          />
        ))
      ) : (
        <div className="no-announcements">
          <h3>Hiện chưa có thông báo nào.</h3>
          <h4>Vui lòng kiểm tra lại sau.</h4>
        </div>
      )}
    </div>
  );
};

export default AnnouncementList;
