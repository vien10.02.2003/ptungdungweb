import { FormEvent, useEffect, useState } from "react";

import { Button, TextField } from "@mui/material";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";


import { API_ENPOINT, ROLE } from "../../appConfig";
import { useAuth } from "./utils/AuthContext";
import {  jwtDecode } from "jwt-decode";
import { JwtPayload } from "./utils/JwtService";


const DangNhap = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [thongBao, setThongBao] = useState("")
    const navigate = useNavigate()
    const { isLoggedIn, setLoggedIn } = useAuth();

   
    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault()
        const loginRequest = {
            username: username,
            password: password
        };
        toast.promise(fetch(API_ENPOINT + "api/login", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginRequest)
        }
        )
            .then((response) => {
                console.log("response1")
                console.log(response)
                if (response.ok) {
                    return response.json()

                } else {
                    setThongBao("Sai tên đăng nhập hoặc mật khẩu")
                }
            }).then(
                (data) => {
                    if (data.status === "error") {
                        toast.error("Sai tên đăng nhập hoặc mật khẩu")
                    }
                    toast.success("Đăng nhập thành công")
                    localStorage.setItem('token', data.token)
                    console.log(data)
                    setLoggedIn(true)

                   

                        if (data.role === ROLE.ADMIN) {

                            navigate("/admin/dashboard")
                        }
                        else if (data.role === ROLE.TEACHER) {
                            navigate("/teacher/dashboard")
                        }  else if (data.role === ROLE.PARENTS) {
                            navigate("/parent/account")
                          }  else if (data.role === ROLE.STUDENT) {
                                navigate("/student/account")
                        } else
                            navigate("/")
                   

                }
            ).catch((error) => {
                console.log(error);
                toast.error("Tên đăng nhập hoặc mật khẩu không chính xác")

            }),
            { pending: "Đang trong quá trình xử lý" }
        )
    }

    return (
        <div className="container my-5 py-4 rounded-5 shadow-5 bg-light"
            style={{ width: "450px" }}>
            <h1 className=" text-center">Đăng Nhập</h1>
            <form className="form mt-3" style={{ padding: "0 20px" }} onSubmit={handleSubmit}>

                <TextField
                    fullWidth
                    required={true}
                    id='outlined-required'
                    label='Tên đăng nhập'
                    placeholder='Nhập tên đăng nhập'
                    value={username}
                    onChange={(e: any) => setUsername(e.target.value)}
                    className='mb-3'
                />
                <TextField
                    fullWidth
                    required={true}
                    type='password'
                    id='outlined-required'
                    label='Mật khẩu'
                    placeholder='Nhập mật khẩu'
                    value={password}
                    onChange={(e: any) => setPassword(e.target.value)}
                    className='mb-3'
                />
                {thongBao && <div style={{ color: 'red' }}>{thongBao}</div>}
                <div className='d-flex justify-content-end mt-2 px-3'>
                    <span>
                        Bạn chưa có tài khoản? <a href="/register">Đăng ký</a>
                    </span>
                </div>
                <div className="text-center my-3">
                    <Button fullWidth
                        variant="contained"
                        type="submit"

                        sx={{
                            padding: "10px",
                        }}>Đăng nhập</Button>
                </div>


            </form>
        </div>


    )
}

export default DangNhap;