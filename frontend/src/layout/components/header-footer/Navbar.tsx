import React, { ChangeEvent, useState } from "react";
import { Search } from "react-bootstrap-icons";
import { Link, useNavigate } from "react-router-dom";
import { logout } from "../utils/JwtService";
import { useAuth } from "../utils/AuthContext";


function Navbar() {
    const navigate = useNavigate()
	const { isLoggedIn, setLoggedIn } = useAuth();

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top nav-shadow"
        style={{ zIndex: 10 }}  >
            <div className="container-fluid">
                <a className="navbar-brand" href="/">English</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="/">Home</a>
                        </li>
                    </ul>
                </div>
                <div className="d-flex">
                    <input className="form-control me-2" type="search" placeholder="Tìm kiếm" aria-label="Search" 
                    ></input>
                    <button className="btn btn-outline" type="submit" ><Search></Search></button>

                </div>
                <ul className="navbar-nav me-1">
                   
                    <li className="nav-item btn-group dropdown ">
                        <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button"  aria-haspopup="true" data-bs-toggle="dropdown" aria-expanded="false">
                            <i className="fas fa-user"></i>
                        </a>
                        <ul className="dropdown-menu " style={{left:"-110px"}} aria-labelledby="navbarDropdown2">
                            <li><a className="dropdown-item" href="/account">Tài khoản</a></li>
                            <li><a className="dropdown-item" href="/login" onClick={()=>{
                                
                                logout(navigate)
												setLoggedIn(false);
                               
                            }}>{isLoggedIn?"Đăng xuất":"Đăng nhập"}</a></li>
                        </ul>
                    </li>
                </ul>



            </div>
        </nav>
    )
}
export default Navbar;