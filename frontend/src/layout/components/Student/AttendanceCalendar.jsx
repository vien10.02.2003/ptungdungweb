import "./calendarStyle.css";
import React, { useState, useEffect } from "react";
import { getDiemDanhTheoHocSinh } from "../../../api/attendanceService";
import { Button, Tooltip } from "@mui/material";

const AttendanceCalendar = ({ hocSinhId, lopHocId }) => {
  const [minusMonth, setMinusMonth] = useState(0);
  const [diemDanhList, setDiemDanhList] = useState([]);

  useEffect(() => {
    fetchDiemDanh();
  }, [minusMonth]);

  const fetchDiemDanh = async () => {
    let startDate = new Date();
    startDate.setMonth(startDate.getMonth() - minusMonth);
    startDate.setDate(1);

    let endDate = new Date(startDate);
    endDate.setMonth(startDate.getMonth() + 1);
    endDate.setDate(0);

    const response = await getDiemDanhTheoHocSinh(
      hocSinhId,
      lopHocId,
      formatDate(startDate),
      formatDate(endDate)
    );
    setDiemDanhList(response);
  };

  const formatDate = (date) => {
    const day = String(date.getDate()).padStart(2, "0");
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const year = date.getFullYear();
    return `${day}-${month}-${year}`;
  };

  const getDaysInMonth = (month, year) => {
    return new Date(year, month + 1, 0).getDate();
  };

  const generateDaysArray = (month, year) => {
    // Tìm ngày đầu tiên của tháng
    const firstDayOfMonth = new Date(year, month, 1);
    // Lấy ngày trong tuần (0: Chủ nhật, 1: Thứ Hai, ..., 6: Thứ Bảy)
    let startDay = firstDayOfMonth.getDay(); // 0 - 6
    startDay = startDay === 0 ? 7 : startDay; // Chuyển Chủ nhật (0) thành 7

    // Tìm ngày cuối cùng của tháng
    const lastDayOfMonth = new Date(year, month + 1, 0).getDate();

    // Tính số ngày cần để hiển thị từ tuần trước (nếu có)
    const daysFromLastMonth = startDay - 1;

    // Tính tổng số ngày cần hiển thị (ngày từ tuần trước + ngày trong tháng + ngày từ tuần sau)
    const totalDaysToShow = daysFromLastMonth + lastDayOfMonth;

    // Tạo mảng các ngày
    const days = [];
    let dayCounter = 1;

    // Thêm các ngày từ tuần trước (nếu có)
    for (let i = daysFromLastMonth; i > 0; i--) {
      days.push({
        day: "",
        outOfMonth: true, // Đánh dấu là ngày ngoài tháng
      });
    }

    // Thêm các ngày trong tháng
    for (let i = 1; i <= lastDayOfMonth; i++) {
      days.push({
        day: i,
        outOfMonth: false,
      });
      dayCounter++;
    }

    // Thêm các ngày từ tuần sau (nếu có)
    while (dayCounter <= totalDaysToShow) {
      days.push({
        day: "",
        outOfMonth: true, // Đánh dấu là ngày ngoài tháng
      });
      dayCounter++;
    }

    return days;
  };

  const currentMonth = new Date().getMonth() - minusMonth;
  const currentYear = new Date().getFullYear();

  const days = generateDaysArray(currentMonth, currentYear);

  const handleMonthChange = (direction) => {
    if (direction === "prev") {
      setMinusMonth(minusMonth + 1);
    } else if (direction === "next") {
      if (minusMonth > 0) {
        setMinusMonth(minusMonth - 1);
      }
    }
  };

  const attendanceData = diemDanhList.reduce((acc, record) => {
    const date = record.ngay.split("-").reverse().join("-");
    acc[date] = record.trangThai;
    return acc;
  }, {});

  return (
    <div className="attendance-calendar">
      <h2>{`Tháng ${currentMonth + 1} Năm ${currentYear}`}</h2>
      <div className="days-grid mt-4">
        <div className="day-name">Mon</div>
        <div className="day-name">Tue</div>
        <div className="day-name">Wed</div>
        <div className="day-name">Thu</div>
        <div className="day-name">Fri</div>
        <div className="day-name">Sat</div>
        <div className="day-name">Sun</div>
        {days.map((day, index) => {
          const date = `${currentYear}-${String(currentMonth + 1).padStart(
            2,
            "0"
          )}-${String(day.day).padStart(2, "0")}`;
          const isPresent = attendanceData[date];
          let className = "day";
          if (isPresent === true) {
            className += " present";
          } else if (isPresent === false) {
            className += " absent";
          }
          if (day.outOfMonth) {
            className += " out-of-month";
          }
          return (
            <div key={index} className={className}>
              {day.day}
            </div>
          );
        })}
      </div>
      
      <div className="navigation mt-4">
        <Tooltip title={"Xem điểm danh"}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => handleMonthChange("prev")}
          >
            Tháng Trước
          </Button>
        </Tooltip>
        <Tooltip title={"Xem điểm danh"}>
          <Button
            className="ms-3"
            color="primary"
            variant="contained"
            onClick={() => handleMonthChange("next")}
          >
            Tháng Sau
          </Button>
        </Tooltip>
      </div>
    </div>
  );
};

export default AttendanceCalendar;
