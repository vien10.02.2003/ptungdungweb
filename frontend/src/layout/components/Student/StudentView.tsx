import { Box, Button, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, Tab, TextField, Tooltip } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Edit } from "@mui/icons-material"
import AddIcon from "@mui/icons-material/Add";
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import ArrowBackIosNewOutlinedIcon from '@mui/icons-material/ArrowBackIosNewOutlined';
import { laythongTinUser } from "../../../api/userService";
import StudentModel from "../../Model/StudentModel";
import ParentModel from "../../Model/Parent";
import { layThongTin1PhuHuynhTheoMaHocSinh } from "../../../api/ParentAPI";
import ClassroomTableView from "../../admin/components/classroom/ClassroomTableView";
import { FadeModal } from "../utils/FadeModal";
import AttendanceCalendar from "./AttendanceCalendar";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import { toast } from "react-toastify";

const StudentView = () => {
    const [keyCountReload, setKeyCountReload] = useState(0);
    const [modifiedStatus, setModifiedStatus] = useState(false)
    const [option, setOption] = useState("");
    const [value, setValue] = React.useState('1')

    const [id, setId] = useState(0);
    const [lopHocId, setLopHocId] = useState(0);
    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue)
    }
    const [openModal, setOpenModal] = React.useState(false);
    const handleOpenModal = (lopHocId: any) => {
        setLopHocId(lopHocId)
        setOpenModal(true)
    };
    const handleCloseModal = () => setOpenModal(false);
    const [hocSinh, setHocSinh] = useState<StudentModel>({
        maSinhVien: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tienDaDong: 0
    })
    const [phuHuynh, setPhuHuynh] = useState<ParentModel>({
        maPhuHuynh: 0,
        ten: "",
        diaChi: "",
        email: "",
        gioiTinh: "",
        sdt: "",
        ngaySinh: '',
    })
    useEffect(() => {
        const token = localStorage.getItem('token')

        if (token) {
            laythongTinUser()
                .then(response => {
                    setHocSinh({ ...response, maSinhVien: response.id })
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }, [])
    useEffect(() => {
        layThongTin1PhuHuynhTheoMaHocSinh(hocSinh.maSinhVien)
            .then(response => {
                setPhuHuynh(response)
            })

    }, [hocSinh.maSinhVien])
    console.log(hocSinh.maSinhVien)

    const handleSubmit = () => {
        const token = localStorage.getItem("token")
        toast.promise(fetch(`http://localhost:8080/hocsinh/cap-nhat?id=${hocSinh.maSinhVien}`, {
            method: "PUT",
            headers: {
                "Content-type": "application/json",
                Authorizaion: `Bearer ${token}`
            },
            body: JSON.stringify({
                id: hocSinh.maSinhVien,
                sdt: hocSinh.sdt,
                ten: hocSinh.ten,
                ngaySinh: hocSinh.ngaySinh,
                diaChi: hocSinh.diaChi,
                gioiTinh: hocSinh.gioiTinh,
            })
        })
            .then(response => {
                setModifiedStatus(!modifiedStatus)
                toast.success("Cập nhật thông tin thành công")
            }).catch(e => {
                toast.error("Cập nhật thông tin thất bại")
                setModifiedStatus(!modifiedStatus)
            }),
            { pending: "Đang trong quá trình xử lý..." }
        )
    }
    return (
        <div className="container p-5 ">
            <div
                className='bg-light rounded px-2 ms-lg-2 ms-md-0 ms-sm-0 '
                style={{ minHeight: "500px" }}
            >
                <Box sx={{ width: '100%', typography: 'body1' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                            <TabList onChange={handleChange} aria-label="lab API tabs example">
                                <Tab label="Thông tin cá nhân" value="1" />
                                <Tab label="Xem điểm danh" value="2" />
                            </TabList>
                        </Box>
                        <TabPanel value="1">
                            <form className="position-relative form">
                                {!modifiedStatus && (
                                    <div className=" my-3 position-absolute"
                                        style={{
                                            bottom: "0",
                                            right: "0",
                                        }}>
                                        <Tooltip title="Chỉnh sửa thông tin"
                                            placement='bottom-end'>
                                            <Button
                                                variant="contained"
                                                className='rounded-pill '
                                                sx={{ width: "150px" }}
                                                size="large"
                                                onClick={() => setModifiedStatus(!modifiedStatus)}>
                                                <Edit></Edit>
                                            </Button>
                                        </Tooltip>

                                    </div>
                                )}
                                <div className="row">
                                    <div className="col-lg-6 col-md-12 col-md-12">
                                        <TextField
                                            required
                                            className='input-field'
                                            fullWidth style={{ margin: "12px 0" }}
                                            label="ID"
                                            value={hocSinh.maSinhVien}
                                            disabled={true}
                                            aria-readonly={true}

                                        />
                                        <TextField
                                            required
                                            className='input-field'
                                            fullWidth style={{ margin: "12px 0" }}
                                            label="Tên"
                                            value={hocSinh.ten}
                                            disabled={modifiedStatus ? false : true}
                                            onChange={e => { setHocSinh({ ...hocSinh, ten: e.target.value }) }}

                                        />
                                        <TextField
                                            required
                                            className='input-field'
                                            fullWidth style={{ margin: "12px 0" }}
                                            label="Số điện thoại"
                                            value={hocSinh.sdt}
                                            disabled={modifiedStatus ? false : true}
                                            onChange={e => setHocSinh({ ...hocSinh, sdt: e.target.value })}
                                        />
                                    </div>
                                    <div className="col-lg-6 col-md-12 col-md-12">
                                        <TextField
                                            required
                                            className='input-field'
                                            fullWidth style={{ margin: "12px 0" }}
                                            label="Địa chỉ"
                                            disabled={modifiedStatus ? false : true}
                                            onChange={e => setHocSinh({ ...hocSinh, diaChi: e.target.value })}

                                            value={hocSinh.diaChi}

                                        />
                                        <TextField
                                            required
                                            className='input-field'
                                            fullWidth style={{ margin: "12px 0" }}
                                            label="Ngày sinh"
                                            value={hocSinh.ngaySinh}
                                            disabled={modifiedStatus ? false : true}
                                            onChange={e => { setHocSinh({ ...hocSinh, ngaySinh: e.target.value }) }}

                                        />
                                        <FormControl>
                                            <FormLabel id="demo-controlled-radio-buttons-group">Gender</FormLabel>
                                            <RadioGroup
                                                aria-labelledby="demo-controlled-radio-buttons-group"
                                                name="controlled-radio-buttons-group"
                                                value={hocSinh.gioiTinh}
                                                onChange={e => setHocSinh({ ...hocSinh, gioiTinh: e.target.value })}
                                            >
                                                <FormControlLabel
                                                    disabled={
                                                        modifiedStatus ? false : true
                                                    }
                                                    value="nữ" control={<Radio />} label="Nữ" />
                                                <FormControlLabel
                                                    disabled={
                                                        modifiedStatus ? false : true
                                                    }
                                                    value="nam" control={<Radio />} label="Nam" />

                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                    {modifiedStatus && <div className="mt-3 text-center">
                                        <Button onClick={handleSubmit} variant="contained"
                                            fullWidth
                                            type="submit"
                                            sx={{ width: "50%", padding: "10px" }}>
                                            Lưu và thay đổi
                                        </Button>
                                    </div>}
                                </div>
                            </form>
                        </TabPanel>
                        <TabPanel value="2">

                                <form className="form">
                                    <div className="row">
                                        <div className="col-3">
                                            <img src="./../../../../img/public/avatardefault_92824.png" style={{ width: "170px" }}></img>
                                        </div>
                                        <div className="col-3">
                                            <Box
                                                sx={{
                                                    "& .MuiTextField-root": { mb: 3 },
                                                }}>

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Mã học viên'
                                                    style={{ width: "100%" }}

                                                    value={hocSinh.maSinhVien}
                                                    disabled={true}
                                                    size='small'
                                                />

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Số điện thoại'
                                                    style={{ width: "100%" }}

                                                    value={hocSinh.sdt}
                                                    disabled={true}
                                                    size='small'
                                                />


                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Mã phụ huynh'
                                                    style={{ width: "100%" }}
                                                    value={phuHuynh.maPhuHuynh}
                                                    disabled={true}

                                                    size='small'
                                                />
                                            </Box>
                                        </div>
                                        <div className="col-3">
                                            <Box
                                                sx={{
                                                    "& .MuiTextField-root": { mb: 3 },
                                                }}>

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Tên học viên'
                                                    style={{ width: "100%" }}

                                                    value={hocSinh.ten}
                                                    disabled={true}
                                                    size='small'
                                                />

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Giới tính'
                                                    style={{ width: "100%" }}
                                                    value={hocSinh.gioiTinh}
                                                    disabled={true}

                                                    size='small'
                                                />
                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Tên phụ huynh'
                                                    style={{ width: "100%" }}
                                                    value={phuHuynh.ten}
                                                    disabled={true}

                                                    size='small'
                                                />
                                            </Box>
                                        </div>
                                        <div className="col-3">
                                            <Box
                                                sx={{
                                                    "& .MuiTextField-root": { mb: 3 },
                                                }}>

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='email'
                                                    type="email"
                                                    style={{ width: "100%" }}

                                                    value={hocSinh.email}
                                                    disabled={true}
                                                    size='small'
                                                />

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Ngày sinh'
                                                    style={{ width: "100%" }}
                                                    value={hocSinh.ngaySinh}
                                                    disabled={true}
                                                    size='small'
                                                />

                                                <TextField
                                                    required
                                                    id='filled-required'
                                                    label='Địa chỉ'
                                                    style={{ width: "100%" }}
                                                    value={hocSinh.diaChi}
                                                    disabled={true}
                                                    size='small'
                                                />
                                            </Box>
                                        </div>
                                    </div>
                                </form>
                                <hr></hr>
                                <div className="container">


                                    <div className="">
                                        <div>
                                            <ClassroomTableView
                                                keyCountReload={keyCountReload}
                                                option={option}
                                                handleOpenModal={handleOpenModal}
                                                setKeyCountReload={setKeyCountReload}
                                                id={hocSinh.maSinhVien}
                                                setOption={setOption}
                                                setId={setId}
                                            />
                                        </div>
                                    </div>
                                    <FadeModal
                                        open={openModal}
                                        handleOpen={handleOpenModal}
                                        handleClose={handleCloseModal}
                                    >

                                        <AttendanceCalendar
                                            hocSinhId={hocSinh.maSinhVien}
                                            lopHocId={id}
                                        />

                                    </FadeModal>
                                </div>
                        </TabPanel>


                    </TabContext>
                </Box>

            </div>

        </div>
    )
}

export default StudentView