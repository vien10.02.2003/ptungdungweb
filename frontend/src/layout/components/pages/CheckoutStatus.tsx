import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { getIdUserByToken } from "../utils/JwtService";
import { useAuth } from "../utils/AuthContext";
import { API_ENPOINT } from "../../../appConfig";
import { toast } from "react-toastify";
import { CheckoutSuccess } from "./CheckoutSuccess";
import { CheckoutFail } from "./CheckoutFail";


const CheckoutStatus=()=> {
	const { isLoggedIn } = useAuth();
	const navigation = useNavigate();

	useEffect(() => {
		if (!isLoggedIn) {
			navigation("/login");
		}
	});

	const location = useLocation();
	const [isSuccess, setIsSuccess] = useState(false);

	useEffect(() => {
		const searchParams = new URLSearchParams(location.search);
		const vnpResponseCode = searchParams.get("vnp_ResponseCode");
		
		if (vnpResponseCode === "00") {
			setIsSuccess(true);
			
		}
	}, [location.search]);

	return <>{isSuccess ? <CheckoutSuccess /> : <CheckoutFail />}</>;
};

export default CheckoutStatus;
