import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  CircularProgress,
  IconButton,
  Tooltip,
} from "@mui/material";
import ClassroomModel from "../Model/Classroom";
import { layCacLopHocTheoMaSinhVien } from "../../api/ClassroomAPI";
import { layThongTin1GiaoVienTheoMaLop } from "../../api/TeacherAPI";

interface ClassroomTableProps {
  id: number;
  handleOpen: any
  setLopHocId: any
}
const ClassroomTableStudent: React.FC<ClassroomTableProps> = (props) => {
  const [loading, setLoading] = useState(false);
  // Tạo biến để lấy tất cả data
  const [data, setData] = useState<ClassroomModel[]>([]);

  useEffect(() => {
    layCacLopHocTheoMaSinhVien(props.id)
      .then((response) => {
        const lopHocs = response.map((lopHoc) => ({
          ...lopHoc,
          id: lopHoc.maLop,
        }));
        setData(lopHocs);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  }, []);

  const columns: GridColDef[] = [
    { field: "id", headerName: "ID", width: 50 },

    { field: "monHoc", headerName: "TÊN LỚP", width: 250 },
    {
      field: "lichHoc",
      headerName: "LỊCH HỌC",
      width: 200,
    },
    { field: "giaoVien", headerName: "GIÁO VIÊN", width: 200 },
    {
      field: "ngayBatDau",
      headerName: "NGÀY BẮT ĐẦU",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "ngayKetThuc",
      headerName: "NGÀY KẾT THÚC",
      width: 150,
      headerAlign: "center",
      align: "center",
    },
    {
      field: "action",
      headerName: "HÀNH ĐỘNG",
      width: 200,
      headerAlign: "center",
      align: "center",
      type: "actions",
      renderCell: (item) => {
        return (
          <div>
            <Tooltip title={"Xem điểm danh"}>
              <Button
                color="primary"
                variant="contained"
                onClick={() => {
                  props.setLopHocId(item.id);
                  props.handleOpen();
                }}
              >
                Xem điểm danh
              </Button>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  if (loading) {
    return (
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <CircularProgress />
      </Box>
    );
  }
  return (
    <div>
      <DataGrid
        rows={data}
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: { page: 0, pageSize: 10 },
          },
        }}
        pageSizeOptions={[10, 15, 20, 30]}
      />
    </div>
  );
};

export default ClassroomTableStudent;
