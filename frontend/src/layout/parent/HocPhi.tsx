import React, { FormEvent, useEffect, useState } from "react"
import ClassroomModel from "../Model/Classroom"
import { layCacLopHocTheoMaSinhVien } from "../../api/ClassroomAPI"
import { Button, Typography } from "@mui/material"
import { layThongTin1HocSinhTheoId } from "../../api/StudentAPI"
import StudentModel from "../Model/StudentModel"
import { DataGrid, GRID_CHECKBOX_SELECTION_FIELD, GridColDef } from "@mui/x-data-grid"
import { API_ENPOINT } from "../../appConfig"
import { toast } from "react-toastify"

interface HocPhiProps {
    maSinhVien: number
}
const HocPhi: React.FC<HocPhiProps> = (props) => {
    const [lopHocs, setLopHocs] = useState<ClassroomModel[]>([])
    const [tongTien, setTongTien] = useState<number>(0)
    const [hocSinh, setHocSinh] = useState<StudentModel>({
        maSinhVien: 0,
        diaChi: '',
        email: '',
        gioiTinh: '',
        ngaySinh: "",
        ten: '',
        sdt: '',
        tienDaDong: 0
    })
    const [selectedRow, setSelectedRow] = useState<ClassroomModel>();


    useEffect(() => {
        layCacLopHocTheoMaSinhVien(props.maSinhVien)
            .then(response => {
                const res = response.map(lopHoc => ({
                    ...lopHoc,
                    id: lopHoc.maLop
                }))
                setLopHocs(res)
            })
        // }).then(data => {
        //     const total = lopHocs.reduce((tongTien, data) => {
        //         return tongTien + data.giaTien;
        //     }, 0)
        //     setTongTien(total)
        // })
    }, [])
    useEffect(() => {
        const total = lopHocs.reduce((tongTien, data) => {
            return tongTien + data.giaTien;
        }, 0)
        setTongTien(total)
    })
    useEffect(() => {
        layThongTin1HocSinhTheoId(props.maSinhVien)
            .then(response => {
                setHocSinh(response)
            })
    }, [])

    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 70 },
        { field: 'monHoc', headerName: 'TÊN LỚP', width: 300 },
        { field: 'giaTien', headerName: 'GIÁ TIỀN', width: 200 },


    ];
    async function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault();
        const token = localStorage.getItem("token");
        const request = {
            id: props.maSinhVien,
            tienDaDong: tongTien
        }
        const response = await fetch(
            API_ENPOINT +
            "vnpay/create-payment?amount=" +
            (tongTien - hocSinh.tienDaDong),
            {
                method: "POST",
                headers: {
                    Authorization: `Bearer ${token}`,
                    "content-type": "application/json",
                },
            }
        );
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }
        const paymentUrl = await response.text();

        // Lưu order vào csdl
        handleSaveInfo(request);

        window.location.replace(paymentUrl);
    }

    const handleSaveInfo = (request: any) => {
        const token = localStorage.getItem("token");
        fetch(API_ENPOINT + `hocsinh/them-hoc-phi`, {
            method: "PUT",
            headers: {
                Authorization: `Bearer ${token}`,
                "content-type": "application/json",
            },
            body: JSON.stringify(request),
        })
            .then(() => {
                toast.success("Thanh toán thành công")
            })
            .catch((error) => {
                console.log(error);
                toast.error("Thanh toán thất bại");
            });
    }
    return (
        <div className="container">
            <form onSubmit={handleSubmit}>
                <div className="">
                    <Typography sx={{ fontSize: 20 }}
                        gutterBottom>TỔNG TIỀN PHẢI ĐÓNG:
                        <strong> {tongTien.toLocaleString()}đ</strong></Typography>
                    <Typography sx={{ fontSize: 20 }}
                        gutterBottom>ĐÃ ĐÓNG
                        <strong> {(hocSinh.tienDaDong).toLocaleString()}đ</strong></Typography>

                </div>
                <DataGrid rows={lopHocs} columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: { page: 0, pageSize: 10 },
                        },
                    }}
                    pageSizeOptions={[10, 15, 20, 30]}
                />
                {(tongTien - hocSinh.tienDaDong) !== 0 ? (
                    <Button className="mt-4"
                        variant="contained"
                        type="submit"
                        sx={{ width: "100%" }}
                    >Thanh toán {(tongTien - hocSinh.tienDaDong).toLocaleString()}đ cho {hocSinh.ten}</Button>)
                    : <Button className="mt-4" sx={{ width: "100%" }} disabled variant="contained">Đã thanh toán đủ</Button>}

            </form>
        </div >

    )
}

export default HocPhi