import { TabContext, TabList, TabPanel } from "@mui/lab";
import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Tab,
  TextField,
  Tooltip,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import ParentModel from "../Model/Parent";
import { Edit } from "@mui/icons-material";
import { laythongTinUser } from "../../api/userService";
import { layThongTin1HocSinhTheoMaPhuHuynh } from "../../api/StudentAPI";
import StudentModel from "../Model/StudentModel";
import ClassroomTableStudent from "./Classroom";
import HocPhi from "./HocPhi";
import { toast } from "react-toastify";
import { FadeModal } from "../components/utils/FadeModal";
import AttendanceCalendar from "../components/Student/AttendanceCalendar";

const ParentAccount = () => {
  const [modifiedStatus, setModifiedStatus] = useState(false);
  const [open, setOpen] = useState(false);
  const [value, setValue] = React.useState("1");
  const [lopHocId, setLopHocId] = useState(0);
  const [maSinhVien, setMaSinhVien] = useState(0);
  const [tongTien, setTongTien] = useState(0);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue);
  };
  const [phuHuynh, setPhuHuynh] = useState<ParentModel>({
    maPhuHuynh: 0,
    ten: "",
    ngaySinh: "",
    gioiTinh: "",
    diaChi: "",
    email: "",
    sdt: "",
  });
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      laythongTinUser()
        .then((response) => {
          setPhuHuynh({ ...response, maPhuHuynh: response.id });
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, []);

    const [hocSinh, setHocSinh] = useState<StudentModel>({
        maSinhVien: 0,
        ten: '',
        diaChi: '',
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        tienDaDong: 0
    })

    useEffect(() => {
        const token = localStorage.getItem('token')

        if (token) {
            laythongTinUser()
                .then(response => {
                    setPhuHuynh({ ...response, maPhuHuynh: response.id })
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }, [])

    useEffect(() => {
        layThongTin1HocSinhTheoMaPhuHuynh(phuHuynh.maPhuHuynh)
            .then(response => {
                setHocSinh(response)
            })
    }, [phuHuynh.maPhuHuynh])
    console.log("tong tien" + tongTien)
    const handleSubmit = () => {
        const token = localStorage.getItem("token")
        toast.promise(fetch(`http://localhost:8080/phuhuynh/cap-nhat?id=${phuHuynh.maPhuHuynh}`, {
            method: "PUT",
            headers: {
                "Content-type": "application/json",
                Authorizaion: `Bearer ${token}`
            },
            body: JSON.stringify({
                id: phuHuynh.maPhuHuynh,
                sdt: phuHuynh.sdt,
                ten: phuHuynh.ten,
                ngaySinh: phuHuynh.ngaySinh,
                diaChi: phuHuynh.diaChi,
                gioiTinh: phuHuynh.gioiTinh,
            })
        })
            .then(response => {
                setModifiedStatus(!modifiedStatus)
                toast.success("Cập nhật thông tin thành công")
            }).catch(e => {
                toast.error("Cập nhật thông tin thất bại")
                setModifiedStatus(!modifiedStatus)
            }),
            { pending: "Đang trong quá trình xử lý..." }
        )
    }
   
  useEffect(() => {
    layThongTin1HocSinhTheoMaPhuHuynh(phuHuynh.maPhuHuynh).then((response) => {
      setHocSinh(response);
    });
  }, [phuHuynh.maPhuHuynh]);
  console.log("tong tien" + tongTien);
  return (
    <div className="container my-5">
      <div
        className="bg-light rounded px-2 ms-lg-2 ms-md-0 ms-sm-0 "
        style={{ minHeight: "500px" }}
      >
        <Box sx={{ width: "100%", typography: "body1" }}>
          <TabContext value={value}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                <Tab label="Thông tin cá nhân" value="1" />
                <Tab label="Thông tin con/em" value="2" />
                <Tab label="Thanh toán học phí" value="3" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <form className="position-relative form">
                {!modifiedStatus && (
                  <div
                    className=" my-3 position-absolute"
                    style={{
                      bottom: "0",
                      right: "0",
                    }}
                  >
                    <Tooltip title="Chỉnh sửa thông tin" placement="bottom-end">
                      <Button
                        variant="contained"
                        className="rounded-pill "
                        sx={{ width: "150px" }}
                        size="large"
                        onClick={() => setModifiedStatus(!modifiedStatus)}
                      >
                        <Edit></Edit>
                      </Button>
                    </Tooltip>
                  </div>
                )}
                <div className="row">
                  <div className="col-lg-6 col-md-12 col-md-12">
                    <TextField
                      required
                      className="input-field"
                      fullWidth
                      style={{ margin: "12px 0" }}
                      label="ID"
                      value={phuHuynh.maPhuHuynh}
                      disabled={true}
                      aria-readonly={true}
                    />
                    <TextField
                      required
                      className="input-field"
                      fullWidth
                      style={{ margin: "12px 0" }}
                      label="Họ đệm"
                      value={phuHuynh.ten}
                      disabled={modifiedStatus ? false : true}
                      onChange={(e) => {
                        setPhuHuynh({ ...phuHuynh, ten: e.target.value });
                      }}
                    />
                    <TextField
                      required
                      className="input-field"
                      fullWidth
                      style={{ margin: "12px 0" }}
                      label="Số điện thoại"
                      value={phuHuynh.sdt}
                      disabled={modifiedStatus ? false : true}
                      onChange={(e) =>
                        setPhuHuynh({ ...phuHuynh, sdt: e.target.value })
                      }
                    />
                  </div>
                  <div className="col-lg-6 col-md-12 col-md-12">
                    <TextField
                      required
                      className="input-field"
                      fullWidth
                      style={{ margin: "12px 0" }}
                      label="Địa chỉ"
                      disabled={modifiedStatus ? false : true}
                      value={phuHuynh.diaChi}
                    />
                    <TextField
                      required
                      className="input-field"
                      fullWidth
                      style={{ margin: "12px 0" }}
                      label="Ngày sinh"
                      value={phuHuynh.ngaySinh}
                      disabled={modifiedStatus ? false : true}
                      onChange={(e) => {
                        setPhuHuynh({ ...phuHuynh, ngaySinh: e.target.value });
                      }}
                    />
                    <FormControl>
                      <FormLabel id="demo-controlled-radio-buttons-group">
                        Gender
                      </FormLabel>
                      <RadioGroup
                        aria-labelledby="demo-controlled-radio-buttons-group"
                        name="controlled-radio-buttons-group"
                        value={phuHuynh.gioiTinh}
                        onChange={(e) =>
                          setPhuHuynh({ ...phuHuynh, gioiTinh: e.target.value })
                        }
                      >
                        <FormControlLabel
                          disabled={modifiedStatus ? false : true}
                          value="nữ"
                          control={<Radio />}
                          label="Nữ"
                        />
                        <FormControlLabel
                          disabled={modifiedStatus ? false : true}
                          value="nam"
                          control={<Radio />}
                          label="Nam"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                  {modifiedStatus && (
                    <div className="mt-3 text-center">
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        fullWidth
                        type="submit"
                        sx={{ width: "50%", padding: "10px" }}
                      >
                        Lưu và thay đổi
                      </Button>
                    </div>
                  )}
                </div>
              </form>
            </TabPanel>
            <TabPanel value="2">
              <form className="form">
                <div className="row">
                  <div className="col-3">
                    <img
                      src="./../../../../img/public/avatardefault_92824.png"
                      style={{ width: "170px" }}
                    ></img>
                  </div>
                  <div className="col-3">
                    <Box
                      sx={{
                        "& .MuiTextField-root": { mb: 3 },
                      }}
                    >
                      <TextField
                        required
                        id="filled-required"
                        label="Mã học viên"
                        style={{ width: "100%" }}
                        value={hocSinh.maSinhVien}
                        disabled={true}
                        size="small"
                      />

                      <TextField
                        required
                        id="filled-required"
                        label="Số điện thoại"
                        style={{ width: "100%" }}
                        value={hocSinh.sdt}
                        disabled={true}
                        size="small"
                      />

                      <TextField
                        required
                        id="filled-required"
                        label="Mã phụ huynh"
                        style={{ width: "100%" }}
                        value={phuHuynh.maPhuHuynh}
                        disabled={true}
                        size="small"
                      />
                    </Box>
                  </div>
                  <div className="col-3">
                    <Box
                      sx={{
                        "& .MuiTextField-root": { mb: 3 },
                      }}
                    >
                      <TextField
                        required
                        id="filled-required"
                        label="Tên học viên"
                        style={{ width: "100%" }}
                        value={hocSinh.ten}
                        disabled={true}
                        size="small"
                      />

                      <TextField
                        required
                        id="filled-required"
                        label="Giới tính"
                        style={{ width: "100%" }}
                        value={hocSinh.gioiTinh}
                        disabled={true}
                        size="small"
                      />
                      <TextField
                        required
                        id="filled-required"
                        label="Tên phụ huynh"
                        style={{ width: "100%" }}
                        value={phuHuynh.ten}
                        disabled={true}
                        size="small"
                      />
                    </Box>
                  </div>
                  <div className="col-3">
                    <Box
                      sx={{
                        "& .MuiTextField-root": { mb: 3 },
                      }}
                    >
                      <TextField
                        required
                        id="filled-required"
                        label="email"
                        type="email"
                        style={{ width: "100%" }}
                        value={hocSinh.email}
                        disabled={true}
                        size="small"
                      />

                      <TextField
                        required
                        id="filled-required"
                        label="Ngày sinh"
                        style={{ width: "100%" }}
                        value={hocSinh.ngaySinh}
                        disabled={true}
                        size="small"
                      />

                      <TextField
                        required
                        id="filled-required"
                        label="Địa chỉ"
                        style={{ width: "100%" }}
                        value={hocSinh.diaChi}
                        disabled={true}
                        size="small"
                      />
                    </Box>
                  </div>
                </div>
              </form>
              <div>
                <ClassroomTableStudent
                  handleOpen={handleOpen}
                  id={hocSinh.maSinhVien}
                  setLopHocId={setLopHocId}
                />
              </div>
            </TabPanel>
            <TabPanel value="3">
              <div className="">
                <HocPhi maSinhVien={hocSinh.maSinhVien}></HocPhi>
              </div>
            </TabPanel>
          </TabContext>
        </Box>
      </div>


      <FadeModal open={open} handleOpen={handleOpen} handleClose={handleClose}>
        <AttendanceCalendar
          hocSinhId={hocSinh.maSinhVien}
          lopHocId={lopHocId}
        />
      </FadeModal>
    </div>
  );
};


export default ParentAccount;

