import { useEffect, useState } from "react";

import { Button, TextField } from "@mui/material";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";


import { API_ENPOINT, ROLE } from "../../appConfig";
import trangchuImage from '../../img/trangchu.png';


const HomePage = () => {
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [thongBao, setThongBao] = useState("")
    const navigate = useNavigate()

    useEffect(() => {
    	// if (isLoggedIn) {
    	// 	navigate("/");
    	// }
    });
    function handleSubmit(event) {
        event.preventDefault()
        const loginRequest = {
            username: username,
            password: password
        };
        toast.promise(fetch(API_ENPOINT + "api/login", {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(loginRequest)
        }
        )
            .then((response) => {
                console.log("response1")
                console.log(response)
                if (response.ok) {
                    return response.json()

                } else {
                    setThongBao("Sai tên đăng nhập hoặc mật khẩu")
                }
            }).then(
                (data) => {
                    if (data.status === "error") {
                        toast.error("Sai tên đăng nhập hoặc mật khẩu")
                    }
                    toast.success("Đăng nhập thành công")
                    localStorage.setItem('token', data.token)
                    console.log(data)
                    // setLoggedIn(true)

                   

                        if (data.role === ROLE.ADMIN) {

                            navigate("/admin/dashboard")
                        }
                        else if (data.role === ROLE.TEACHER) {
                            navigate("/teacher/dashboard")
                        }  else if (data.role === ROLE.PARENTS) {
                            navigate("/account")
                        } else
                            navigate("/")
                   

                }
            ).catch((error) => {
                console.log(error);
                toast.error("Tên đăng nhập hoặc mật khẩu không chính xác")

            }),
            { pending: "Đang trong quá trình xử lý" }
        )
    }

    return (
        <div className=""
            style={{ }}>
            <img src={trangchuImage} alt="ahe" style={{width:"100%"}}/>
        </div>


    )
}

export default HomePage;