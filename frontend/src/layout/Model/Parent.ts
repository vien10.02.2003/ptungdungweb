import StudentModel from "./StudentModel";

class ParentModel {
    maPhuHuynh: number;
    email: string;
    tenDangNhap?: string;
    ten: string;
    sdt: string;
    gioiTinh: string;
    matKhau?: string;
    ngaySinh?: string;
    diaChi: string;
    tenHocSinh?: string;
    tienDaDong?: number


    constructor(
        maPhuHuynh: number,
        email: string,
        ten: string,
        sdt: string,
        gioiTinh: string,
        tuoi: number,
        diaChi: string) {
        this.maPhuHuynh = maPhuHuynh;
        this.email = email;
       
        this.ten = ten;
        this.sdt = sdt;
        this.gioiTinh = gioiTinh;
       
        this.diaChi = diaChi
    }

}
export default ParentModel