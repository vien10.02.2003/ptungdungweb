import { ThirtyFpsSelect } from "@mui/icons-material";

class StudentModel {
    maSinhVien: number;
    email: string;
    tenDangNhap?: string;
    ten: string;
    sdt: string;
    gioiTinh: string;
    matKhau?: string;
    ngaySinh: string;
    diaChi: string
    tienDaDong: number;


    constructor(
        maSinhVien: number,
        email: string,
        ten: string,
        sdt: string,
        gioiTinh: string,
        ngaySinh: string,
        diaChi: string,
        tienDaDong: number) {
        this.maSinhVien = maSinhVien;
        this.email = email;

        this.ten = ten;
        this.sdt = sdt;
        this.gioiTinh = gioiTinh;
        this.tienDaDong = tienDaDong;
        this.ngaySinh = ngaySinh
        this.diaChi = diaChi
    }

}
export default StudentModel