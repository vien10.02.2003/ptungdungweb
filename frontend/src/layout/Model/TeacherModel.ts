class TeacherModel {
    maGiaoVien: number;
    email: string;
    hoDem?: string;
    tenDangNhap?: string;
    ten: string;
    sdt: string;
    gioiTinh: string;
    matKhau?: string;
    ngaySinh?: string;
    diaChi: string;
    tienDaNhan?:number;
    tienChuaNhan?: number


    constructor(
        maGiaoVien: number,
        email: string,
        hoDem: string,
        tenDangNhap: string,
        ten: string,
        sdt: string,
        gioiTinh: string,
        tuoi: number,
        diaChi: string) {
        this.maGiaoVien = maGiaoVien;
        this.email = email;
        this.ten = ten;
        this.sdt = sdt;
        this.gioiTinh = gioiTinh;
        this.diaChi = diaChi
    }

}
export default TeacherModel