import TeacherModel from "./TeacherModel";

class ClassroomModel {
    maLop: number;
    monHoc?: string;
    soHocSinh?: number;
    ngayBatDau: string;
    ngayKetThuc?: string;
    lichHoc?: string;
    giaTien: number;
    giaoVien?: string;
    isActive?: boolean;

    constructor(
        maLop: number,
        tenLop: string,
        soHocSinh: number,
        ngayBatDau: string,
        ngayKetThuc: string,
        lichHoc: string,
        giaTien: number,
        isActive: boolean,
    ) {
        this.maLop = maLop;
        this.monHoc = tenLop;
        this.soHocSinh = soHocSinh;
        this.ngayBatDau = ngayBatDau;
        this.ngayKetThuc = ngayKetThuc;
        this.lichHoc = lichHoc;
        this.giaTien = giaTien;
        this.isActive = isActive
    }
}
export default ClassroomModel