/* eslint-disable jsx-a11y/anchor-is-valid */
import { useLocation } from "react-router-dom";
import { AdminSlidebar } from "./admin/components/AdminSlidebar";
import { TeacherSlidebar } from "./teacher/TeacherSlidebar.jsx";

export default function MainSlideBar() {
  const location = useLocation();
  const locationPath = location.pathname;

  const renderLevels = () => {
    if (locationPath === "/") {
      return <div></div>;
    } else if (locationPath.slice(0, 8) === "/teacher") {
      return (
        <div>
          <TeacherSlidebar />
        </div>
      );
    } else if (locationPath.slice(0, 6) === "/admin") {
      return (
        <div>
          <AdminSlidebar />
        </div>
      );
    }
  };

  return <div className="navigation">{renderLevels()}</div>;
}
