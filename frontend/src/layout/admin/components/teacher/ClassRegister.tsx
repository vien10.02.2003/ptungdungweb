import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import ClassroomModel from "../../../Model/Classroom";
import { layToanBoLopHoc, layToanBoLopHocChuaCoGiaoVien } from "../../../../api/ClassroomAPI";
import { DataGrid, GridColDef, GridEventListener } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { Box, Button, CircularProgress, IconButton, Tooltip, Typography } from "@mui/material";
import { API_ENPOINT } from "../../../../appConfig";
import { toast } from "react-toastify";

interface ClassroomRegisterProps {
	setOption: any;
	handleOpenModal: any;
	setKeyCountReload?: any;
	keyCountReload?: any;
	setId: any;
	id?: number
	handleClose: any
}
const ClassRegister: React.FC<ClassroomRegisterProps> = (props) => {
	const [keyCountReload, setKeyCountReload] = useState(0);
	const [selectedRow, setSelectedRow] = useState<ClassroomModel>();
	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);
	const [maLop, setMaLop] = useState(0)

	const [id, setId] = useState<number>(0);

	const [loading, setLoading] = useState(false);
	// Tạo biến để lấy tất cả data
	const [data, setData] = useState<ClassroomModel[]>([]);

	const handleRowClick = (row: any) => {
		setSelectedRow(row);
		console.log(row.id)
		setMaLop(row.id)
	};


	useEffect(() => {
		layToanBoLopHocChuaCoGiaoVien()
			.then(response => {
				const lopHocs = response.map(lopHoc => ({
					...lopHoc,
					id: lopHoc.maLop
				}))
				setData(lopHocs)
				setLoading(false)
			})
			.catch((error) => console.log(error));
	}, [])

	const handleRegister = () => {
		if (selectedRow) {
			const token = localStorage.getItem('token')
			const endpoint = API_ENPOINT + `lophoc/them-giao-vien?maLop=${maLop}&maGiaoVien=${props.id}`
			const request = {
				maLop: maLop,
				maGiaoVien: props.id,
			}
			
			console.log(request)
			toast.promise(
				fetch(endpoint, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify(request),
				})
					.then(response =>{
						props.setKeyCountReload(Math.random())
						props.handleClose()
						toast.success("Đăng ký lớp học cho giáo viên thành công")
					} )
					.catch(error => console.log(error)),

				{ pending: "Đang trong quá trình xử lý ..." }
			)
		}

	}




	const columns: GridColDef[] = [
		{ field: "id", headerName: "ID", width: 100 },


		{ field: "monHoc", headerName: "TÊN LỚP", width: 300 },
		{
			field: "lichHoc",
			headerName: "LỊCH HỌC",
			width: 200,
		},
		{ field: "ngayBatDau", headerName: "NGÀY BẮT ĐẦU", width: 200 },
		{ field: "ngayKetThuc", headerName: "NGÀY BẮT ĐẦU", width: 200 },


	];

	if (loading) {
		return (
			<Box
				sx={{
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				<CircularProgress />
			</Box>
		);
	}

	return (
		<div>
			<Typography className='text-center mb-4' variant='h4' component='h2'>
				ĐĂNG KÝ LỚP HỌC
			</Typography>
			<DataGrid onRowClick={handleRowClick} style={{ cursor: "pointer" }} rows={data} columns={columns}
				initialState={{
					pagination: {
						paginationModel: { page: 0, pageSize: 10 },
					},
				}}
				pageSizeOptions={[10, 15, 20, 30]} />

			<Button variant="outlined" onClick={handleRegister}
				sx={{ width: "25%", padding: "10px" }}
				className="w-100 my-3"
				type="submit">
				Đăng ký
			</Button>
		</div>
	);

}

export default ClassRegister