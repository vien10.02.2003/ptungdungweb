import { Button } from "@mui/material";
import React, { useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import StudentForm from "./TeacherForm";
import { FadeModal } from "../../../components/utils/FadeModal";
import TeacherForm from "./TeacherForm";
import { TeacherTable } from "./TeacherTable";
import ClassRegister from "./ClassRegister";
const TeacherManagement = () => {
	// Tạo ra biến để mỗi khi thao tác CRUD thì sẽ update lại table
	const [keyCountReload, setKeyCountReload] = useState(0);

	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = React.useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const [id, setId] = useState<number>(0);

	return (
		<div className='conatiner p-5'>
			<div className='shadow-4-strong rounded p-5'>
				<div className='mb-3'>
					<Button
						variant='contained'
						color='success'
						onClick={() => {
							handleOpenModal();
							setOption("add");
						}}
						startIcon={<AddIcon />}
					>
						Thêm người dùng
					</Button>
				</div>
				<div>
					<TeacherTable
						keyCountReload={keyCountReload}
						setOption={setOption}
						handleOpenModal={handleOpenModal}
						setKeyCountReload={setKeyCountReload}
						setId={setId}
					/>
				</div>
			</div>
			<FadeModal
				open={openModal}
				handleOpen={handleOpenModal}
				handleClose={handleCloseModal}
			>
				{option != 'register' ?
					<TeacherForm
						option={option}
						setKeyCountload={setKeyCountReload}
						id={id}
						handleClose={handleCloseModal}
					/>

					: <ClassRegister
					handleOpenModal={handleOpenModal}
					setId={setId}
					id={id}
					handleClose={handleCloseModal}
					setOption={setOption}
					keyCountReload={keyCountReload}
					setKeyCountReload={setKeyCountReload} />}

			</FadeModal>
		</div>
	);
};

export default TeacherManagement