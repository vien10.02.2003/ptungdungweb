import { FormEvent, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { LoadingButton } from "@mui/lab"
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import TeacherModel from "../../../Model/TeacherModel";
import { layThongTin1GiaoVienTheoId, layToanBoGiaoVien } from "../../../../api/TeacherAPI";

interface TeacherFormProps {
    option: string;
    setKeyCountload?: any
    id: number;
    handleClose: any
}
const TeacherForm: React.FC<TeacherFormProps> = (props) => {
    const [user, setUser] = useState<TeacherModel>({
        maGiaoVien: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tienDaNhan:0,
        tienChuaNhan:0
    })

    const [statusBtn, setStatusBtn] = useState(false);

  useEffect(()=>{
    if(props.option==="update"){
        layThongTin1GiaoVienTheoId(props.id)
        .then(response=>{
            setUser(response)
        })
    }
  },[])


function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault()
    const token = localStorage.getItem('token')

    setStatusBtn(true);

    const endpoint = props.option === 'add'
        ? `http://localhost:8080/giaovien/them`
        : `http://localhost:8080/giaovien/cap-nhat?id=${user.maGiaoVien}`
    const method = props.option === 'add' ? 'POST' : 'PUT'
    toast.promise(
        fetch(endpoint, {
            method: method,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(user),
        })
            .then(response => {
                if (response.ok) {
                    setUser({
                        maGiaoVien: 0,
                        ngaySinh: '',
                        gioiTinh: '',
                        email: '',
                        sdt: '',
                        ten: "",
                        diaChi:"",
                        tienChuaNhan:0,
                        tienDaNhan:0
                    })
                    setStatusBtn(false)
                    props.setKeyCountload(Math.random())
                    props.handleClose()
                    toast.success(
                        props.option === "add"
                            ? "Thêm người dùng thành công"
                            : "Cập nhật người dùng thành công"
                    );
                } else {
                    setStatusBtn(false);
                    toast.error("Gặp lỗi trong quá trình xử lý người dùng");
                }
            })
            .catch((error) => {
				console.log(error);
				setStatusBtn(false);
				toast.error("Gặp lỗi trong quá trình xử lý người dùng");
			}),
            { pending: "Đang trong quá trình xử lý ..." }
    )
}


return (
    <div>
        <Typography className='text-center' variant='h4' component='h2'>
            {props.option === "add"
                ? "TẠO GIÁO VIÊN"
                : props.option === "update"
                    ? "SỬA GIÁO VIÊN"
                    : "XEM CHI TIẾT"}
        </Typography>
        <hr></hr>
        <div className='container'>
            <form className="form" onSubmit={handleSubmit}>
                <input type="hidden" value={user.maGiaoVien} hidden></input>
                <div className="row">
                    <div className="col-6">
                        <Box
                            sx={{
                                "& .MuiTextField-root": { mb: 3 },
                            }}>

                            <TextField
                                required
                                id='filled-required'
                                label='Email'
                                type='email'
                                style={{ width: "100%" }}

                                value={user.email}
                                InputProps={{
                                    disabled:
                                        props.option === "update" ? true : false,
                                }}
                                onChange={(e: any) => {
                                    setUser({...user, email:e.target.value})

                                }}
                                onBlur={(e: any) => {

                                }}
                                size='small'
                            />

                            <TextField
                                required
                                id='filled-required'
                                label='Số điện thoại'
                                style={{ width: "100%" }}

                                value={user.sdt}
                                onChange={(e: any) => {
                                    setUser({
                                        ...user,
                                        sdt: e.target.value,
                                    });

                                }}
                                onBlur={(e: any) => {

                                }}
                                size='small'
                            />

                            <TextField
                                required
                                id='filled-required'
                                label='Ngày sinh'
                                style={{ width: "100%" }}
                                value={user.ngaySinh}
                                onChange={(e: any) => {
                                    setUser({
                                        ...user,
                                        ngaySinh: e.target.value
                                    })
                                }}
                                size='small'
                            />
                            <TextField
                                id='filled-required'
                                label='Tiền đã nhận'
                                style={{ width: "100%" }}
                                value={user.tienDaNhan}
                                onChange={(e: any) => {
                                    setUser({
                                        ...user,
                                        tienDaNhan: e.target.value
                                    })
                                }}
                                size='small'
                            />
                        </Box>
                    </div>

                    <div className='col-6'>
                        <Box
                            sx={{
                                "& .MuiTextField-root": { mb: 3 },
                            }}
                        >


                            <TextField
                                required
                                id='filled-required'
                                label='Tên'
                                style={{ width: "100%" }}
                                value={user.ten}
                                onChange={(e: any) =>
                                    setUser({ ...user, ten: e.target.value })
                                }
                                size='small'
                            />

                            <TextField
                                id='filled-required'
                                label='Địa chỉ'
                                style={{ width: "100%" }}
                                value={user.diaChi}
                                onChange={(e: any) =>
                                    setUser({
                                        ...user,
                                        diaChi: e.target.value,
                                    })
                                }
                                size='small'
                            />

                            <FormControl fullWidth size='small' sx={{ mb: 3 }}>
                                <InputLabel id='demo-simple-select-label'>
                                    Giới tính
                                </InputLabel>
                                <Select
                                    labelId='demo-simple-select-label'
                                    id='demo-simple-select'
                                    value={user.gioiTinh}
                                    label='Giới tính'
                                    onChange={(e: any) =>
                                        setUser({ ...user, gioiTinh: e.target.value })
                                    }
                                >
                                    <MenuItem value={"nam"}>Nam</MenuItem>
                                    <MenuItem value={"nữ"}>Nữ</MenuItem>
                                </Select>
                            </FormControl>
                            <TextField
                                id='filled-required'
                                label='Tiền chưa nhận'
                                style={{ width: "100%" }}
                                value={user.tienChuaNhan}
                                onChange={(e: any) => {
                                    setUser({
                                        ...user,
                                        tienChuaNhan: e.target.value
                                    })
                                }}
                                size='small'
                            />

                        </Box>
                    </div>

                </div>
                <LoadingButton
                    className='w-100 my-3'
                    type='submit'
                    loading={statusBtn}
                    variant='outlined'
                    sx={{ width: "25%", padding: "10px" }}
                >
                    {props.option === "add" ? "Tạo người dùng" : "Lưu người dùng"}
                </LoadingButton>
            </form>
        </div>
    </div>
)
}
export default TeacherForm