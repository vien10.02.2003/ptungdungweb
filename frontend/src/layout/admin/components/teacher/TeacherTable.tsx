import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import {
	Box,
	Chip,
	CircularProgress,
	IconButton,
	Tooltip,
} from "@mui/material";
import { DataGrid, GridRowsProp, GridColDef } from '@mui/x-data-grid';
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import TeacherModel from "../../../Model/TeacherModel";
import { layToanBoGiaoVien } from "../../../../api/TeacherAPI";
import LibraryAddOutlinedIcon from '@mui/icons-material/LibraryAddOutlined';
import { useConfirm } from "material-ui-confirm";



interface TeacherTableProps {
	setOption: any;
	handleOpenModal: any;
	setKeyCountReload?: any;
	keyCountReload?: any;
	setId: any;
}

export const TeacherTable: React.FC<TeacherTableProps> = (props) => {
    
	const [loading, setLoading] = useState(true);
	// Tạo biến để lấy tất cả data
	const [data, setData] = useState<TeacherModel[]>([]);
	const confirm = useConfirm()


useEffect(()=>{
	layToanBoGiaoVien()
	.then(response=>{
		const teachers = response.map(teacher=>({
			...teacher,
			id: teacher.maGiaoVien
		}))
		setData(teachers)
		setLoading(false)
	})
	.catch((error) => console.log(error));
},[props.keyCountReload])

console.log(data)
const handleDelete = (id: any) =>{
	const token = localStorage.getItem("token");
	confirm({
		title: "Xóa người dùng",
		description: "Bạn có chắc muốn xóa người dùng này?",
		confirmationText: "Xoá",
		cancellationText: "Huỷ"
	})
	.then(()=>{
		toast.promise(
			fetch( `http://localhost:8080/giaovien?id=${id}`, {
				method: "DELETE",
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
				.then((response) => {
					if (response.ok) {
						toast.success("Xoá giáo viên thành công");
						props.setKeyCountReload(Math.random());
					} else {
						toast.error("Lỗi khi xoá người dùng");
					}
				})
				.catch((error) => {
					toast.error("Lỗi khi xoá người dùng");
					console.log(error);
				}),
			{ pending: "Đang trong quá trình xử lý ..." }
		)
	}).catch((error)=>{
		
	})
}
	const columns: GridColDef[] = [
		{
			field: "action",
			headerName: "HÀNH ĐỘNG",
			width: 200,
			type: "actions",
			renderCell: (item) => {
				return (
					<div>
						<Tooltip title={"Đăng ký lớp học"}>
							<IconButton
								color='primary'
								onClick={() => {
									props.setOption("register");
									props.setId(item.id);
									props.handleOpenModal();
								}}
							>
								<LibraryAddOutlinedIcon />
							</IconButton>
						</Tooltip>
						<Tooltip title={"Chỉnh sửa"}>
							<IconButton
								color='primary'
								onClick={() => {
									props.setOption("update");
									props.setId(item.id);
									props.handleOpenModal();
								}}
							>
								<EditOutlinedIcon />
							</IconButton>
						</Tooltip>
						<Tooltip title={"Xoá"}>
							<IconButton
								color='error'
								onClick={() => {handleDelete(item.id)}}
							>
								<DeleteOutlineOutlined />
							</IconButton>
						</Tooltip>
					</div>
				);
			},
		},
		{ field: "id", headerName: "ID", width: 50 },
		
		
		{ field: "ten", headerName: "TÊN", width: 100 },
		{
			field: "ngaySinh",
			headerName: "TUỔI",
			width: 100,
		},
		{ field: "diaChi", headerName: "ĐỊA CHỈ", width: 100 },
		{ field: "email", headerName: "EMAIL", width: 200 },
		{ field: "gioiTinh", headerName: "GIỚI TÍNH", width: 100 },
		{ field: "sdt", headerName: "SỐ ĐIỆN THOẠI", width: 120 },
		{ field: "tienDaNhan", headerName: "TIỀN ĐÃ NHẬN", width: 120 },
		{ field: "tienChuaNhan", headerName: "TIỀN CHƯA NHẬN", width: 120 },


		
	];
	if (loading) {
		return (
			<Box
				sx={{
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				<CircularProgress />
			</Box>
		);
	}

	return(
		<DataGrid rows={data} columns={columns} 
		initialState={{
			pagination: {
				paginationModel: { page: 0, pageSize: 10 },
			},
		}}
		pageSizeOptions={[10, 15, 20, 30]}/>
	
	)


};
