import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import {
	Box,
	Chip,
	CircularProgress,
	IconButton,
	Tooltip,
} from "@mui/material";
import { DataGrid, GridRowsProp, GridColDef } from '@mui/x-data-grid';
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import TeacherModel from "../../../Model/TeacherModel";
import { layToanBoGiaoVien } from "../../../../api/TeacherAPI";
import { layToanBoPhuHuynh } from "../../../../api/ParentAPI";
import ParentModel from "../../../Model/Parent";
import { useConfirm } from "material-ui-confirm";




interface ParentTableProps {
	setOption: any;
	handleOpenModal: any;
	setKeyCountReload?: any;
	keyCountReload?: any;
	setId: any;
}

export const ParentTable: React.FC<ParentTableProps> = (props) => {

	const [loading, setLoading] = useState(false);
	// Tạo biến để lấy tất cả data
	const [data, setData] = useState<ParentModel[]>([]);
	const confirm = useConfirm()

	useEffect(() => {
		layToanBoPhuHuynh()
			.then(response => {
				const parents = response.map(parent => ({
					...parent,
					id: parent.maPhuHuynh,
					tienDaDong: parent.tienDaDong
				}))
				setData(parents)
				setLoading(false)
			})
			.catch((error) => console.log(error));
	}, [props.keyCountReload])


	console.log(data)
	//Test
	const rows = [
		{ id: 1, ten: 'Snow', tuoi: 14, diaChi: "HN", email: "zuoq@gmail.com", gioiTinh: "nam", sdt:"0123123321" },
		{ id: 2, ten: 'Lannister', tuoi: 31, diaChi: "HN", email: "zuoq@gmail.com", gioiTinh: "nam", sdt:"0123123321" },
		{ id: 3, ten: 'Lannister', tuoi: 31, diaChi: "HN", email: "zuoq@gmail.com", gioiTinh: "nam", sdt:"0123123321" },
		{ id: 4, ten: 'Stark', tuoi: 11, diaChi: "HN", email: "zuoq@gmail.com", gioiTinh: "nam", sdt:"0123123321" },
		{ id: 5, ten: 'Targaryen', tuoi: 20, diaChi: "HN", email: "zuoq@gmail.com", gioiTinh: "nam", sdt:"0123123321" },
	];

	const handleDelete = (id: any) =>{
        const token = localStorage.getItem("token");
        confirm({
            title: "Xóa người dùng",
            description: "Bạn có chắc muốn xóa người dùng này?",
            confirmationText: "Xoá",
            cancellationText: "Huỷ"
        })
        .then(()=>{
            toast.promise(
                fetch( `http://localhost:8080/phuhuynh?id=${id}`, {
                    method: "DELETE",
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                })
                    .then((response) => {
                        if (response.ok) {
                            toast.success("Xoá phụ huynh thành công");
                            props.setKeyCountReload(Math.random());
                        } else {
                            toast.error("Lỗi khi xoá người dùng");
                        }
                    })
                    .catch((error) => {
                        toast.error("Lỗi khi xoá người dùng");
                        console.log(error);
                    }),
                { pending: "Đang trong quá trình xử lý ..." }
            )
        }).catch((error)=>{

		})
    }

	const columns: GridColDef[] = [
		{
			field: "action",
			headerName: "HÀNH ĐỘNG",
			width: 200,
			type: "actions",
			renderCell: (item) => {
				return (
					<div>
						<Tooltip title={"Chỉnh sửa"}>
							<IconButton
								color='primary'
								onClick={() => {
									props.setOption("update");
									props.setId(item.id);
									props.handleOpenModal();
								}}
							>
								<EditOutlinedIcon />
							</IconButton>
						</Tooltip>
						<Tooltip title={"Xoá"}>
							<IconButton
								color='error'
								onClick={()=>handleDelete(item.id)}
							>
								<DeleteOutlineOutlined />
							</IconButton>
						</Tooltip>
					</div>
				);
			},
		},
		{ field: "id", headerName: "ID", width: 50 },


		{ field: "ten", headerName: "TÊN", width: 100 },
		{
			field: "tuoi",
			headerName: "TUỔI",
			width: 100,
		},
		{ field: "diaChi", headerName: "ĐỊA CHỈ", width: 100 },
		{ field: "email", headerName: "EMAIL", width: 200 },
		{ field: "gioiTinh", headerName: "GIỚI TÍNH", width: 100 },
		{ field: "sdt", headerName: "SỐ ĐIỆN THOẠI", width: 120 },
		{ field: "tienDaDong", headerName: "TIỀN ĐÃ ĐÓNG", width: 120 },


		
	];
	
	if (loading) {
		return (
			<Box
				sx={{
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				<CircularProgress />
			</Box>
		);
	}

	return (
		<DataGrid rows={data} columns={columns}
		initialState={{
			pagination: {
				paginationModel: { page: 0, pageSize: 10 },
			},
		}}
		pageSizeOptions={[10, 15, 20, 30]} />
	);
};
