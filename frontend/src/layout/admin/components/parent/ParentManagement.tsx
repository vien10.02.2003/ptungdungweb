import { Button } from "@mui/material";
import React, { useState } from "react";
import { FadeModal } from "../../../components/utils/FadeModal";

import ParentForm from "./ParentForm";
import { ParentTable } from "./ParentTable";
const ParentManagement = () => {
	// Tạo ra biến để mỗi khi thao tác CRUD thì sẽ update lại table
	const [keyCountReload, setKeyCountReload] = useState(0);

	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = React.useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const [id, setId] = useState<number>(0);

	return (
		<div className='conatiner p-5'>
			<div className='shadow-4-strong rounded p-5'>
				
				<div>
					<ParentTable
						keyCountReload={keyCountReload}
						setOption={setOption}
						handleOpenModal={handleOpenModal}
						setKeyCountReload={setKeyCountReload}
						setId={setId}
					/>
				</div>
			</div>
			<FadeModal
				open={openModal}
				handleOpen={handleOpenModal}
				handleClose={handleCloseModal}
			>
				<ParentForm
					option={option}
					setKeyCountload={setKeyCountReload}
					id={id}
					handleClose={handleCloseModal}
				/>
			</FadeModal>
		</div>
	);
};


export default ParentManagement;