import { FormEvent, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { LoadingButton } from "@mui/lab"
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import TeacherModel from "../../../Model/TeacherModel";
import ParentModel from "../../../Model/Parent";
import { layThongTin1PhuHuynhTheoId, layToanBoPhuHuynh } from "../../../../api/ParentAPI";
import StudentModel from "../../../Model/StudentModel";

interface ParentFormProps {
    option: string;
    setKeyCountload?: any
    id: number;
    handleClose: any
}
const ParentForm: React.FC<ParentFormProps> = (props) => {
    const [user, setUser] = useState<ParentModel>({
        maPhuHuynh: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tenHocSinh: ""
    })
    const [hocSinh, setHocSinh] = useState<StudentModel>({
        maSinhVien: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tienDaDong:0
    })

    useEffect(()=>{
        layThongTin1PhuHuynhTheoId(props.id)
        .then(response=>{
            setUser(response)
        })
    },[])


    const [statusBtn, setStatusBtn] = useState(false);


    function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
        const token = localStorage.getItem('token')

        setStatusBtn(true);

        const endpoint = props.option === 'add'
            ? `http://localhost:8080/phuhuynh/them`
            : `http://localhost:8080/phuhuynh/cap-nhat?id=${user.maPhuHuynh}`
        const method = props.option === 'add' ? 'POST' : 'PUT'
        toast.promise(
            fetch(endpoint, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(user
                ),
            })
                .then(response => {
                    if (response.ok) {
                        setUser({
                            maPhuHuynh: 0,
                            ngaySinh: '',
                            gioiTinh: '',
                            email: '',
                            sdt: '',
                            ten: "",
                            diaChi:"",
                        })
                        setStatusBtn(false)
                        props.setKeyCountload(Math.random())
                        props.handleClose()
                        toast.success(
                            props.option === "add"
                                ? "Thêm giáo viên thành công"
                                : "Cập nhật giáo viên thành công"
                        );
                    } else {
                        setStatusBtn(false);
                        toast.error("Gặp lỗi trong quá trình xử lý giáo viên");
                    }
                })
                .catch((error) => {
    				console.log(error);
    				setStatusBtn(false);
    				toast.error("Gặp lỗi trong quá trình xử lý giáo viên");
    			}),
                { pending: "Đang trong quá trình xử lý ..." }
        )
    }


    return (
        <div>
            <Typography className='text-center' variant='h4' component='h2'>
                {props.option === "add"
                    ? "TẠO PHỤ HUYNH"
                    : props.option === "update"
                        ? "SỬA PHỤ HUYNH"
                        : "XEM CHI TIẾT"}
            </Typography>
            <hr></hr>
            <div className='container'>
                <form className="form" onSubmit={handleSubmit}>
                    <input type="hidden" value={user.maPhuHuynh} hidden></input>
                    <div className="row">
                        <Box
                            sx={{
                                "& .MuiTextField-root": { mb: 3 },
                            }}>
                            <TextField
                                required
                                id='filled-required'
                                label='Tên con em'
                                style={{ width: "100%" }}
                                value={user.tenHocSinh}
                                InputProps={{
                                    disabled:
                                        props.option === "update" ? true : false,
                                }}
                                onChange={(e: any) => {
                                    setHocSinh({
                                        ...hocSinh,
                                        ten: e.target.value,
                                    });
                                }}
                                onBlur={(e: any) => {

                                }}
                                size='small'
                            />
                        </Box>
                        <div className="col-6">
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}>

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Email'
                                    type='email'
                                    style={{ width: "100%" }}

                                    value={user.email}
                                    InputProps={{
                                        disabled:
                                            props.option === "update" ? true : false,
                                    }}
                                    onChange={(e: any) => {
                                        setUser({
                                            ...user,
                                            email: e.target.value,
                                        });
                                    }}
                                    onBlur={(e: any) => {

                                    }}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Số điện thoại'
                                    style={{ width: "100%" }}

                                    value={user.sdt}
                                    onChange={(e: any) => {
                                        setUser({
                                            ...user,
                                            sdt: e.target.value,
                                        });

                                    }}
                                    onBlur={(e: any) => {

                                    }}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Tuổi'
                                    style={{ width: "100%" }}
                                    value={user.ngaySinh}
                                    onChange={(e: any) => {
                                        setUser({
                                            ...user,
                                            ngaySinh: e.target.value
                                        })
                                    }}
                                    size='small'
                                />
                            </Box>
                        </div>

                        <div className='col-6'>
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}
                            >


                                <TextField
                                    required
                                    id='filled-required'
                                    label='Tên'
                                    style={{ width: "100%" }}
                                    value={user.ten}
                                    onChange={(e: any) =>
                                        setUser({ ...user, ten: e.target.value })
                                    }
                                    size='small'
                                />

                                <TextField
                                    id='filled-required'
                                    label='Địa chỉ'
                                    style={{ width: "100%" }}
                                    value={user.diaChi}
                                    onChange={(e: any) =>
                                        setUser({
                                            ...user,
                                            diaChi: e.target.value,
                                        })
                                    }
                                    size='small'
                                />

                                <FormControl fullWidth size='small' sx={{ mb: 3 }}>
                                    <InputLabel id='demo-simple-select-label'>
                                        Giới tính
                                    </InputLabel>
                                    <Select
                                        labelId='demo-simple-select-label'
                                        id='demo-simple-select'
                                        value={user.gioiTinh}
                                        label='Giới tính'
                                        onChange={(e: any) =>
                                            setUser({ ...user, gioiTinh: e.target.value })
                                        }
                                    >
                                        <MenuItem value={"nam"}>Nam</MenuItem>
                                        <MenuItem value={"nữ"}>Nữ</MenuItem>
                                    </Select>
                                </FormControl>


                            </Box>
                        </div>

                    </div>
                    <LoadingButton
                        className='w-100 my-3'
                        type='submit'
                        loading={statusBtn}
                        variant='outlined'
                        sx={{ width: "25%", padding: "10px" }}
                    >
                        {props.option === "add" ? "Tạo người dùng" : "Lưu người dùng"}
                    </LoadingButton>
                </form>
            </div>
        </div>
    )
}
export default ParentForm