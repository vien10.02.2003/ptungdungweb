import React, { useState } from "react";
import { ParentTable } from "../parent/ParentTable";
import { FadeModal } from "../../../components/utils/FadeModal";
import ParentForm from "../parent/ParentForm";
import ClassroomTable from "./ClassroomTable";
import { Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ClassroomForm from "./ClassroomForm";

const ClassroomManagement = () => {
	const [keyCountReload, setKeyCountReload] = useState(0);

	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = React.useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const [id, setId] = useState<number>(0);

	return (
		<div className='conatiner p-5'>
			<div className='shadow-4-strong rounded p-5'>
				<div className='mb-3'>
					<Button
						variant='contained'
						color='success'
						onClick={() => {
							handleOpenModal();
							setOption("add");
						}}
						startIcon={<AddIcon />}
					>
						Thêm lớp học
					</Button>
				</div>
				<div>
					<ClassroomTable
						keyCountReload={keyCountReload}
						setOption={setOption}
						handleOpenModal={handleOpenModal}
						setKeyCountReload={setKeyCountReload}
						setId={setId}
					/>
				</div>
			</div>
			<FadeModal
				open={openModal}
				handleOpen={handleOpenModal}
				handleClose={handleCloseModal}
			>
				<ClassroomForm
					option={option}
					setKeyCountload={setKeyCountReload}
					id={id}
					handleClose={handleCloseModal}
				/>
			</FadeModal>
		</div>
	)
}
export default ClassroomManagement