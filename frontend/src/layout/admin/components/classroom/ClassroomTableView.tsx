import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import ClassroomModel from "../../../Model/Classroom";
import { layCacLopHocTheoMaSinhVien, layToanBoLopHoc } from "../../../../api/ClassroomAPI";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { Box, Button, CircularProgress, IconButton, Tooltip } from "@mui/material";

interface ClassroomTableProps {
    option?: any;
    setOption?: any
    handleOpenModal?: any;
    setKeyCountReload?: any;
    keyCountReload?: any;
    id: number
    setId?: any
}
const ClassroomTableView: React.FC<ClassroomTableProps> = (props) => {
    const [openModal, setOpenModal] = useState(false);

    const [loading, setLoading] = useState(false);
    // Tạo biến để lấy tất cả data
    const [data, setData] = useState<ClassroomModel[]>([]);


    useEffect(() => {
        layCacLopHocTheoMaSinhVien(props.id)
            .then(response => {
                const lopHocs = response.map((lopHoc) => ({
                    ...lopHoc,
                    id: lopHoc.maLop
                }))
                setData(lopHocs)
                setLoading(false)
            })
            .catch((error) => console.log(error));
    }, [props.id])


   

    const columns: GridColDef[] = [
        { field: "id", headerName: "ID", width: 50 },


        { field: "monHoc", headerName: "TÊN LỚP", width: 250 },
        {
            field: "lichHoc",
            headerName: "LỊCH HỌC",
            width: 130,
        },
        { field: "ngayBatDau", headerName: "NGÀY BẮT ĐẦU", width: 130 },
        { field: "ngayKetThuc", headerName: "NGÀY BẮT ĐẦU", width: 130 },
        {
			field: "action",
			headerName: "HÀNH ĐỘNG",
			width: 200,
			type: "actions",
			renderCell: (item) => {
				return (
					<div>
						
						<Tooltip title={"Xem điểm danh"}>
							<Button 
                            color='primary'
                            variant="contained"
                            onClick={() => {
                                props.setOption("view")
                                props.setId(item.id);
                                props.handleOpenModal();
                            }}
                            >
                                Xem điểm danh
                            </Button>
						</Tooltip>
					
					</div>
				);
			},
		},


    ];

    if (loading) {
        return (
            <Box
                sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <CircularProgress />
            </Box>
        );
    }
    return (
        <div>
            <DataGrid rows={data} columns={columns} 
             initialState={{
                pagination: {
                    paginationModel: { page: 0, pageSize: 10 },
                },
            }}
            pageSizeOptions={[10, 15, 20, 30]}/>
            

        </div>
    );

}

export default ClassroomTableView