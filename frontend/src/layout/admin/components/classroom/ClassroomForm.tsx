import { FormEvent, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { LoadingButton } from "@mui/lab"
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import TeacherModel from "../../../Model/TeacherModel";
import ParentModel from "../../../Model/Parent";
import ClassroomModel from "../../../Model/Classroom";
import { layThongTin1LopHocTheoId, layToanBoLopHoc } from "../../../../api/ClassroomAPI";

interface ClassroomFormProps {
    option: string;
    setKeyCountload?: any
    id: number;
    handleClose: any
}
const ClassroomForm: React.FC<ClassroomFormProps> = (props) => {
    const [lopHoc, setLopHoc] = useState<ClassroomModel>({
        maLop: 0,
        monHoc: "",
        giaTien: 0,
        lichHoc: '',
        ngayBatDau: '',
        ngayKetThuc: '',
        soHocSinh: 0,
    })

    const [statusBtn, setStatusBtn] = useState(false);

    useEffect(() => {
        if (props.option === "update") {
            layThongTin1LopHocTheoId(props.id)
                .then((response) => {
                    setLopHoc({
                        ...response,
                        // ngayBatDau: new Date(response.ngayBatDau),
                        // ngayKetThuc: new Date(response.ngayKetThuc)
                    })
                })
        }

    }, [])
    console.log(lopHoc)
    function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
        const token = localStorage.getItem('token')

        setStatusBtn(true);

        const endpoint = props.option === 'add'
            ? `http://localhost:8080/lophoc/them`
            : `http://localhost:8080/lophoc/cap-nhat?id=${lopHoc.maLop}`
        const method = props.option === 'add' ? 'POST' : 'PUT'
        toast.promise(
            fetch(endpoint, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify(lopHoc),
            })
                .then(response => {
                    if (response.ok) {
                        setLopHoc({
                            maLop: 0,
                            monHoc: "",
                            giaTien: 0,
                            lichHoc: '',
                            ngayBatDau: '',
                            ngayKetThuc: '',
                            soHocSinh: 0,
                        })
                        setStatusBtn(false)
                        props.setKeyCountload(Math.random())
                        props.handleClose()
                        toast.success(
                            props.option === "add"
                                ? "Thêm người dùng thành công"
                                : "Cập nhật người dùng thành công"
                        );
                    } else {
                        setStatusBtn(false);
                        toast.error("Gặp lỗi trong quá trình xử lý người dùng");
                    }
                })
                .catch((error) => {
                    console.log(error);
                    setStatusBtn(false);
                    toast.error("Gặp lỗi trong quá trình xử lý người dùng");
                }),
            { pending: "Đang trong quá trình xử lý ..." }
        )
    }


    return (
        <div>
            <Typography className='text-center' variant='h4' component='h2'>
                {props.option === "add"
                    ? "TẠO LỚP HỌC"
                    : props.option === "update"
                        ? "SỬA LỚP HỌC"
                        : "XEM LỚP HỌC"}
            </Typography>
            <hr></hr>
            <div className='container'>
                <form className="form" onSubmit={handleSubmit}>
                    <input type="hidden" value={lopHoc.maLop} hidden></input>
                    <div className="row">

                        <div className="col-6">
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}>

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Tên lớp'
                                    style={{ width: "100%" }}
                                    value={lopHoc.monHoc}
                                    InputProps={{
                                        disabled:
                                            props.option === "update" ? true : false,
                                    }}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            monHoc: e.target.value
                                        })
                                    }}
                                    onBlur={(e: any) => {

                                    }}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Lịch học'
                                    style={{ width: "100%" }}

                                    value={lopHoc.lichHoc}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            lichHoc: e.target.value
                                        })
                                    }}
                                    onBlur={(e: any) => {

                                    }}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Ngày bắt đầu'
                                    style={{ width: "100%" }}
                                    value={lopHoc.ngayBatDau}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            ngayBatDau: e.target.value
                                        })
                                    }}
                                    size='small'
                                />
                            </Box>
                        </div>

                        <div className='col-6'>
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}
                            >


                                <TextField
                                    required
                                    id='filled-required'
                                    label='Giá tiền'
                                    style={{ width: "100%" }}
                                    value={lopHoc.giaTien}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            giaTien: e.target.value
                                        })
                                    }}
                                    size='small'
                                />

                                <TextField
                                    id='filled-required'
                                    label='Số học sinh'
                                    style={{ width: "100%" }}
                                    value={lopHoc.soHocSinh}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            soHocSinh: e.target.value
                                        })
                                    }}
                                    size='small'
                                />

                                <TextField
                                    id='filled-required'
                                    label='Ngày kết thúc'
                                    style={{ width: "100%" }}
                                    value={lopHoc.ngayKetThuc}
                                    onChange={(e: any) => {
                                        setLopHoc({
                                            ...lopHoc,
                                            ngayKetThuc: e.target.value
                                        })
                                    }}
                                    size='small'
                                />


                            </Box>
                        </div>

                    </div>
                    <LoadingButton
                        className='w-100 my-3'
                        type='submit'
                        loading={statusBtn}
                        variant='outlined'
                        sx={{ width: "25%", padding: "10px" }}
                    >
                        {props.option === "add" ? "Tạo lớp học" : "Lưu lớp học"}
                    </LoadingButton>
                </form>
            </div>
        </div>
    )
}
export default ClassroomForm