import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import ClassroomModel from "../../../Model/Classroom";
import { layToanBoLopHoc } from "../../../../api/ClassroomAPI";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { Box, CircularProgress, IconButton, Tooltip } from "@mui/material";
import LibraryAddOutlinedIcon from '@mui/icons-material/LibraryAddOutlined';
import { toast } from "react-toastify";
import { useConfirm } from "material-ui-confirm";
interface ClassroomTableProps {
	setOption: any;
	handleOpenModal: any;
	setKeyCountReload?: any;
	keyCountReload?: any;
	setId: any;
}
const ClassroomTable : React.FC<ClassroomTableProps> = (props) =>{
    const [keyCountReload, setKeyCountReload] = useState(0);

	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);
	const confirm = useConfirm()

	const [id, setId] = useState<number>(0);

		const [loading, setLoading] = useState(false);
	// Tạo biến để lấy tất cả data
	const [data, setData] = useState<ClassroomModel[]>([]);


	useEffect(() => {
		layToanBoLopHoc()
			.then(response => {
				const lopHocs = response.map(lopHoc => ({
					...lopHoc,
					id: lopHoc.maLop
				}))
				setData(lopHocs)
				setLoading(false)
			})
			.catch((error) => console.log(error));
	}, [props.keyCountReload])


	console.log(data)
	//Test
	const rows = [
		{ id: 1, tenLop: 'TOEIC 700+', lichHoc: "T3| 7:00-9:00", ngayBatDau: "13/5/2024", ngayKetThuc: "14/8/2024", giaTien: 1000000, soHocSinh: 50 },
        { id: 2, tenLop: 'TOEIC 300+', lichHoc: "T3| 7:00-9:00", ngayBatDau: "13/5/2024", ngayKetThuc: "14/8/2024", giaTien: 1000000, soHocSinh: 50 },
        { id: 3, tenLop: 'TOEIC CHO NGƯỜI ĐI LÀM', lichHoc: "T3| 7:00-9:00", ngayBatDau: "13/5/2024", ngayKetThuc: "14/8/2024", giaTien: 1000000, soHocSinh: 50 },
        { id: 4, tenLop: 'TOEIC CHO NGƯỜI MẤT GỐC', lichHoc: "T3| 7:00-9:00", ngayBatDau: "13/5/2024", ngayKetThuc: "14/8/2024", giaTien: 1000000, soHocSinh: 50 },
        { id: 5, tenLop: 'TOEIC CƠ BẢN', lichHoc: "T3| 7:00-9:00", ngayBatDau: "13/5/2024", ngayKetThuc: "14/8/2024", giaTien: 1000000, soHocSinh: 50 },
	];
	const handleDelete = (id: any) =>{
		const token = localStorage.getItem("token");
		confirm({
			title: "Xóa người dùng",
			description: "Bạn có chắc muốn xóa người dùng này?",
			confirmationText: "Xoá",
			cancellationText: "Huỷ"
		})
		.then(()=>{
			toast.promise(
				fetch( `http://localhost:8080/lophoc?id=${id}`, {
					method: "DELETE",
					headers: {
						Authorization: `Bearer ${token}`,
					},
				})
					.then((response) => {
						if (response.ok) {
							toast.success("Xoá lớp học thành công");
							props.setKeyCountReload(Math.random());
						} else {
							toast.error("Lỗi khi xoá lớp học");
						}
					})
					.catch((error) => {
						toast.error("Lỗi khi xoá lớp học");
						console.log(error);
					}),
				{ pending: "Đang trong quá trình xử lý ..." }
			)
		}).catch((error)=>{
			
		})
	}

	const columns: GridColDef[] = [
		{
			field: "action",
			headerName: "HÀNH ĐỘNG",
			width: 200,
			type: "actions",
			renderCell: (item) => {
				return (
					<div>
						
						<Tooltip title={"Chỉnh sửa"}>
							<IconButton
								color='primary'
								onClick={() => {
									props.setOption("update");
									props.setId(item.id);
									props.handleOpenModal();
								}}
							>
								<EditOutlinedIcon />
							</IconButton>
						</Tooltip>
						<Tooltip title={"Xoá"}>
							<IconButton
								color='error'
								onClick={() => {handleDelete(item.id)}}
							>
								<DeleteOutlineOutlined />
							</IconButton>
						</Tooltip>
					</div>
				);
			},
		},
		{ field: "id", headerName: "ID", width: 50 },


		{ field: "monHoc", headerName: "TÊN LỚP", width: 100 },
		{
			field: "lichHoc",
			headerName: "LỊCH HỌC",
			width: 100,
		},
		{ field: "ngayBatDau", headerName: "NGÀY BẮT ĐẦU", width: 100 },
		{ field: "ngayKetThuc", headerName: "NGÀY BẮT ĐẦU", width: 200 },
		{ field: "giaTien", headerName: "GIÁ TIỀN", width: 100 },
		{ field: "soHocSinh", headerName: "SỐ HỌC SINH", width: 120 },

		
	];
	
	if (loading) {
		return (
			<Box
				sx={{
					display: "flex",
					alignItems: "center",
					justifyContent: "center",
				}}
			>
				<CircularProgress />
			</Box>
		);
	}

	return (
		<DataGrid rows={data} columns={columns}
		initialState={{
			pagination: {
				paginationModel: { page: 0, pageSize: 10 },
			},
		}}
		pageSizeOptions={[10, 15, 20, 30]} />
	);
    
}

export default ClassroomTable