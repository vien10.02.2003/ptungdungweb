import { Button } from "@mui/material";
import React, { useState } from "react";
import { StudentTable } from "./StudentTable";
import AddIcon from "@mui/icons-material/Add";
import StudentForm from "./StudentForm";
import { FadeModal } from "../../../components/utils/FadeModal";
import StudentInfo from "./StudentInfo";
import { Link, useNavigate } from "react-router-dom";
const StudentManagement = () => {
	// Tạo ra biến để mỗi khi thao tác CRUD thì sẽ update lại table
	const [keyCountReload, setKeyCountReload] = useState(0);
	const navigate = useNavigate()
	const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
	const [openModal, setOpenModal] = React.useState(false);
	const handleOpenModal = () => setOpenModal(true);
	const handleCloseModal = () => setOpenModal(false);

	const [id, setId] = useState<number>(0);
	console.log(option)
	return (
		<>
			{option !== 'view' ? (
				<div className='conatiner p-5'>
					<div className='shadow-4-strong rounded p-5'>
						<div className='mb-3'>
							<Button
								variant='contained'
								color='success'
								onClick={() => {
									handleOpenModal();
									setOption("add");
								}}
								startIcon={<AddIcon />}
							>
								Thêm người dùng
							</Button>
						</div>
						<div>
							<StudentTable
								keyCountReload={keyCountReload}
								setOption={setOption}
								handleOpenModal={handleOpenModal}
								setKeyCountReload={setKeyCountReload}
								setId={setId}
							/>
						</div>
					</div>

					<FadeModal
						open={openModal}
						handleOpen={handleOpenModal}
						handleClose={handleCloseModal}
					>

						<StudentForm
							option={option}
							setKeyCountload={setKeyCountReload}
							id={id}
							handleClose={handleCloseModal}
						/>

					</FadeModal>

				</div >
			)
				: <StudentInfo id={id} option={"view"} setOption={setOption}/>}
		</>
	);
};
export default StudentManagement