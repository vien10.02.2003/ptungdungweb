import { DeleteOutlineOutlined } from "@mui/icons-material";
import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import ClassroomModel from "../../../Model/Classroom";
import { layToanBoLopHoc } from "../../../../api/ClassroomAPI";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { Box, Button, CircularProgress, IconButton, Tooltip } from "@mui/material";
import { toast } from "react-toastify";
import { API_ENPOINT } from "../../../../appConfig";

interface ClassroomTableProps {
    setOption: any;
    setKeyCountReload?: any;
    keyCountReload?: any;
    setId: any;
    id?: number
    handleClose?: any
}
const ClassRegister: React.FC<ClassroomTableProps> = (props) => {
    const [keyCountReload, setKeyCountReload] = useState(0);
const [selectedRow, setSelectedRow] = useState()
const[maLop, setMaLop] = useState(0)
    const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
    const [openModal, setOpenModal] = useState(false);
    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => setOpenModal(false);

    const [id, setId] = useState<number>(0);

    const [loading, setLoading] = useState(false);
    // Tạo biến để lấy tất cả data
    const [data, setData] = useState<ClassroomModel[]>([]);

	const handleRowClick = (row: any) => {
		setSelectedRow(row);
		console.log(row.id)
		setMaLop(row.id)
	};

    useEffect(() => {
        layToanBoLopHoc()
            .then(response => {
                const lopHocs = response.map(lopHoc => ({
                    ...lopHoc,
                    id: lopHoc.maLop
                }))
                setData(lopHocs)
                setLoading(false)
            })
            .catch((error) => console.log(error));
    }, [])
    const handleRegister = () => {
		if (selectedRow) {
			const token = localStorage.getItem('token')
			const endpoint = API_ENPOINT + `hocsinh/dangkylophoc?maLop=${maLop}&maHocSinh=${props.id}`
			const request = {
				maLop: maLop,
				maHocSinh: props.id,
			}
			
			console.log(request)
			toast.promise(
				fetch(endpoint, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify(request),
				})
					.then(response =>{
						props.setKeyCountReload(Math.random())
						props.handleClose()
						toast.success("Đăng ký lớp học cho học viên thành công")
                        console.log(response)
					} )
					.catch(error => console.log(error)),

				{ pending: "Đang trong quá trình xử lý ..." }
			)
		}

	}


  
    const columns: GridColDef[] = [
        { field: "id", headerName: "ID", width: 80 },


        { field: "monHoc", headerName: "TÊN LỚP", width: 300 },
        {
            field: "lichHoc",
            headerName: "LỊCH HỌC",
            width: 130,
        },
        { field: "ngayBatDau", headerName: "NGÀY BẮT ĐẦU", width: 130 },
        { field: "ngayKetThuc", headerName: "NGÀY KẾT THÚC", width: 130 },
        { field: "giaTien", headerName: "GIÁ TIỀN", width: 100 },
        { field: "soHocSinh", headerName: "SỐ HỌC SINH", width: 150 },


    ];

    if (loading) {
        return (
            <Box
                sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <CircularProgress />
            </Box>
        );
    }

    return (
        <div>
            <DataGrid onRowClick={handleRowClick} style={{cursor:"pointer"}} rows={data} columns={columns}
            initialState={{
                pagination: {
                    paginationModel: { page: 0, pageSize: 10 },
                },
            }}
            pageSizeOptions={[10, 15, 20, 30]}
            />
            <div className="d-flex justify-content-center mt-3">
                
                <div className="p-3">
                    <Button onClick={handleRegister} variant="contained" >Đăng ký</Button>
                </div>
            </div>
        </div>

    );

}

export default ClassRegister