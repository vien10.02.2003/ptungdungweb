import { Box, Button, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import StudentModel from "../../../Model/StudentModel";
import ParentModel from "../../../Model/Parent";
import AddIcon from "@mui/icons-material/Add";
import { StudentTable } from "./StudentTable";
import { FadeModal } from "../../../components/utils/FadeModal";
import StudentForm from "./StudentForm";
import { layThongTin1HocSinhTheoId } from "../../../../api/StudentAPI";
import { layThongTin1PhuHuynhTheoId, layThongTin1PhuHuynhTheoMaHocSinh } from "../../../../api/ParentAPI";
import ClassroomTable from "../classroom/ClassroomTable";
import ClassroomTableView from "../classroom/ClassroomTableView";
import ClassRegister from "./ClassRegister";
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import ArrowBackIosNewOutlinedIcon from '@mui/icons-material/ArrowBackIosNewOutlined';
import AttendanceCalendar from "../../../components/Student/AttendanceCalendar";
interface StudentInfoProps {
    option?: any;
    id: number;
    setOption?: any
}
const StudentInfo: React.FC<StudentInfoProps> = (props) => {
    const [keyCountReload, setKeyCountReload] = useState(0);

    const [option, setOption] = useState("");
    const [id, setId] = useState(0);

    const [openModal, setOpenModal] = React.useState(false);
    const [openAtendanceModal, setOpenAtendanceModal] = React.useState(false);
    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => setOpenModal(false);
    const [user, setUser] = useState<StudentModel>({
        maSinhVien: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tienDaDong:0
    })
    const [phuHuynh, setPhuHuynh] = useState<ParentModel>({
        maPhuHuynh: 0,
        ten: "",
        diaChi: "",
        email: "",
        gioiTinh: "",
        sdt: "",
        ngaySinh: '',
    })
    useEffect(() => {
        if (props.option === "view") {
            layThongTin1HocSinhTheoId(props.id)
                .then(response => {
                    setUser({
                        ...response,
                        // ngaySinh: new Date(response.ngaySinh)
                    })
                })
        }
    }, [keyCountReload, props.option])
    useEffect(() => {
        if (props.option === "view") {
            layThongTin1PhuHuynhTheoMaHocSinh(props.id)
                .then(response => {
                    setPhuHuynh(response)
                })
        }
    }, [])
    return (
        <div className="container p-5 ">
            <div className="shadow-4-strong rounded ms-5 p-5">
                <div className='mb-3'>
                    <Button 
                        color='success'
                        onClick={() => {
                            props.setOption("back")
                        }}
                        startIcon={<ArrowBackIosNewOutlinedIcon />}
                    >
                        
                    </Button>
                </div>
                <form className="form">
                    <div className="row">
                        <div className="col-3">
                            <img src="./../../../../img/public/avatardefault_92824.png" style={{ width: "170px" }}></img>
                        </div>
                        <div className="col-3">
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}>

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Mã học viên'
                                    style={{ width: "100%" }}

                                    value={user.maSinhVien}
                                    disabled={true}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Số điện thoại'
                                    style={{ width: "100%" }}

                                    value={user.sdt}
                                    disabled={true}
                                    size='small'
                                />


                                <TextField
                                    required
                                    id='filled-required'
                                    label='Mã phụ huynh'
                                    style={{ width: "100%" }}
                                    value={phuHuynh.maPhuHuynh}
                                    disabled={true}

                                    size='small'
                                />
                            </Box>
                        </div>
                        <div className="col-3">
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}>

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Tên học viên'
                                    style={{ width: "100%" }}

                                    value={user.ten}
                                    disabled={true}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Giới tính'
                                    style={{ width: "100%" }}
                                    value={user.gioiTinh}
                                    disabled={true}

                                    size='small'
                                />
                                <TextField
                                    required
                                    id='filled-required'
                                    label='Tên phụ huynh'
                                    style={{ width: "100%" }}
                                    value={phuHuynh.ten}
                                    disabled={true}

                                    size='small'
                                />
                            </Box>
                        </div>
                        <div className="col-3">
                            <Box
                                sx={{
                                    "& .MuiTextField-root": { mb: 3 },
                                }}>

                                <TextField
                                    required
                                    id='filled-required'
                                    label='email'
                                    type="email"
                                    style={{ width: "100%" }}

                                    value={user.email}
                                    disabled={true}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Ngày sinh'
                                    style={{ width: "100%" }}
                                    value={user.ngaySinh}
                                    disabled={true}
                                    size='small'
                                />

                                <TextField
                                    required
                                    id='filled-required'
                                    label='Địa chỉ'
                                    style={{ width: "100%" }}
                                    value={user.diaChi}
                                    disabled={true}
                                    size='small'
                                />
                            </Box>
                        </div>
                    </div>
                </form>
                <hr></hr>
                <div className="container">
                    <div className='mb-3'>
                        <Button
                            variant='contained'
                            color='success'
                            onClick={() => {
                                handleOpenModal();
                                setOption("add");
                            }}
                            startIcon={<AddIcon />}
                        >
                            Đăng ký lớp học
                        </Button>
                    </div>
                    <FadeModal
                        open={openModal}
                        handleOpen={handleOpenModal}
                        handleClose={handleCloseModal}
                    >
                        <ClassRegister
                            id={props.id}
                            keyCountReload={keyCountReload}
                            setOption={setOption}
                            handleClose={handleCloseModal}
                            setKeyCountReload={setKeyCountReload}
                            setId={setId}
                        />
                    </FadeModal>
                    
                    <FadeModal
                        open={openAtendanceModal}
                        handleOpen={()=>setOpenAtendanceModal(true)}
                        handleClose={()=>setOpenAtendanceModal(false)}
                    >
                        <AttendanceCalendar
							hocSinhId={user.maSinhVien}
                            lopHocId={id}
						/>
                    </FadeModal>
                    <div className="">
                        <div>
                            <ClassroomTableView
                                keyCountReload={keyCountReload}
                                option={option}
                                handleOpenModal={()=>setOpenAtendanceModal(true)}
                                setKeyCountReload={setKeyCountReload}
                                setOption={()=>{}}
                                setId={setId}
                                id={props.id}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default StudentInfo