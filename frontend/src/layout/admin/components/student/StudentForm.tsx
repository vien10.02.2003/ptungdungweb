import { FormEvent, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { DatePicker, LoadingButton } from "@mui/lab"
import { Box, Button, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from "@mui/material";
import StudentModel from "../../../Model/StudentModel";
import ParentModel from "../../../Model/Parent";
import { layThongTin1HocSinhTheoId, layToanBoHocVien } from "../../../../api/StudentAPI";
import { layThongTin1PhuHuynhTheoId, layThongTin1PhuHuynhTheoMaHocSinh } from "../../../../api/ParentAPI";

interface StudentFormProps {
    option: string;
    setKeyCountload?: any
    id: number;
    handleClose: any
}
const StudentForm: React.FC<StudentFormProps> = (props)=>{
    const [user, setUser] = useState<StudentModel>({
        maSinhVien: 0,
        ngaySinh: '',
        gioiTinh: '',
        email: '',
        sdt: '',
        ten: "",
        diaChi: "",
        tienDaDong:0
    })
const [phuHuynh, setPhuHuynh] = useState<ParentModel>({
    maPhuHuynh: 0,
    ten: "",
    diaChi:"",
    email:"",
    gioiTinh:"",
    sdt:"",
    ngaySinh: '',
})

useEffect(()=>{
    if(props.option==="update"){
    layThongTin1HocSinhTheoId(props.id)
    .then(response=>{
        setUser({
            ...response,
            // ngaySinh: new Date(response.ngaySinh)
        })
    })
}
},[])
useEffect(()=>{
    if(props.option==="update"){
    layThongTin1PhuHuynhTheoMaHocSinh(props.id)
    .then(response=>{
       setPhuHuynh(response)
    })
}
},[])
console.log(user)
    const [statusBtn, setStatusBtn] = useState(false);

    
    function handleSubmit(event: FormEvent<HTMLFormElement>) {
        event.preventDefault()
        const token = localStorage.getItem('token')

        setStatusBtn(true);

        const endpoint = props.option === 'add'
            ? `http://localhost:8080/hocsinh`
            : `http://localhost:8080/hocsinh/cap-nhat?id=${props.id}`
        const method = props.option === 'add' ? 'POST' : 'PUT'
        toast.promise(
            fetch(endpoint, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({
                    maSinhVien: user.maSinhVien,
                    ngaySinh: user.ngaySinh,
                    gioiTinh: user.gioiTinh,
                    email: user.email,
                    sdt: user.sdt,
                    ten: user.ten,
                    diaChi: user.diaChi,
                    phuHuynh:{
                        ten: phuHuynh.ten,
                        sdt: phuHuynh.sdt,
                    }
                }),
            })
                .then(response => {
                    if (response.ok) {
                        setUser({
                            maSinhVien: 0,
                            ngaySinh: '',
                            gioiTinh: '',
                            email: '',
                            sdt: '',
                            ten: "",
                            diaChi:"",
                            tienDaDong:0
                        })
                        setStatusBtn(false)
                        props.setKeyCountload(Math.random())
                        props.handleClose()
                        toast.success(
                            props.option === "add"
                                ? "Thêm người dùng thành công"
                                : "Cập nhật người dùng thành công"
                        );
                    } else {
                        setStatusBtn(false);
                        toast.error("Gặp lỗi trong quá trình xử lý người dùng");
                    }
                })
                .catch((error) => {
					console.log(error);
					setStatusBtn(false);
					toast.error("Gặp lỗi trong quá trình xử lý người dùng");
				}),
                { pending: "Đang trong quá trình xử lý ..." }
        )
    }
    
    
    return(
        <div>
        <Typography className='text-center' variant='h4' component='h2'>
            {props.option === "add"
                ? "TẠO NGƯỜI DÙNG"
                : props.option === "update"
                    ? "SỬA NGƯỜI DÙNG"
                    : "XEM CHI TIẾT"}
        </Typography>
        <hr></hr>
        <div className='container'>
        <form  className="form" onSubmit={handleSubmit}>
            <input type="hidden" value={user.maSinhVien} hidden></input>
            <div className="row">
                <div className="col-6">
                    <Box
                        sx={{
                            "& .MuiTextField-root": { mb: 3 },
                        }}>

                        <TextField
                            required
                            id='filled-required'
                            label='Email'
                            type='email'
                            style={{ width: "100%" }}
                          
                            value={user.email}
                            InputProps={{
                                disabled:
                                    props.option === "update" ? true : false,
                            }}
                            onChange={(e: any) => {
                                setUser({
                                    ...user,
                                    email: e.target.value,
                                });
                            }}
                            onBlur={(e: any) => {
                                
                            }}
                            size='small'
                        />

                        <TextField
                            required
                            id='filled-required'
                            label='Số điện thoại'
                            style={{ width: "100%" }}
                           
                            value={user.sdt}
                            onChange={(e: any) => {
                                setUser({
                                    ...user,
                                    sdt: e.target.value,
                                });
                               
                            }}
                            onBlur={(e: any) => {
                               
                            }}
                            size='small'
                        />

                        <TextField
                            required
                            id='filled-required'
                            label='Ngày sinh'
                            style={{ width: "100%" }}
                            value={user.ngaySinh}
                            onChange={(e: any)=>{
                                setUser({
                                    ...user,
                                    ngaySinh: e.target.value
                                })
                            }}
                            size='small'
                        />
                         <TextField
                            required
                            id='filled-required'
                            label='Tên bố/mẹ'
                            style={{ width: "100%" }}
                            value={phuHuynh.ten}
                            onChange={(e: any)=>{
                                setPhuHuynh({
                                    ...phuHuynh,
                                    ten: e.target.value
                                })
                            }}
                            InputProps={{
                                disabled:
                                    props.option === "update" ? true : false,
                            }}
                            size='small'
                        />
                    </Box>
                </div>

                <div className='col-6'>
                    <Box
                        sx={{
                            "& .MuiTextField-root": { mb: 3 },
                        }}
                    >
                       

                        <TextField
                            required
                            id='filled-required'
                            label='Tên'
                            style={{ width: "100%" }}
                            value={user.ten}
                            onChange={(e: any) =>
                                setUser({ ...user, ten: e.target.value })
                            }
                            size='small'
                        />

                        <TextField
                            id='filled-required'
                            label='Địa chỉ'
                            style={{ width: "100%" }}
                            value={user.diaChi}
                            onChange={(e: any) =>
                                setUser({
                                    ...user,
                                    diaChi: e.target.value,
                                })
                            }
                            size='small'
                        />

                        <FormControl fullWidth size='small' sx={{ mb: 3 }}>
                            <InputLabel id='demo-simple-select-label'>
                                Giới tính
                            </InputLabel>
                            <Select
                                labelId='demo-simple-select-label'
                                id='demo-simple-select'
                                value={user.gioiTinh}
                                label='Giới tính'
                                onChange={(e: any) =>
                                    setUser({ ...user, gioiTinh: e.target.value })
                                }
                            >
                                <MenuItem value={"nam"}>Nam</MenuItem>
                                <MenuItem value={"nữ"}>Nữ</MenuItem>
                            </Select>
                        </FormControl>

                        <TextField
                            required
                            id='filled-required'
                            label='Số điện thoại'
                            style={{ width: "100%" }}
                            value={phuHuynh.sdt}
                            onChange={(e: any)=>{
                                setPhuHuynh({
                                    ...phuHuynh,
                                    sdt: e.target.value
                                })
                            }}
                            InputProps={{
                                disabled:
                                    props.option === "update" ? true : false,
                            }}
                            size='small'
                        />

                       
                    </Box>
                </div>
              
            </div>
            <LoadingButton
                className='w-100 my-3'
                type='submit'
                loading={statusBtn}
                variant='outlined'
                sx={{ width: "25%", padding: "10px" }}
            >
                {props.option === "add" ? "Tạo học viên" : "Lưu học viên"}
            </LoadingButton>
        </form>
    </div>
       
    </div>
    )
}
export default StudentForm