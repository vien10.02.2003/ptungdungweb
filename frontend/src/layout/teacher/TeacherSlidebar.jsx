/* eslint-disable jsx-a11y/anchor-is-valid */
import { useEffect, useState } from "react";
import PersonIcon from "@mui/icons-material/Person";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { laythongTinUser } from "../../api/userService";
import { getLopHocTheoGiaoVien } from "../../api/classRoomService";
import { useAuth } from "../components/utils/AuthContext";
import { logout } from "../components/utils/JwtService";
import DashboardIcon from "@mui/icons-material/Dashboard";

export const TeacherSlidebar = () => {
  const [info, setInfomation] = useState({});
  const [lophoc, setLopHoc] = useState([]);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const {setLoggedIn} = useAuth()
    const navigate = useNavigate()
  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  useEffect(() => {
    const fetchData = async () => {
      let data = await laythongTinUser();
      setInfomation(data);
      if (data) {
        let lophocData = await getLopHocTheoGiaoVien(data.id);
        setLopHoc(lophocData);
      }
    };
    fetchData();
  }, []);

  return (
    <div
      className="position-fixed bg-primary d-flex flex-column justify-content-between min-vh-100"
      style={{ zIndex: "100" }}
    >
      <div className="px-3">
        <a
          className="text-decoration-none d-flex align-items-center text-white d-none d-sm-flex align-items-sm-center justify-content-center"
          href="#"
        >
          <img src="./../../../../img/public/logo1.png" alt="" width={100} />
        </a>
        <h2 className="text-center" style={{ color: "white" }}>
          TEACHER
        </h2>
        <hr className="text- white d-none d-sm-block d-md-block" />

        <ul className="nav nav-pills flex-column" id="parentM">
          <li className="nav-item">
            <NavLink
              to={"/teacher/dashboard"}
              className={`nav-link d-flex align-items-center justify-content-center`}
            >
              <DashboardIcon fontSize="small" />
              <span className="ms-2 d-none d-sm-inline d-md-inline">
                Trang chủ
              </span>
            </NavLink>
          </li>
          <li className="nav-item">
            <button
              className={`nav-link d-flex align-items-center justify-content-center`}
              onClick={toggleDropdown}
            >
              <ArrowDropDownIcon
                fontSize="small"
                style={{
                  transform: dropdownOpen ? "rotate(0deg)" : "rotate(-90deg)",
                  transition: "transform 0.3s", // Thêm chuyển động mượt mà
                }}
              />
              <span className="ms-2 d-none d-sm-inline d-md-inline">
                Danh sách lớp học
              </span>
            </button>
            {dropdownOpen && (
              <ul style={{ listStyleType: "none", paddingLeft: "15px" }}>
                {lophoc.map((classItem) => (
                  <li key={classItem.id}>
                    <NavLink
                      className="nav-link d-flex align-items-center justify-content-center"
                      to={`/teacher/course/${classItem.id}`}
                    >
                      {classItem.soKhoaHoc + " " + classItem.monHoc}
                    </NavLink>
                  </li>
                ))}
              </ul>
            )}
          </li>
        </ul>
      </div>
      <div className="dropdown open text-center">
        <a
          className="my-3 btn border-0 dropdown-toggle text-white d-inline-flex align-items-center justify-content-center"
          type="button"
          id="triggerId"
          data-bs-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <PersonIcon fontSize="small" />
          <span className="ms-2">{info?.user?.username}</span>
        </a>
        <div className="dropdown-menu" aria-labelledby="triggerId">
          <Link
            className="dropdown-item"
            style={{ cursor: "pointer" }}
            to={"/login"}
            onClick={() => {
							setLoggedIn(false);
							logout(navigate);
						}}
          >
            Đăng xuất
          </Link>
        </div>
      </div>
    </div>
  );
};
