import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import { getLopHocTheoId } from "../../../api/classRoomService";
import {
  addAllDiemDanh,
  addDiemDanh,
  getDiemDanhTheoLopHoc,
  updateDiemDanh,
} from "../../../api/attendanceService";
import { Button } from "react-bootstrap";
import StudentInfo from "../../admin/components/student/StudentInfo";
import { IconButton, Tooltip } from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { FadeModal } from "../../components/utils/FadeModal";
import ClassRoomInfo from "./ClassRoomInfo";
import InfoIcon from "@mui/icons-material/Info";

// Styled-components cho bảng điểm danh
const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

const Th = styled.th`
  border: 1px solid #ddd;
  padding: 8px;
  background-color: #f2f2f2;
  text-align: ${(props) => props.align || "center"};
`;

const Td = styled.td`
  border: 1px solid #ddd;
  padding: 8px;
  background-color: none;
  text-align: ${(props) => props.align || "center"};
`;

const CustomCheckbox = styled.input.attrs({ type: "checkbox" })`
  width: 20px;
  height: 20px;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
    background-color: lightgray;
    border-color: lightgray;
  }
`;

const Classroom = () => {
  const [option, setOption] = useState(""); // Truyền vào là có thể là (add, update)
  const [studentId, setStudentId] = useState(""); // Truyền vào là có thể là (add, update)
  const { classRoomId } = useParams(); // Lấy id từ URL
  const [students, setStudents] = useState([]);
  const [attendance, setAttendance] = useState([]);
  const [dateOffset, setDateOffset] = useState(0);
  const [lopHoc, setLopHoc] = useState({});
  const [openModal, setOpenModal] = useState(false);

  const formatDate = (date) => {
    const day = String(date.getDate()).padStart(2, "0"); // Lấy ngày và thêm số 0 vào đầu nếu cần
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Lấy tháng và thêm số 0 vào đầu nếu cần (tháng trong JavaScript bắt đầu từ 0)
    const year = date.getFullYear(); // Lấy năm
    return `${day}-${month}-${year}`; // Trả về chuỗi định dạng dd-MM-yyyy
  };

  const fetchStudents = async () => {
    try {
      let lophocData = await getLopHocTheoId(classRoomId);
      setLopHoc(lophocData);
      setStudents(lophocData.hocSinh);
      return lophocData.hocSinh;
    } catch (error) {
      console.error("Error fetching students:", error);
    }
  };

  const fetchAttendance = async () => {
    try {
      const endDate = new Date();
      endDate.setDate(endDate.getDate() - dateOffset);
      const startDate = new Date();
      startDate.setDate(endDate.getDate() - 3);
      const response = await getDiemDanhTheoLopHoc(
        classRoomId,
        dates[0],
        dates[3]
      );
      setAttendance(response);
      return response;
    } catch (error) {
      console.error("Error fetching attendance:", error);
    }
  };

  useEffect(() => {
    valueInitialization();
  }, [classRoomId]);
  
  useEffect(() => {
    fetchAttendance();
  }, [dateOffset]);

  const valueInitialization = async () => {
    let studentList = await fetchStudents();
    let attendaceList = await fetchAttendance();
    await sentInitialAttendance(studentList, attendaceList);
  };

  const sentInitialAttendance = async (studentList, attendaceList) => {
    let date = formatDate(new Date());
    let record = attendaceList.find((record) => record.ngay === date);
    if (isWeekday(new Date()) && !record) {
      let attendanceList = [];
      for (let student of studentList) {
        let diemDanh = {
          ngay: date,
          trangThai: false,
          lopHocId: classRoomId,
          hocSinhId: student.id,
        };
        attendanceList.push(diemDanh);
      }
      await addAllDiemDanh(attendanceList);
    }
  };

  const isWeekday = (date) => {
    const day = date.getDay();
    // getDay() trả về 0 cho Chủ nhật, 1 cho Thứ hai, ... , 6 cho Thứ bảy
    return day >= 1 && day <= 5;
  };

  const handleAttendanceChange = async (studentId, date, checked) => {
    let record = attendance.find(
      (record) => record.hocSinhId === studentId && record.ngay === date
    );
    let diemDanh = {
      ngay: date,
      trangThai: checked,
      lopHocId: classRoomId,
      hocSinhId: studentId,
    };
    if (record) {
      await updateDiemDanh(diemDanh, record.id);
    } else {
      await addDiemDanh(diemDanh);
    }
    fetchAttendance();
  };

  const getDateHeader = () => {
    let dateList = [];
    let i = 0;
    while (dateList.length <= 3) {
      const date = new Date();
      date.setDate(date.getDate() - dateOffset - i);
      if (isWeekday(date)) {
        dateList.push(formatDate(date));
      }
      i++;
    }
    return dateList.reverse();
  };

  const getAttendanceStatus = (studentId, date) => {
    const record = attendance.find(
      (record) => record.hocSinhId === studentId && record.ngay === date
    );
    return record ? record.trangThai : false;
  };

  const isCurentDate = (date1) => {
    const [day, month, year] = date1.split("-").map(Number);
    let d1 = new Date(year, month - 1, day);
    let currentDate = new Date();
    const day1 = new Date(d1.getFullYear(), d1.getMonth(), d1.getDate());
    const day2 = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      currentDate.getDate()
    );

    if (day1 < day2) return false;
    return true;
  };

  const dates = getDateHeader();

  const renderCell = (id) => {
    return (
      <div>
        <IconButton
          color="primary"
          onClick={() => {
            setOption("view");
            setStudentId(id);
          }}
        >
          <VisibilityIcon />
        </IconButton>
      </div>
    );
  };

  return (
    <>
      {option !== "view" ? (
        <div className="conatiner p-5" style={{ backgroundColor: "white" }}>
          <div className="shadow-4-strong rounded ms-5 p-5">
            <h3 className="mb-4">
              Lớp học {lopHoc.monHoc} khóa {lopHoc.soKhoaHoc}{" "}
              <Tooltip title={"Thông tin lớp"}>
                <IconButton color="primary" onClick={() => setOpenModal(true)}>
                  <InfoIcon />
                </IconButton>
              </Tooltip>
            </h3>
            <Table>
              <thead>
                <tr>
                  <Th>Hành Động</Th>
                  <Th align="left">Tên Học Sinh</Th>
                  {dates.map((date) => (
                    <Th key={date}>{date}</Th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {students.map((student) => (
                  <tr key={student.id}>
                    <Td>{renderCell(student.id)}</Td>
                    <Td align="left">{student.ten}</Td>
                    {dates.map((date, index) => (
                      <Td key={date}>
                        <CustomCheckbox
                          checked={getAttendanceStatus(student.id, date)}
                          disabled={!isCurentDate(date)}
                          onChange={(e) =>
                            handleAttendanceChange(
                              student.id,
                              date,
                              e.target.checked
                            )
                          }
                        />
                      </Td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </Table>
            <div className="text-center mt-2">
              <Button
                style={{
                  marginLeft: "30px",
                  backgroundColor: "#d3d3d3",
                  color: "black",
                }}
                onClick={() => setDateOffset(dateOffset + 1)}
              >
                {"<"}
              </Button>
              <Button
                style={{
                  marginLeft: "30px",
                  backgroundColor: "#d3d3d3",
                  color: "black",
                }}
                onClick={() =>
                  setDateOffset(dateOffset <= 0 ? 0 : dateOffset - 1)
                }
              >
                {">"}
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <StudentInfo id={studentId} option={"view"} setOption={setOption} />
      )}
      <div style={{ width: "300px" }}>
        <FadeModal
          open={openModal}
          handleOpen={() => setOpenModal(true)}
          handleClose={() => setOpenModal(false)}
        >
          <ClassRoomInfo lophoc={lopHoc} />
        </FadeModal>
      </div>
    </>
  );
};

export default Classroom;
