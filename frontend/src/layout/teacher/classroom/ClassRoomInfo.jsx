import { useEffect } from "react";

const ClassRoomInfo = ({ lophoc }) => {
  useEffect(() => {
    console.log(lophoc);
  }, []);
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <table className="classroom-info">
        <tbody>
          <tr>
            <td>
              <h3>Mã lớp học:</h3>
            </td>
            <td>
              <h4>{lophoc.id}</h4>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Tên lớp học:</h3>
            </td>
            <td>
              <h4>{lophoc.monHoc}</h4>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Khóa học:</h3>
            </td>
            <td>
              <h4>{lophoc.soKhoaHoc}</h4>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Ngày bắt đầu:</h3>
            </td>
            <td>
              <h4>{lophoc.ngayBatDau}</h4>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Ngày kết thúc:</h3>
            </td>
            <td>
              <h4>{lophoc.ngayKetThuc}</h4>
            </td>
          </tr>
          <tr>
            <td>
              <h3>Số học sinh:</h3>
            </td>
            <td>
              <h4>
                {lophoc.hocSinh.length}/{lophoc.soLuongHocSinh}
              </h4>
            </td>
          </tr>
          {/* Thêm các thông tin khác của lớp học nếu cần */}
        </tbody>
      </table>
    </div>
  );
};
export default ClassRoomInfo;
