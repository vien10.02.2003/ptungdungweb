

import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ParentAccount from './layout/parent/ParentAccount';
import Classroom from './layout/teacher/classroom/Classroom';
import { AuthProvider } from './layout/components/utils/AuthContext';
import CheckoutStatus from './layout/components/pages/CheckoutStatus';
import Navbar from './layout/components/header-footer/Navbar';
import StudentView from './layout/components/Student/StudentView';

import { useEffect } from "react";
import "./App.css";
import {
  BrowserRouter,
  Route,
  Routes,
  useNavigate,
  Navigate,
  useLocation,
} from "react-router-dom";
import DangNhap from "./layout/components/DangNhap";
import { ConfirmProvider } from "material-ui-confirm";
import StudentManagement from "./layout/admin/components/student/StudentManager";
import TeacherManagement from "./layout/admin/components/teacher/TeacherManagement";
import ParentManagement from "./layout/admin/components/parent/ParentManagement";
import ClassroomManagement from "./layout/admin/components/classroom/ClassroomManagement";
import MainSlideBar from "./layout/MainSlideBar";
import "react-toastify/dist/ReactToastify.css";
import TrangChu from "./layout/components/TrangChu";
import DangKyNguoiDung from './layout/components/DangKyNguoiDung';
import HomePage from './layout/homePage/HomePage';


const MyRoutes = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const isAdminPath = location.pathname.startsWith("/admin");
  const isTeacherPath = location.pathname.startsWith("/teacher");


  // useEffect(() => {
  //   navigate("/login");
  // }, []);
  return (
    <AuthProvider>
      <ConfirmProvider>
        {!isAdminPath && !isTeacherPath && <Navbar></Navbar>}
        <div className="row overflow-hidden w-100">
          {(isAdminPath || isTeacherPath)&& < div className="col-2 col-md-3 col-lg-2">
            <MainSlideBar />
          </div>
          }
         <div  className={(isAdminPath || isTeacherPath)?"col-lg-10 col-md-9":""} >
            <Routes>
              <Route path="/admin/dashboard" element={<TrangChu />} />
              <Route path="/admin/student" element={<StudentManagement />} />

              <Route path="/admin/teacher" element={<TeacherManagement />} />
              <Route path="/admin/parent" element={<ParentManagement />} />
              <Route path="/admin/course" element={<ClassroomManagement />} />

              <Route
                path="/teacher/course/:classRoomId"
                element={<Classroom />}
              />
              <Route path="/teacher/dashboard" element={<TrangChu />} />

              <Route path="/parent/account" element={<ParentAccount />} />
              <Route path="/check-out/status" element={<CheckoutStatus />} />
              <Route path='/login' element={<DangNhap />} />

              <Route path='/parent/account' element={<ParentAccount />} />
              <Route path='/student/account' element={<StudentView />} />
              <Route path="*" element={<Navigate to="/login" />} /> {/* điều hướng sang /login nếu đường dẫn không đúng */}
              <Route
                path='/check-out/status'
                element={<CheckoutStatus />}
              />
              <Route
                path='/'
                element={<HomePage />}
              />

            </Routes>
          </div>

          
          <div>

          </div>
          <ToastContainer
            position="bottom-center"
            autoClose={3000}
            pauseOnFocusLoss={false}
          />
        </div>

      </ConfirmProvider>
    </AuthProvider >
  );
};

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <MyRoutes></MyRoutes>
      </BrowserRouter>
    </div>
  );
}

export default App;
