
import TeacherModel from "../layout/Model/TeacherModel"
import { my_request } from "./Request"

export async function layToanBoGiaoVien(): Promise<TeacherModel[]> {
   const duongDan = `http://localhost:8080/giao-vien`

   const giaoVienResponse = await my_request(duongDan)
   const data = giaoVienResponse._embedded.giaoViens.map((giaoVien: any)=>{
      const danhSachGiaoVien: TeacherModel = {
         maGiaoVien: giaoVien.id,
         ten: giaoVien.ten,
         email: giaoVien.email,
         diaChi: giaoVien.diaChi,
         ngaySinh: giaoVien.ngaySinh,
         gioiTinh: giaoVien.gioiTinh,
         sdt: giaoVien.sdt,
         tienDaNhan: giaoVien.tienDaNhan,
         tienChuaNhan: giaoVien.tienChuaNhan
      }
      return danhSachGiaoVien
   })
   
return data
}

export async function layThongTin1GiaoVienTheoId(id?: number): Promise<TeacherModel> {
   const duongDan = `http://localhost:8080/giao-vien/${id}`
   const giaoVienResponse = await fetch(duongDan)
   const data = await giaoVienResponse.json()
   const giaoVien: TeacherModel = {
      maGiaoVien: data.id,
      ten: data.ten,
      email: data.email,
      diaChi: data.diaChi,
      ngaySinh: data.ngaySinh,
      gioiTinh: data.gioiTinh,
      sdt: data.sdt,
      tienDaNhan: data.tienDaNhan,
      tienChuaNhan: data.tienChuaNhan

   }
   return giaoVien
}

export async function layThongTin1GiaoVienTheoMaLop(id?: number): Promise<TeacherModel> {
   const duongDan = `http://localhost:8080/lop-hoc/${id}/giaoVien`
   const giaoVienResponse = await fetch(duongDan)
   const data = await giaoVienResponse.json()
   const giaoVien: TeacherModel = {
      maGiaoVien: data.id,
      ten: data.ten,
      email: data.email,
      diaChi: data.diaChi,
      ngaySinh: data.ngaySinh,
      gioiTinh: data.gioiTinh,
      sdt: data.sdt

   }
   return giaoVien
}