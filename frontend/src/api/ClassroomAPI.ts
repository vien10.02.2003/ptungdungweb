import ClassroomModel from "../layout/Model/Classroom"
import { my_request } from "./Request"

export async function layToanBoLopHoc(): Promise<ClassroomModel[]>{
    const duongDan = `http://localhost:8080/lop-hoc`

    const lopHocResponse = await my_request(duongDan)
    const data = lopHocResponse._embedded.lopHocs.map((lopHoc: any)=>{
       const danhSachLopHoc: ClassroomModel = {
          maLop: lopHoc.id,
          monHoc: lopHoc.monHoc,
          lichHoc: lopHoc.lichHoc,
          ngayBatDau: lopHoc.ngayBatDau,
          ngayKetThuc: lopHoc.ngayKetThuc,
          giaTien: lopHoc.soTien,
          soHocSinh: lopHoc.soLuongHocSinh,
          isActive: lopHoc.isActive,
          
       }
       return danhSachLopHoc
    })
    
 return data
}
export async function layToanBoLopHocChuaCoGiaoVien(): Promise<ClassroomModel[]>{
   const duongDan = `http://localhost:8080/lop-hoc/search/findLopHocNotHaveTeacher`

   const lopHocResponse = await my_request(duongDan)
   const data = lopHocResponse._embedded.lopHocs.map((lopHoc: any)=>{
      const danhSachLopHoc: ClassroomModel = {
         maLop: lopHoc.id,
         monHoc: lopHoc.monHoc,
         lichHoc: lopHoc.lichHoc,
         ngayBatDau: lopHoc.ngayBatDau,
         ngayKetThuc: lopHoc.ngayKetThuc,
         giaTien: lopHoc.soTien,
         soHocSinh: lopHoc.soLuongHocSinh,
         isActive: lopHoc.isActive,
         
      }
      return danhSachLopHoc
   })
   
return data
}

export async function layThongTin1LopHocTheoId(id: number): Promise<ClassroomModel> {
   const duongDan = `http://localhost:8080/lop-hoc/${id}`
   const lopHocResponse = await my_request(duongDan)
   const data = await lopHocResponse
   const lopHoc: ClassroomModel = {
      maLop: data.id,
      monHoc: data.monHoc,
      giaTien: data.soTien,
      lichHoc: data.lichHoc,
      ngayBatDau: data.ngayBatDau,
      ngayKetThuc: data.ngayKetThuc,
      soHocSinh: data.soLuongHocSinh

   }
   return lopHoc
}

export async function layCacLopHocTheoMaSinhVien(id: number): Promise<ClassroomModel[]> {
   const duongDan = `http://localhost:8080/hoc-vien/${id}/lopHoc`
   const lopHocResponse = await my_request(duongDan)
   const data = await Promise.all(lopHocResponse._embedded.lopHocs.map(async (lopHoc: any)=>{
      const giaoVienResponse = await my_request(`http://localhost:8080/lop-hoc/${lopHoc.id}/giaoVien`)
      const danhSachLopHoc: ClassroomModel = {
         maLop: lopHoc.id,
         monHoc: lopHoc.monHoc,
         lichHoc: lopHoc.lichHoc,
         ngayBatDau: lopHoc.ngayBatDau,
         ngayKetThuc: lopHoc.ngayKetThuc,
         giaTien: lopHoc.soTien,
         soHocSinh: lopHoc.soLuongHocSinh,
         isActive: lopHoc.isActive,
         giaoVien: giaoVienResponse.ten
      }
      return danhSachLopHoc
   }))
   
return data
}

export async function layCacLopHocTheoMaGiaoVien(id?: number): Promise<ClassroomModel[]> {
   const duongDan = `http://localhost:8080/giao-vien/${id}/lopHoc`
   const lopHocResponse = await my_request(duongDan)
   const data = await Promise.all(lopHocResponse._embedded.lopHocs.map(async (lopHoc: any)=>{
      const giaoVienResponse = await my_request(`http://localhost:8080/lop-hoc/${lopHoc.id}/giaoVien`)
      const danhSachLopHoc: ClassroomModel = {
         maLop: lopHoc.id,
         monHoc: lopHoc.monHoc,
         lichHoc: lopHoc.lichHoc,
         ngayBatDau: lopHoc.ngayBatDau,
         ngayKetThuc: lopHoc.ngayKetThuc,
         giaTien: lopHoc.soTien,
         soHocSinh: lopHoc.soLuongHocSinh,
         isActive: lopHoc.isActive,
         giaoVien: giaoVienResponse.ten
      }
      return danhSachLopHoc
   }))
   
return data
}

