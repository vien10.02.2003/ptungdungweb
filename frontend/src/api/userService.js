import axios from "../axiosConfig";
import { API_ENPOINT } from "../appConfig";
export async function laythongTinUser() {
    const duongDan = API_ENPOINT + `api/user/info`
    try {
        const response = await axios.get(duongDan);
        if (response.status === 200) {
            return response.data;
        }
    } catch (error) {
        console.error('Có lỗi xảy ra:', error);
    }
}