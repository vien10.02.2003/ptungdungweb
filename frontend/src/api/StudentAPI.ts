
import StudentModel from "../layout/Model/StudentModel"
import { my_request } from "./Request"

export async function layToanBoHocVien(): Promise<StudentModel[]> {
   const duongDan = `http://localhost:8080/hoc-vien`

   const hocVienResponse = await my_request(duongDan)
   const data = hocVienResponse._embedded.hocSinhs.map((sinhVien: any) => {
      const danhSachHocVien: StudentModel = {
         maSinhVien: sinhVien.id,
         ten: sinhVien.ten,
         email: sinhVien.email,
         diaChi: sinhVien.diaChi,
         ngaySinh: sinhVien.ngaySinh,
         gioiTinh: sinhVien.gioiTinh,
         sdt: sinhVien.sdt,
         tienDaDong: sinhVien.tienDaDong
      }
      return danhSachHocVien
   })

   return data
}

export async function layThongTin1HocSinhTheoId(id?: number): Promise<StudentModel> {
   const duongDan = `http://localhost:8080/hoc-vien/${id}`
   const hocVienResponse = await fetch(duongDan)
   const data = await hocVienResponse.json()
   const hocSinh: StudentModel = {
      maSinhVien: data.id,
      ten: data.ten,
      email: data.email,
      diaChi: data.diaChi,
      ngaySinh: data.ngaySinh,
      gioiTinh: data.gioiTinh,
      sdt: data.sdt,
      tienDaDong: data.tienDaDong
   }
   return hocSinh
}
export async function layThongTin1HocSinhTheoMaPhuHuynh(id: number): Promise<StudentModel> {
   const duongDan = `http://localhost:8080/phu-huynh/${id}/hocSinh`
   const hocVienResponse = await fetch(duongDan)
   const data = await hocVienResponse.json()
   const hocSinh: StudentModel = {
      maSinhVien: data.id,
      ten: data.ten,
      email: data.email,
      diaChi: data.diaChi,
      ngaySinh: data.ngaySinh,
      gioiTinh: data.gioiTinh,
      sdt: data.sdt,
      tienDaDong: data.tienDaDong
   }
   return hocSinh
}
