import axios from "../axiosConfig";
import { API_ENPOINT } from "../appConfig";
export async function getDiemDanhTheoLopHoc(lopHocId, startTime, endTime) {
  const duongDan =
    API_ENPOINT +
    "diemdanh/diemDanh?lopHocId=" +
    lopHocId +
    "&startTime=" +
    startTime +
    "&endTime=" +
    endTime;
  try {
    const response = await axios.get(duongDan);
    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.error("Có lỗi xảy ra:", error);
  }
}

export async function getDiemDanhTheoHocSinh(hocSinhId,lopHocId,  startTime, endTime) {
  const duongDan =
    API_ENPOINT +
    "diemdanh/hocsinh?hocSinhId=" +
    hocSinhId +
    "&lopHocId="+
    lopHocId+
    "&startTime=" +
    startTime +
    "&endTime=" +
    endTime;
  try {
    const response = await axios.get(duongDan);
    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.error("Có lỗi xảy ra:", error);
  }
}

export async function addAllDiemDanh(diemDanhList) {
  const duongDan = API_ENPOINT + "diemdanh/all";
  try {
    const response = await axios.post(duongDan, diemDanhList);
    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.error("Có lỗi xảy ra:", error);
  }
}

export async function addDiemDanh(diemDanh) {
  const duongDan = API_ENPOINT + "diemdanh";
  try {
    const response = await axios.post(duongDan, diemDanh);
    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.error("Có lỗi xảy ra:", error);
  }
}

export async function updateDiemDanh(diemDanh,id) {
  const duongDan = API_ENPOINT + "diemdanh?id=";
  try {
    const response = await axios.put(duongDan+id, diemDanh);
    if (response.status === 200) {
      return response.data;
    }
  } catch (error) {
    console.error("Có lỗi xảy ra:", error);
  }
}
