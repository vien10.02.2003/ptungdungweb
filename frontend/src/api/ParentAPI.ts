import ParentModel from "../layout/Model/Parent"
import { my_request } from "./Request"

export async function layToanBoPhuHuynh(): Promise<ParentModel[]> {
    const duongDan = `http://localhost:8080/phu-huynh`

    const phuHuynhResponse = await my_request(duongDan)
    const data = await Promise.all(phuHuynhResponse._embedded.phuHuynhs.map(async (phuHuynh: any) => {
      const hocSinh = await my_request( `http://localhost:8080/phu-huynh/${phuHuynh.id}/hocSinh`)
        const danhSachPhuHuynh: ParentModel = {
            maPhuHuynh: phuHuynh.id,
            ten: phuHuynh.ten,
            email: phuHuynh.email,
            diaChi: phuHuynh.diaChi,
            ngaySinh: phuHuynh.ngaySinh,
            gioiTinh: phuHuynh.gioiTinh,
            sdt: phuHuynh.sdt,
            tienDaDong: hocSinh.tienDaDong
        }
        return danhSachPhuHuynh
    }))

    return data
}

export async function layThongTin1PhuHuynhTheoId(id?: number): Promise<ParentModel> {
    const duongDan = `http://localhost:8080/phu-huynh/${id}`
    const hocSinhResponse = await my_request(`http://localhost:8080/phu-huynh/${id}/hocSinh`)
    const phuHuynhResponse = await fetch(duongDan)
    const data = await phuHuynhResponse.json()
    const phuHuynh: ParentModel = {
        maPhuHuynh: data.id,
        ten: data.ten,
        email: data.email,
        diaChi: data.diaChi,
        ngaySinh: data.ngaySinh,
        gioiTinh: data.gioiTinh,
        sdt: data.sdt,
        tenHocSinh: hocSinhResponse.ten

    }
    return phuHuynh
 }

 export async function layThongTin1PhuHuynhTheoMaHocSinh(id?: number): Promise<ParentModel> {
    const duongDan = `http://localhost:8080/hoc-vien/${id}/phuHuynh`
    // const hocSinhResponse = await my_request(`http://localhost:8080/phu-huynh/${id}/hocSinh`)
    const phuHuynhResponse = await fetch(duongDan)
    const data = await phuHuynhResponse.json()
    const phuHuynh: ParentModel = {
        maPhuHuynh: data.id,
        ten: data.ten,
        email: data.email,
        diaChi: data.diaChi,
        ngaySinh: data.ngaySinh,
        gioiTinh: data.gioiTinh,
        sdt: data.sdt,
        // tenHocSinh: hocSinhResponse.ten

    }
    return phuHuynh
 }