import axios from "../axiosConfig";
import { API_ENPOINT } from "../appConfig";
export async function getLopHocTheoGiaoVien(giaoVienId) {
    const duongDan = API_ENPOINT + `lophoc/giaovien/`
    try {
        console.log("bgajls")
        const response = await axios.get(duongDan + giaoVienId);
        if (response.status === 200) {
            return response.data;
        }
    } catch (error) {
        console.error('Có lỗi xảy ra:', error);
    }
}
export async function getLopHocTheoId(lopHocId) {
    const duongDan = API_ENPOINT + `lophoc?id=`
    try {
        const response = await axios.get(duongDan + lopHocId);
        if (response.status === 200) {
            return response.data;
        }
    } catch (error) {
        console.error('Có lỗi xảy ra:', error);
    }
}