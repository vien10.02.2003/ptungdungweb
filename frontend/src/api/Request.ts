export async function my_request(duongDan: string) {
    let token = localStorage.getItem("token")
    const response = await fetch(duongDan,{
        method: 'GET', // hoặc 'POST', 'PUT', 'DELETE', vv.
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        }
    });

    if (!response.ok) {
        throw new Error(`Không thể lấy dữ liệu từ ${duongDan}`)
    }
    return response.json();
}
